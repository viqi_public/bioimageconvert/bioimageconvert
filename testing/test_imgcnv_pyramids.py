#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_metadata_read, assert_image_commands

class TestBioImageConvertPyramids():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    # test reading pyramidal metadata

    def test_imgcnv_pyramids_metadata_photoshop (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5000
        meta_test['image_num_y'] = 2853
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 4
        meta_test['image_resolution_level_scales'] = [1.0, 0.5, 0.25, 0.125]
        assert_metadata_read( "Photoshop pyramid", "monument_photoshop.tif", meta_test )

    def test_imgcnv_pyramids_metadata_vips (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5000
        meta_test['image_num_y'] = 2853
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 5
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624]
        assert_metadata_read( "Imagemagick pyramid", "monument_vips.tif", meta_test )

    def test_imgcnv_pyramids_metadata_tiff_top_ifd (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5000
        meta_test['image_num_y'] = 2853
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 7
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
        assert_metadata_read( "Top-IFD TIFF pyramid", "monument_imgcnv_topdirs.tif", meta_test )

    def test_imgcnv_pyramids_metadata_tiff_sub_ifd (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5000
        meta_test['image_num_y'] = 2853
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 7
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
        assert_metadata_read( "Sub-IFD TIFF pyramid", "monument_imgcnv_subdirs.tif", meta_test )

    def test_imgcnv_pyramids_metadata_ome_tiff_sub_ifd (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5000
        meta_test['image_num_y'] = 2853
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 7
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
        assert_metadata_read( "OME-TIFF pyramid", "monument_imgcnv.ome.tif", meta_test )

    def test_imgcnv_pyramids_metadata_tiff_256px (self):
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 5000
        meta_test['image_num_y'] = 2853
        meta_test['tile_num_x'] = 256
        meta_test['tile_num_y'] = 256
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 7
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0624,0.0312,0.0156]
        assert_metadata_read( "256px tiles TIFF pyramid", "monument_imgcnv.256.tif", meta_test )

        # test reading pyramidal levels
    def test_imgcnv_pyramids_levels_tiff_subifd (self):
        meta_test = {}
        meta_test['image_num_x'] = 156
        meta_test['image_num_y'] = 89
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv_subdirs.tif', meta_test )

    def test_imgcnv_pyramids_levels_tiff_topifd (self):
        meta_test = {}
        meta_test['image_num_x'] = 156
        meta_test['image_num_y'] = 89
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv_topdirs.tif', meta_test )

    def test_imgcnv_pyramids_levels_ome_tiff (self):
        meta_test = {}
        meta_test['image_num_x'] = 156
        meta_test['image_num_y'] = 89
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv.ome.tif', meta_test )

    def test_imgcnv_pyramids_levels_tiff_256px (self):
        meta_test = {}
        meta_test['image_num_x'] = 156
        meta_test['image_num_y'] = 89
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], 'monument_imgcnv.256.tif', meta_test )

    def test_imgcnv_pyramids_levels_vips (self):
        meta_test = {}
        meta_test['image_num_x'] = 312
        meta_test['image_num_y'] = 178
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '4'], 'monument_vips.tif', meta_test )

    def test_imgcnv_pyramids_levels_photoshop (self):
        meta_test = {}
        meta_test['image_num_x'] = 625
        meta_test['image_num_y'] = 357
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', 3], 'monument_photoshop.tif', meta_test )

    #-----------------------------------------------------------------
    # test reading pyramidal tiles
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_tiles_tif_subifd_tile_full (self):
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,2'], 'monument_imgcnv_subdirs.tif', meta_test ) # first tile

    def test_imgcnv_pyramids_tiles_tif_subifd_tile_edge (self):
        meta_test = {}
        meta_test['image_num_x'] = 226
        meta_test['image_num_y'] = 201
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,2,1,2'], 'monument_imgcnv_subdirs.tif', meta_test ) # last tile

    def test_imgcnv_pyramids_tiles_tif_subifd_tile_diff_size (self):
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,1,1,2'], 'monument_imgcnv_subdirs.tif', meta_test ) # tile size different from stored

    #-----------------------------------------------------------------
    # vips tiff
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_tiles_vips_tile_full (self):
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,2'], 'monument_vips.tif', meta_test ) # first tile

    def test_imgcnv_pyramids_tiles_vips_tile_edge (self):
        meta_test = {}
        meta_test['image_num_x'] = 226
        meta_test['image_num_y'] = 201
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,2,1,2'], 'monument_vips.tif', meta_test ) # last tile

    def test_imgcnv_pyramids_tiles_vips_tile_diff_size (self):
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,1,1,2'], 'monument_vips.tif', meta_test ) # tile size different from stored

    #-----------------------------------------------------------------
    # ome-tiff
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_tiles_ome_tiff_tile_full (self):
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,2'], 'monument_imgcnv.ome.tif', meta_test ) # first tile

    def test_imgcnv_pyramids_tiles_ome_tiff_tile_edge (self):
        meta_test = {}
        meta_test['image_num_x'] = 226
        meta_test['image_num_y'] = 201
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,2,1,2'], 'monument_imgcnv.ome.tif', meta_test ) # last tile

    def test_imgcnv_pyramids_tiles_ome_tiff_tile_diff_size (self):
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,1,1,2'], 'monument_imgcnv.ome.tif', meta_test ) # tile size different from stored

    #-----------------------------------------------------------------
    # jpeg2000
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_jpeg_2000_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 17334
        meta_test['image_num_y'] = 17457
        meta_test['tile_num_x'] = 2048
        meta_test['tile_num_y'] = 2048
        meta_test['image_pixel_depth'] = 8
        meta_test['image_num_resolution_levels'] = 8
        meta_test['image_resolution_level_scales'] = [1.0,0.5,0.25,0.125,0.0625,0.03125,0.015625,0.007812]
        meta_test['image_resolution_level_structure'] = 'flat'
        assert_metadata_read( "2048px tiles JPEG-2000 pyramid", "retina.jp2", meta_test )

    def test_imgcnv_pyramids_jpeg_2000_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 271
        meta_test['image_num_y'] = 273
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '6'], 'retina.jp2', meta_test )

    def test_imgcnv_pyramids_jpeg_2000_tile_full (self):
        # tiles
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,4'], 'retina.jp2', meta_test ) # first tile

    def test_imgcnv_pyramids_jpeg_2000_tile_edge (self):
        meta_test = {}
        meta_test['image_num_x'] = 59
        meta_test['image_num_y'] = 67
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,2,2,4'], 'retina.jp2', meta_test ) # last tile

        # testing tile size different from stored is not required for flat structure
        # ?

    #-----------------------------------------------------------------
    # czi
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_zeiss_czi_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 21787
        meta_test['image_num_y'] = 23863
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 256
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'RGB'
        meta_test['fov_height'] = 1200
        meta_test['fov_width'] = 1600
        meta_test['image_logical_x'] = -168462
        meta_test['image_logical_y'] = 48596
        meta_test['image_num_resolution_levels'] = 12
        meta_test['image_num_resolution_levels_actual'] = 6
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906,0.001953,0.000977,0.000488]
        meta_test['image_resolution_level_scales_actual'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['pixel_resolution_x'] = 0.22
        meta_test['pixel_resolution_y'] = 0.22
        meta_test['document/acquisition_date'] = '2014-09-23T19:36:35.1926747+02:00'
        meta_test['document/application'] = 'ZEN 2012 (blue edition) v1.1.2.0'
        meta_test['document/username'] = 'sxq748'
        meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['objectives/objective:0/numerical_aperture'] = '0.8'
        meta_test['objectives/objective:0/name'] = 'Plan-Apochromat 20x/0.8 M27'
        meta_test['channels/channel:1/acquisition_mode'] = 'WideField'
        meta_test['channels/channel:1/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel:1/contrast_method'] = 'Brightfield'
        meta_test['channels/channel:1/exposure'] = '0.2'
        meta_test['channels/channel:1/exposure_units'] = 'ms'
        meta_test['channels/channel:1/name'] = 'Green TL Brightfield (Dye1)'
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['coordinates/axis_orientation'] = 'ascending,descending'
        meta_test['coordinates/axis_origin'] = 'top_left'
        meta_test['coordinates/tie_points/bottom_right'] = '-146676,72458'
        meta_test['coordinates/tie_points/center'] = '-157568,60528'
        # meta_test['coordinates/tie_points/fovs/00000'] = '-158400,48600' # changed schema in 3.3.0 to contain all fovs in a list
        assert_metadata_read( "Zeiss CZI", "17.czi", meta_test )

    def test_imgcnv_pyramids_zeiss_czi_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 680
        meta_test['image_num_y'] = 745
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], '17.czi', meta_test )

    def test_imgcnv_pyramids_zeiss_czi_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,4'], '17.czi', meta_test ) # first tile

    def test_imgcnv_pyramids_zeiss_czi_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 337
        meta_test['image_num_y'] = 467
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,2,2,4'], '17.czi', meta_test ) # last tile

    def test_imgcnv_pyramids_zeiss_czi_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,0,0,4'], '17.czi', meta_test )

    #-----------------------------------------------------------------
    # czi_non_power_of_two
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_zeiss_czi_non_power_of_two_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 12000
        meta_test['image_num_y'] = 6000
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_num_fovs'] = 12
        meta_test['image_num_series'] = 1
        meta_test['image_mode'] = 'RGB'
        meta_test['fov_height'] = 2000
        meta_test['fov_width'] = 3000
        meta_test['image_logical_x'] = 0
        meta_test['image_logical_y'] = 0
        meta_test['image_num_resolution_levels'] = 11
        meta_test['image_num_resolution_levels_actual'] = 5
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906,0.001953,0.000977]
        meta_test['image_resolution_level_scales_actual'] = [1.000000,0.333333,0.111167,0.111111,0.037000]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['document/acquisition_date'] = '2012-01-13T15:59:03.30925+01:00'
        meta_test['channels/channel:1/color'] = '0.00,1.00,0.00'
        meta_test['channels/channel:1/name'] = 'Green C1 (Dye1)'
        assert_metadata_read( "Zeiss CZI", "MultiResolution-Mosaic.czi", meta_test )

    def test_imgcnv_pyramids_zeiss_czi_non_power_of_two_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 375
        meta_test['image_num_y'] = 187
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], 'MultiResolution-Mosaic.czi', meta_test )

    def test_imgcnv_pyramids_zeiss_czi_non_power_of_two_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,3'], 'MultiResolution-Mosaic.czi', meta_test ) # first tile

    def test_imgcnv_pyramids_zeiss_czi_non_power_of_two_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 476
        meta_test['image_num_y'] = 238
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,2,1,3'], 'MultiResolution-Mosaic.czi', meta_test ) # last tile

    def test_imgcnv_pyramids_zeiss_czi_non_power_of_two_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,0,0,4'], 'MultiResolution-Mosaic.czi', meta_test )

    #-----------------------------------------------------------------
    # imaris
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_imaris_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 2
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 64
        meta_test['image_num_x'] = 2048
        meta_test['image_num_y'] = 1567
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_num_resolution_levels'] = 4
        meta_test['image_resolution_level_structure'] = 'hierarchical'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000]
        meta_test['tile_num_x'] = 256
        meta_test['tile_num_y'] = 256
        meta_test['tile_num_original_x'] = 256
        meta_test['tile_num_original_y'] = 256
        meta_test['tile_num_original_z'] = 16
        meta_test['document/acquisition_date'] = '1991-10-01T16:45:45'
        meta_test['channels/channel:0/name'] = 'CollagenIV (TxRed)'
        meta_test['channels/channel:1/name'] = 'GFAP (FITC)'
        meta_test['pixel_resolution_x'] = 0.0229058
        meta_test['pixel_resolution_y'] = 0.0228985
        meta_test['pixel_resolution_z'] = 0.2
        meta_test['pixel_resolution_unit_x'] = 'um'
        assert_metadata_read( "Imaris", "retina_large.ims", meta_test )

    def test_imgcnv_pyramids_imaris_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 195
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '3'], 'retina_large.ims', meta_test )

    def test_imgcnv_pyramids_imaris_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'retina_large.ims', meta_test ) # first tile

    def test_imgcnv_pyramids_imaris_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 272
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,1,1,1'], 'retina_large.ims', meta_test ) # last tile

    def test_imgcnv_pyramids_imaris_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 2
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_p'] = 64
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,0,0,1'], 'retina_large.ims', meta_test )

    #-----------------------------------------------------------------
    # VQI
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_vqi_objects_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 84007
        meta_test['image_num_y'] = 28030
        meta_test['image_pixel_depth'] = 32 # support feature reading
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'mask'
        meta_test['image_num_resolution_levels'] = 9
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906]
        meta_test['tile_num_x'] = 28
        meta_test['tile_num_y'] = 256
        meta_test['image_series_paths/00000'] = '/cells'
        assert_metadata_read( "VQI", "sparse_dense.VQI.h5", meta_test )

    def test_imgcnv_pyramids_vqi_objects_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 1312
        meta_test['image_num_y'] = 437
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '6'], 'sparse_dense.VQI.h5', meta_test )

    def test_imgcnv_pyramids_vqi_objects_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 32
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'sparse_dense.VQI.h5', meta_test ) # first tile

    def test_imgcnv_pyramids_vqi_objects_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 65
        meta_test['image_num_y'] = 364
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,5,1,5'], 'sparse_dense.VQI.h5', meta_test ) # last tile

    def test_imgcnv_pyramids_vqi_objects_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 32
        assert_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], 'sparse_dense.VQI.h5', meta_test )

    def test_imgcnv_pyramids_vqi_objects_dense_fov (self):
        # fetching densified FOV
        meta_test = {}
        meta_test['image_num_x'] = 2040
        meta_test['image_num_y'] = 2040
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 32
        assert_image_commands( ['-t', 'tiff', '-slice', 'fov:2'], 'sparse_dense.VQI.h5', meta_test )

    def test_imgcnv_pyramids_vqi_objects_object (self):
        # fetching individual object
        meta_test = {}
        meta_test['image_num_x'] = 14
        meta_test['image_num_y'] = 18
        meta_test['image_num_c'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/cells/7'], 'sparse_dense.VQI.h5', meta_test )

    #-----------------------------------------------------------------
    # svs
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_aperio_svs_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 46000
        meta_test['image_num_y'] = 32914
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_resolution_levels'] = 3
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.249992,0.062498]
        meta_test['tile_num_x'] = 256
        meta_test['tile_num_y'] = 256
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['image_series_paths/00003'] = '/thumbnail'
        meta_test['pixel_resolution_x'] = 0.499
        meta_test['pixel_resolution_y'] = 0.499
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['document/quickhash'] = '30f1a38031fc0e21d81f9d01435ac4af848f6fe2bbf8f7768184336ee5d7e796'
        meta_test['ColorProfile/color_space'] = 'RGB'
        meta_test['ColorProfile/description'] = 'ScanScope v1'
        meta_test['ColorProfile/profile'] = 'embedded_icc'
        meta_test['ColorProfile/size'] = 141992
        meta_test['ColorProfile/version'] = '2.4'
        assert_metadata_read( "Aperio", "CMU-1.svs", meta_test )

    def test_imgcnv_pyramids_aperio_svs_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 2874
        meta_test['image_num_y'] = 2057
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '4'], 'CMU-1.svs', meta_test )

    def test_imgcnv_pyramids_aperio_svs_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'CMU-1.svs', meta_test ) # first tile

    def test_imgcnv_pyramids_aperio_svs_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 314
        meta_test['image_num_y'] = 9
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,5,4,4'], 'CMU-1.svs', meta_test ) # last tile

    def test_imgcnv_pyramids_aperio_svs_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], 'CMU-1.svs', meta_test )

    def test_imgcnv_pyramids_aperio_svs_preview (self):
        meta_test = {}
        meta_test['image_num_x'] = 1280
        meta_test['image_num_y'] = 431
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/preview'], 'CMU-1.svs', meta_test )

    def test_imgcnv_pyramids_aperio_svs_label (self):
        meta_test = {}
        meta_test['image_num_x'] = 387
        meta_test['image_num_y'] = 463
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/label'], 'CMU-1.svs', meta_test )

    #-----------------------------------------------------------------
    # ndpi
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_hamamatsu_ndpi_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 51200
        meta_test['image_num_y'] = 38144
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_resolution_levels'] = 9
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906]
        meta_test['pixel_resolution_x'] = 0.45641259698767683
        meta_test['pixel_resolution_y'] = 0.45506257110352671
        meta_test['tile_num_x'] = 256
        meta_test['tile_num_y'] = 256
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/preview'
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['document/quickhash'] = 'f14cd7dcf9dff2b7b3551672a1f3248bd35cc4a2e57e3ca47a2b83eac9ad3987'
        assert_metadata_read( "Hamamatsu", "CMU-1.ndpi", meta_test )

    def test_imgcnv_pyramids_hamamatsu_ndpi_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 3200
        meta_test['image_num_y'] = 2384
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '4'], 'CMU-1.ndpi', meta_test )

    def test_imgcnv_pyramids_hamamatsu_ndpi_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'CMU-1.ndpi', meta_test ) # first tile

    def test_imgcnv_pyramids_hamamatsu_ndpi_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 128
        meta_test['image_num_y'] = 336
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,6,4,4'], 'CMU-1.ndpi', meta_test ) # last tile

    def test_imgcnv_pyramids_hamamatsu_ndpi_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], 'CMU-1.ndpi', meta_test )

    def test_imgcnv_pyramids_hamamatsu_ndpi_preview (self):
        meta_test = {}
        meta_test['image_num_x'] = 1191
        meta_test['image_num_y'] = 408
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/preview'], 'CMU-1.ndpi', meta_test )

    #-----------------------------------------------------------------
    # philips
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_philips_meta (self):
        # meta
        meta_test = {}
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 155136
        meta_test['image_num_y'] = 95744
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_resolution_levels'] = 10
        meta_test['image_resolution_level_structure'] = 'arbitrary'
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250,0.015625,0.007812,0.003906,0.001953]
        meta_test['tile_num_x'] = 256
        meta_test['tile_num_y'] = 256
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['pixel_resolution_x'] = 0.25
        meta_test['pixel_resolution_y'] = 0.25
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        #meta_test['objectives/objective:0/magnification'] = '20'
        meta_test['document/quickhash'] = '9744e9ed2bd07f14b0f9f91a4e72de1c7578a1b850746979ae795ab784c603e7'
        assert_metadata_read( "Philips", "2017SM09941_3_EVG.tiff", meta_test )

    def test_imgcnv_pyramids_philips_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 4848
        meta_test['image_num_y'] = 2992
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], '2017SM09941_3_EVG.tiff', meta_test )

    def test_imgcnv_pyramids_philips_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], '2017SM09941_3_EVG.tiff', meta_test ) # first tile

    def test_imgcnv_pyramids_philips_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 240
        meta_test['image_num_y'] = 432
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,9,5,5'], '2017SM09941_3_EVG.tiff', meta_test ) # last tile

    def test_imgcnv_pyramids_philips_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,4,5,1'], '2017SM09941_3_EVG.tiff', meta_test )

    def test_imgcnv_pyramids_philips_preview (self):
        meta_test = {}
        meta_test['image_num_x'] = 1771
        meta_test['image_num_y'] = 830
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/preview'], '2017SM09941_3_EVG.tiff', meta_test )

    #-----------------------------------------------------------------
    # qptiff rgb
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_akoya_qptiff_rgb_meta (self):
        meta_test = {}
        meta_test['format'] = 'QPTIFF'
        meta_test['image_num_c'] = 3
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 30720
        meta_test['image_num_y'] = 26640
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'RGB'
        meta_test['image_num_labels'] = 1
        meta_test['image_num_previews'] = 1
        meta_test['image_num_resolution_levels'] = 5
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['tile_size_x'] = [512,512,512,512,1920]
        meta_test['tile_size_y'] = [512,512,512,512,1665]
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['image_series_paths/00003'] = '/thumbnail'
        meta_test['pixel_resolution_x'] = 0.498893
        meta_test['pixel_resolution_y'] = 0.498893
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/slide_id'] = 'HandEcompressed'
        meta_test['document/instrument_name'] = 'VectraPolaris 1.0'
        meta_test['document/application'] = 'VectraPolaris 1.0'
        meta_test['document/vendor'] = 'Akoya'
        meta_test['objectives/objective:0/name'] = '20x'
        meta_test['objectives/objective:0/magnification'] = 20
        meta_test['channels/channel:0/name'] = 'Red'
        meta_test['channels/channel:0/color'] = [1.0,0,0]
        meta_test['channels/channel:0/modality'] = 'Brightfield'
        meta_test['channels/channel:0/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:1/name'] = 'Green'
        meta_test['channels/channel:1/color'] = [0,1.0,0]
        meta_test['channels/channel:1/modality'] = 'Brightfield'
        meta_test['channels/channel:1/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:2/name'] = 'Blue'
        meta_test['channels/channel:2/color'] = [0,0,1.0]
        meta_test['channels/channel:2/modality'] = 'Brightfield'
        meta_test['channels/channel:2/camera'] = 'C11440-50U-53'
        assert_metadata_read( "Akoya QPTIFF", 'HandEcompressed_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_rgb_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 1920
        meta_test['image_num_y'] = 1665
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '4'], 'HandEcompressed_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_rgb_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'HandEcompressed_Scan1.qptiff', meta_test ) # first tile

    def test_imgcnv_pyramids_akoya_qptiff_rgb_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 258
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,7,6,3'], 'HandEcompressed_Scan1.qptiff', meta_test ) # last tile

    def test_imgcnv_pyramids_akoya_qptiff_rgb_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,4,5,4'], 'HandEcompressed_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_rgb_preview (self):
        # fetching preview
        meta_test = {}
        meta_test['image_num_x'] = 2084
        meta_test['image_num_y'] = 4024
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/preview'], 'HandEcompressed_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_rgb_label (self):
        # fetching label
        meta_test = {}
        meta_test['image_num_x'] = 521
        meta_test['image_num_y'] = 449
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/label'], 'HandEcompressed_Scan1.qptiff', meta_test )

    #-----------------------------------------------------------------
    # qptiff fluo
    #-----------------------------------------------------------------

    def test_imgcnv_pyramids_akoya_qptiff_5ch_meta (self):
        # Akoya QPTIFF - 5ch fluorescent
        meta_test = {}
        meta_test['format'] = 'QPTIFF'
        meta_test['image_num_c'] = 5
        meta_test['image_num_t'] = 1
        meta_test['image_num_z'] = 1
        meta_test['image_num_x'] = 24960
        meta_test['image_num_y'] = 34560
        meta_test['image_pixel_depth'] = 8
        meta_test['image_pixel_format'] = 'unsigned integer'
        meta_test['image_mode'] = 'multichannel'
        meta_test['image_num_labels'] = 1
        meta_test['image_num_previews'] = 1
        meta_test['image_num_resolution_levels'] = 6
        meta_test['image_resolution_level_scales'] = [1.000000,0.500000,0.250000,0.125000,0.062500,0.031250]
        meta_test['tile_num_x'] = 512
        meta_test['tile_num_y'] = 512
        meta_test['tile_size_x'] = [512,512,512,512,512,780]
        meta_test['tile_size_y'] = [512,512,512,512,512,1080]
        meta_test['image_series_paths/00000'] = '/image'
        meta_test['image_series_paths/00001'] = '/label'
        meta_test['image_series_paths/00002'] = '/preview'
        meta_test['image_series_paths/00003'] = '/thumbnail'
        meta_test['pixel_resolution_x'] = 0.497995
        meta_test['pixel_resolution_y'] = 0.497995
        meta_test['pixel_resolution_unit_x'] = 'microns'
        meta_test['pixel_resolution_unit_y'] = 'microns'
        meta_test['document/slide_id'] = 'LuCa-7color'
        meta_test['document/instrument_name'] = 'VectraPolaris 1.0'
        meta_test['document/application'] = 'VectraPolaris 1.0'
        meta_test['document/vendor'] = 'Akoya'
        meta_test['document/acquisition_date'] = '2017-09-25T18:54:15.6407059Z'
        meta_test['objectives/objective:0/name'] = '10x'
        meta_test['objectives/objective:0/magnification'] = 20

        meta_test['channels/channel:0/name'] = 'DAPI'
        meta_test['channels/channel:0/color'] = [0.00,0.00,1.00]
        meta_test['channels/channel:0/color_original'] = [0,0,255]
        meta_test['channels/channel:0/modality'] = 'Fluorescence'
        meta_test['channels/channel:0/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:0/datetime'] = '2017-09-25T18:54:15.6407059Z'
        meta_test['channels/channel:0/dye'] = 'DAPI'
        meta_test['channels/channel:0/fluor'] = 'DAPI'
        meta_test['channels/channel:0/exposure'] = 1592
        meta_test['channels/channel:0/exposure_units'] = 'us'
        meta_test['channels/channel:0/filter'] = 'Semrock:FF02-409/LP-25 Emission / Semrock:FF01-387/11-25 Excitation'

        meta_test['channels/channel:1/name'] = 'FITC'
        meta_test['channels/channel:1/color'] = [0.00,1.00,0.00]
        meta_test['channels/channel:1/color_original'] = [0,255,0]
        meta_test['channels/channel:1/modality'] = 'Fluorescence'
        meta_test['channels/channel:1/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:1/datetime'] = '2017-09-22T13:26:32.4325052Z'
        meta_test['channels/channel:1/dye'] = 'FITC'
        meta_test['channels/channel:1/fluor'] = 'FITC'
        meta_test['channels/channel:1/exposure'] = 2021
        meta_test['channels/channel:1/exposure_units'] = 'us'
        meta_test['channels/channel:1/filter'] = 'Chroma:ET510LP Emission / Chroma:ET480/40x Excitation'

        meta_test['channels/channel:2/name'] = 'CY3'
        meta_test['channels/channel:2/color'] = [1.00,1.00,0.00]
        meta_test['channels/channel:2/color_original'] = [255,255,0]
        meta_test['channels/channel:2/modality'] = 'Fluorescence'
        meta_test['channels/channel:2/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:2/datetime'] = '2017-09-22T13:29:37.4446619Z'
        meta_test['channels/channel:2/dye'] = 'CY3'
        meta_test['channels/channel:2/fluor'] = 'CY3'
        meta_test['channels/channel:2/exposure'] = 11621
        meta_test['channels/channel:2/exposure_units'] = 'us'
        meta_test['channels/channel:2/filter'] = 'Chroma:HQ572LP Emission / Chroma:HQ545/30x Excitation'

        meta_test['channels/channel:3/name'] = 'Texas Red'
        meta_test['channels/channel:3/color'] = [1.00,0.50,0.00]
        meta_test['channels/channel:3/color_original'] = [255,128,0]
        meta_test['channels/channel:3/modality'] = 'Fluorescence'
        meta_test['channels/channel:3/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:3/datetime'] = '2017-09-22T14:37:23.8128330Z'
        meta_test['channels/channel:3/dye'] = 'Texas Red'
        meta_test['channels/channel:3/fluor'] = 'Texas Red'
        meta_test['channels/channel:3/exposure'] = 48317
        meta_test['channels/channel:3/exposure_units'] = 'us'
        meta_test['channels/channel:3/filter'] = 'Semrock:FF01-657/106-25 Emission / Semrock:FF01-578/22-25 Excitation'

        meta_test['channels/channel:4/name'] = 'CY5'
        meta_test['channels/channel:4/color'] = [1.00,0.00,0.00]
        meta_test['channels/channel:4/color_original'] = [255,0,0]
        meta_test['channels/channel:4/modality'] = 'Fluorescence'
        meta_test['channels/channel:4/camera'] = 'C11440-50U-53'
        meta_test['channels/channel:4/datetime'] = '2017-09-22T13:36:18.6176751Z'
        meta_test['channels/channel:4/dye'] = 'CY5'
        meta_test['channels/channel:4/fluor'] = 'CY5'
        meta_test['channels/channel:4/exposure'] = 3180
        meta_test['channels/channel:4/exposure_units'] = 'us'
        meta_test['channels/channel:4/filter'] = 'Chroma:ET700/75m Emission / Chroma:ET620/60x Excitation'

        assert_metadata_read( "Akoya QPTIFF", 'LuCa-7color_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_5ch_levels (self):
        # levels
        meta_test = {}
        meta_test['image_num_x'] = 780
        meta_test['image_num_y'] = 1080
        meta_test['image_num_c'] = 5
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-res-level', '5'], 'LuCa-7color_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_5ch_tile_full (self):
        # tile full
        meta_test = {}
        meta_test['image_num_x'] = 512
        meta_test['image_num_y'] = 512
        meta_test['image_num_c'] = 5
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,0,0,1'], 'LuCa-7color_Scan1.qptiff', meta_test ) # first tile

    def test_imgcnv_pyramids_akoya_qptiff_5ch_tile_edge (self):
        # tile edge
        meta_test = {}
        meta_test['image_num_x'] = 24
        meta_test['image_num_y'] = 112
        meta_test['image_num_c'] = 5
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '512,3,4,4'], 'LuCa-7color_Scan1.qptiff', meta_test ) # last tile

    def test_imgcnv_pyramids_akoya_qptiff_5ch_tile_diff_size (self):
        # tile size different from stored - in this case the structure is virtual
        meta_test = {}
        meta_test['image_num_x'] = 256
        meta_test['image_num_y'] = 256
        meta_test['image_num_c'] = 5
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-tile', '256,1,1,5'], 'LuCa-7color_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_5ch_preview (self):
        # fetching preview
        meta_test = {}
        meta_test['image_num_x'] = 2090
        meta_test['image_num_y'] = 4036
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/preview'], 'LuCa-7color_Scan1.qptiff', meta_test )

    def test_imgcnv_pyramids_akoya_qptiff_5ch_label (self):
        # fetching label
        meta_test = {}
        meta_test['image_num_x'] = 522
        meta_test['image_num_y'] = 450
        meta_test['image_num_c'] = 3
        meta_test['image_num_z'] = 1
        meta_test['image_num_t'] = 1
        meta_test['image_pixel_depth'] = 8
        assert_image_commands( ['-t', 'tiff', '-path', '/label'], 'LuCa-7color_Scan1.qptiff', meta_test )
