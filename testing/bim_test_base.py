#!/usr/bin/python

# The following command line paratemeters can be used in any combination:
# all     - execute all tests
# reading - execute reading tests
# writing - execute writing tests
# meta    - execute metadata reading tests
# video   - execute video reading and metadata tests

""" imgcnv testing framework
"""
__author__    = "Dmitry Fedorov"
__version__   = "3.0.0"
__copyright__ = "ViQi Inc"

import os
from subprocess import Popen, PIPE #, call
import time
import urllib.request #, urllib.parse, urllib.error
import posixpath
import datetime
import pytz
from dateutil.parser import parse as date_parse

IMGCNV = os.getenv('BIM_TEST_PATH_TO_IMGCNV', './imgcnv')
if os.name == 'nt':
    IMGCNV = os.getenv('BIM_TEST_PATH_TO_IMGCNV', './imgcnv.exe')

IMGCNVVER = "3.0.0"
url_image_store = os.getenv('BIM_TEST_IMAGESTORE_DOWNLOAD_URL', 'https://s3-us-west-2.amazonaws.com/viqi-test-images/')
local_store_images  = os.getenv('BIM_TEST_PATH_TO_LOCAL_IMAGESTORE', 'images')
local_store_tests   = os.getenv('BIM_TEST_PATH_TO_LOCAL_TEST_FILES', 'tests')
offline_mode        = os.getenv('BIM_TEST_OFFLINE_MODE', 'False').lower() == 'true'


#-----------------------------------------------------------------------------
# setup
#-----------------------------------------------------------------------------

def create_local_dirs():
    if not os.path.exists(local_store_images):
        os.mkdir(local_store_images)
    if not os.path.exists(local_store_tests):
        os.mkdir(local_store_tests)

def fetch_file(filename):
    url = posixpath.join(url_image_store, urllib.parse.quote_plus( filename ) )
    path = os.path.join(local_store_images, filename)
    if not offline_mode:
        if not os.path.exists(path):
            # print("{}".format(path))
            urllib.request.urlretrieve(url, path)
        # else:
        #     print("{} [using local copy]".format(path))
    if not os.path.exists(path):
        print ('!!! Could not find required test image: "%s" !!!'%path)
    return path

def setup_tests():
    check_version ( IMGCNVVER )
    create_local_dirs()
    download_test_data()


###############################################################
# misc
###############################################################
def version ():
    imgcnvver = Popen ([IMGCNV, '-v'],stdout=PIPE).communicate()[0]
    for line in imgcnvver.splitlines():
        line = safedecode(line)
        if not line or line.startswith('Input'):
            return False
        return line.replace('\n', '')

def check_version ( needed ):
    inst = version()
    if not inst:
        raise Exception('imgcnv was not found')

    inst_ver = inst.split('.')
    need_ver = needed.split('.')
    if int(inst_ver[0])<int(need_ver[0]) or int(inst_ver[0])==int(need_ver[0]) and int(inst_ver[1])<int(need_ver[1]):
        raise Exception('Imgcnv needs update! Has: '+inst+' Needs: '+needed)

def safedecode(s, encoding='utf-8'):
    if isinstance(s, str) is True:
        return s
    try:
        return s.decode(encoding)
    except UnicodeDecodeError:
        try:
            return s.decode('utf-8')
        except UnicodeDecodeError:
            try:
                return s.decode('latin-1')
            except UnicodeDecodeError:
                return s.decode('ascii', 'replace')

def safe_filename(fn):
    return fn.replace('/', '.').replace('\\', '.').replace(':', '.')

def parse_imgcnv_info(s):
    d = {}
    for l in s.splitlines():
        l = safedecode(l)
        kk = l.split(': ', 1)
        if len(kk)<2:
            continue
        k = kk[0]
        v = kk[1]
        k = safedecode(k, 'utf-8').replace('%3A', ':')
        v = safedecode(v, 'utf-8').replace('\n', '').replace('%3E', '>').replace('%3C', '<').replace('%3A', ':').replace('%22', '"').replace('%0A', '\n')
        d[k] = v
    return d


def copy_keys(dict_in, keys_in):
    dict_out = {}
    for k in keys_in:
        if k in dict_in:
            dict_out[k] = dict_in[k]
    return dict_out
    #return { ?zip(k,v)? for k in keys_in if k in dict_in }

def compare_datetime(dt1, dt2):
    try:
        tz = ('%+06.2f'%(time.timezone/-3600.0)).replace('.', '')

        if isinstance(dt1, (datetime.datetime, datetime.time, datetime.date)) is not True:
            dt1 = date_parse('%s%s'%(dt1, tz))

        if isinstance(dt2, (datetime.datetime, datetime.time, datetime.date)) is not True:
            dt2 = date_parse('%s%s'%(dt2, tz))

        dt1 = dt1.astimezone(tz=pytz.utc)
        dt2 = dt2.astimezone(tz=pytz.utc)
        return (dt1==dt2)
    except Exception:
        return False

def compare_iterables(v1, v2):
    #print v1
    #print v2
    if isinstance(v1, list) is not True:
        v1 = v1.split(',')
    if isinstance(v2, list) is not True:
        v2 = v2.split(',')
    #print v1
    #print v2
    if len(v1) != len(v2):
        return False
    for i in range(len(v1)):
        #print v1[i]
        #print v2[i]
        if compare_values(v1[i], v2[i]) is not True:
            return False
    return True

def compare_values(iv, tv):
    try:
        if isinstance(tv, int):
            return (int(iv)==tv)
        if isinstance(tv, (float)):
            return (float(iv)==tv)
        if isinstance(tv, (datetime.datetime, datetime.time, datetime.date)):
            return compare_datetime(iv, tv)
        if isinstance(tv, list):
            return compare_iterables(iv, tv)
    except Exception:
        pass
    return (iv==tv)

###############################################################
# info comparisons
###############################################################

class InfoComparator(object):
    '''Compares two info dictionaries'''

    def compare(self, iv, tv):
        return False

    def fail_str(self, k, iv, tv, filename='', command=''):
        return '"%s" failed comparison: (computed) "%s" != "%s" (expected) during %s'%(k, iv, tv, str(command))

class InfoEquality(InfoComparator):

    def compare(self, iv, tv):
        return compare_values(iv, tv)

    def fail_str(self, k, iv, tv, filename='', command=''):
        return '"%s" failed comparison: (computed) "%s" != "%s" (expected) during %s'%(k, iv, tv, str(command))

class InfoNumericLessEqual(InfoComparator):

    def compare(self, iv, tv):
        return (int(iv)<=int(tv))

    def fail_str(self, k, iv, tv, filename='', command=''):
        return '"%s" failed comparison: (computed) "%s" != "%s" (expected) during %s'%(k, iv, tv, str(command))

def assert_compare_info(info, test, cc=InfoEquality(), command='' ):
    filename = info.get('filename', '')
    for tk in test:
        assert tk in info, '"%s" not found in info during %s'%(tk, str(command))
        assert cc.compare(info.get(tk), test.get(tk)), cc.fail_str( tk, info.get(tk), test.get(tk), filename, command=command)

def standardize_meta_fields(meta):
    if meta.get('pixel_resolution_unit_x') == 'um':
        meta['pixel_resolution_unit_x'] = 'microns'
    if meta.get('pixel_resolution_unit_y') == 'um':
        meta['pixel_resolution_unit_y'] = 'microns'
    return meta

###############################################################
# TESTS
###############################################################

def assert_image_read( fmt, filename ):

    #------------------------------------------------------------------------
    # reading and converting into TIFF
    #------------------------------------------------------------------------
    out_name = '%s/_test_converting_%s.tif'%(local_store_tests, filename)
    thumb_name = '%s/_test_thumbnail_%s.jpg'%(local_store_tests, filename)
    out_fmt = 'tiff'
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_org = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading metadata failed due to unsupported format'
    assert len(info_org) > 0, 'Reading metadata failed with empty result'
    assert 'width' in info_org, 'Metadata does not contain "width"'
    assert int(info_org['width']) > 1, 'Width is less than 2px'

    # convert the file into TIFF
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading converted metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading converted metadata failed due to unsupported format'
    assert len(info_cnv) > 0, 'Reading converted metadata failed with empty result'

    # test if converted file has same info
    info_test = copy_keys(info_cnv, ('pages', 'channels', 'width', 'height', 'depth'))
    assert_compare_info(info_org, info_test, command=command)

    #------------------------------------------------------------------------
    # Writing thumbnail
    #------------------------------------------------------------------------
    command = [IMGCNV, '-i', filename, '-o', thumb_name, '-t', 'jpeg', '-depth', '8,d', '-page', '1', '-display', '-resize', '128,128,BC,AR']
    r = Popen (command, stdout=PIPE).communicate()[0]

    command = [IMGCNV, '-i', thumb_name, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_thb = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading thumbnail metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading converted metadata failed due to unsupported format'
    assert len(info_thb) > 0, 'Reading thumbnail metadata failed with empty result'

    # if compare_info(info_thb, {'pages':1, 'channels':3, 'depth':8}, command=command, inv=True ) is True:
    #     if compare_info(info_thb, {'width': 128, 'height': 128}, InfoNumericLessEqual(), command=command, inv=True) is True:
    #         print_passed('thumbnail geometry')

    assert_compare_info(info_thb, {'pages':1, 'channels':3, 'depth':8}, command=command)
    assert_compare_info(info_thb, {'width': 128, 'height': 128}, InfoNumericLessEqual(), command=command)

def assert_image_write( fmt, filename ):

    out_name = '%s/_test_writing_%s.%s'%(local_store_tests, filename, fmt)
    out_fmt = fmt
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_org = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading original image failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading original image failed due to unsupported format'
    assert len(info_org) > 0, 'Reading original metadata failed with empty result'

    # convert the file into format
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-info']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading written image failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading written image failed due to unsupported format'
    assert len(info_cnv) > 0, 'Reading written metadata failed with empty result'

    # test if converted file has same info
    info_test = copy_keys(info_cnv, ('pages', 'channels', 'width', 'height', 'depth'))
    assert_compare_info(info_org, info_test, command=command)


def assert_video_write( fmt, filename, fps_test=15 ):

    out_name = '%s/_test_writing_%s.%s'%(local_store_tests, filename, fmt)
    out_fmt = fmt
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_org = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading original image failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading original image failed due to unsupported format'
    assert len(info_org) > 0, 'Reading original metadata failed with empty result'

    # convert the file into format
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt, '-depth', '8,d,u', '-options', 'fps 15']
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-meta']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading written image failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading written image failed due to unsupported format'
    assert len(info_cnv) > 0, 'Reading written metadata failed with empty result'

    # test if converted file has same info
    info_org['video_frames_per_second'] = str(fps_test)
    info_test = copy_keys(info_cnv, ('image_num_x', 'image_num_y', 'video_frames_per_second'))
    assert_compare_info(info_org, info_test, command=command)


def assert_image_metadata( fmt, filename, meta_test, meta_test_cnv=None):
    out_name = '%s/_test_metadata_%s.ome.tif'%(local_store_tests, filename)
    out_fmt = 'ome-tiff'
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_org = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading metadata failed due to unsupported format'
    assert len(meta_org) > 0, 'Reading metadata failed with empty result'

    # test if converted file has same info
    assert_compare_info(meta_org, meta_test, command=command)

    # convert the file into format
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading metadata failed due to unsupported format'
    assert len(meta_cnv) > 0, 'Reading metadata failed with empty result'

    if meta_test_cnv is None:
        meta_test_cnv=meta_test
    meta_test_cnv = standardize_meta_fields(meta_test_cnv)
    assert_compare_info(meta_cnv, meta_test_cnv, command=command)

def assert_metadata_read( fmt, filename, meta_test, extra=None ):

    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta']
    if extra is not None:
        command.extend(extra)
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_org = parse_imgcnv_info(r)
    r = safedecode(r)
    meta_org['filename'] = filename

    assert r is not None, 'Reading metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading metadata failed due to unsupported format'
    assert len(meta_org) > 0, 'Reading metadata failed with empty result'

    assert_compare_info(meta_org, meta_test, command=command)

def assert_image_video( fmt, filename, meta_test  ):
    filename = '%s/%s'%(local_store_images, filename)

    # test if file can be red
    command = [IMGCNV, '-i', filename, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    meta_org = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading metadata failed due to unsupported format'
    assert len(meta_org) > 0, 'Reading metadata failed with empty result'

    # test if converted file has same info
    assert_compare_info(meta_org, meta_test, command=command)

def assert_image_transforms( transform_type, transform, filename, meta_test  ):
    fmt = 'tiff'
    out_name = '%s/_test_transform_%s%s_%s.%s'%(local_store_tests, filename, transform_type, transform, fmt)
    out_fmt = fmt
    filename = '%s/%s'%(local_store_images, filename)

    # convert the file into transform
    command = [IMGCNV, '-i', filename, '-o', out_name, '-t', out_fmt, transform_type, transform]
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command = [IMGCNV, '-i', out_name, '-meta-parsed']
    r = Popen (command, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading transformed metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading transformed metadata failed due to unsupported format'
    assert len(info_cnv) > 0, 'Reading transformed metadata failed with empty result'

    # test if converted file has same info
    assert_compare_info(info_cnv, meta_test, command=command)

def assert_image_commands( extra, filename, meta_test  ):
    extra = [str(i) for i in extra]
    fmt = 'tiff'

    out_name = '%s/_test_command_%s_%s.%s'%(local_store_tests, filename, safe_filename('_'.join(extra)), fmt)
    out_fmt = fmt
    filename = '%s/%s'%(local_store_images, filename)

    # convert the file into transform
    command = [IMGCNV, '-i', filename, '-o', out_name]
    if '-t' not in extra:
        command.extend(['-t', out_fmt])
    command.extend(extra)
    r = Popen (command, stdout=PIPE).communicate()[0]

    # get info from the converted file
    command2 = [IMGCNV, '-i', out_name, '-meta-parsed']
    r = Popen (command2, stdout=PIPE).communicate()[0]
    info_cnv = parse_imgcnv_info(r)
    r = safedecode(r)

    assert r is not None, 'Reading written metadata failed with empty result'
    assert r.startswith('Input format is not supported') is False, 'Reading written metadata failed due to unsupported format'
    assert len(info_cnv) > 0, 'Reading written metadata failed with empty result'

    # test if converted file has same info
    assert_compare_info(info_cnv, meta_test, command=command)

#-----------------------------------------------------------------------------
# test data
#-----------------------------------------------------------------------------

def download_test_data():
    fetch_file('0022.zvi')
    fetch_file('040130Topography001.tif')
    fetch_file('107_07661.bmp')
    fetch_file('112811B_5.oib')
    fetch_file('122906_3Ax(inverted).avi')
    fetch_file('161pkcvampz1Live2-17-2004_11-57-21_AM.tif')
    fetch_file('23D3HA-cy3 psd-gfp-488 Homer-647 DIV 14 - 3.zvi')
    fetch_file('241aligned.avi')
    fetch_file('3Dstack.tif.3D.mov')
    fetch_file('7.5tax10.stk')
    fetch_file('A01.jpg')
    fetch_file('autocorrelation.tif')
    fetch_file('AXONEME.002')
    fetch_file('B01C0201.M2T')
    fetch_file('B4nf.RS.RE.z1.5.15.06.3DAnimation.avi')
    fetch_file('bigtiff.ome.btf')
    fetch_file('cells.ome.tif')
    fetch_file('combinedsubtractions.lsm')
    fetch_file('CRW_0136.CRW')
    fetch_file('CRW_0136_COMPR.dng')
    fetch_file('CSR 4 mo COX g M cone r PNA b z1.oib')
    fetch_file('DSCN0041.NEF')
    fetch_file('EleanorRigby.mpg')
    fetch_file('flowers_24bit_hsv.tif')
    fetch_file('flowers_24bit_nointr.png')
    fetch_file('flowers_8bit_gray.png')
    fetch_file('flowers_8bit_palette.png')
    fetch_file('Girsh_path3.m2ts')
    fetch_file('HEK293_Triple_Dish1_set_9.lsm')
    fetch_file('IMG_0040.CR2')
    fetch_file('IMG_0184.JPG')
    fetch_file('IMG_0488.JPG')
    fetch_file('IMG_0562.JPG')
    fetch_file('IMG_0593.JPG')
    fetch_file('IMG_1003.JPG')
    fetch_file('K560-tax-6-7-7-1.stk')
    fetch_file('Live_10-3-2009_6-44-12_PM.tif')
    fetch_file('MB_10X_20100303.oib')
    fetch_file('MDD2-7.stk')
    fetch_file('MF Mon 2x2.tif')
    fetch_file('Muller cell z4.oib.3D.mov')
    fetch_file('MZ2.PIC')
    fetch_file('out.avi')
    fetch_file('out.flv')
    fetch_file('out.m4v')
    fetch_file('out.mjpg')
    fetch_file('out.mkv')
    fetch_file('out.mov')
    fetch_file('out.mpg')
    fetch_file('out.ogg')
    fetch_file('out.swf')
    fetch_file('out.vcd')
    fetch_file('out.webm')
    fetch_file('out.wmv')
    fetch_file('out_h264.mp4')
    fetch_file('out_h265.mp4')
    fetch_file('P1110010.ORF')
    fetch_file('PENTAX_IMGP1618.JPG')
    fetch_file('PICT1694.MRW')
    fetch_file('radiolaria.avi')
    fetch_file('Retina 4 top.oib')
    fetch_file('Step_into_Liquid_1080.wmv')
    fetch_file('sxn3_w1RGB-488nm_s1.stk')
    fetch_file('test z1024 Image0004.oib')
    fetch_file('test.ogv')
    fetch_file('test16bit.btf')
    fetch_file('tubule20000.ibw')
    fetch_file('Untitled_MMImages_Pos0.ome.tif')
    fetch_file('wta.ome.tif')
    fetch_file('monument_imgcnv.256.tif')
    fetch_file('monument_imgcnv.ome.tif')
    fetch_file('monument_imgcnv_subdirs.tif')
    fetch_file('monument_imgcnv_topdirs.tif')
    fetch_file('monument_photoshop.tif')
    fetch_file('monument_vips.tif')
    fetch_file('10')
    fetch_file('MR-MONO2-8-16x-heart')
    fetch_file('US-MONO2-8-8x-execho')
    fetch_file('US-PAL-8-10x-echo')
    fetch_file('0015.DCM')
    fetch_file('0020.DCM')
    fetch_file('ADNI_002_S_0295_MR_3-plane_localizer__br_raw_20060418193538653_1_S13402_I13712.dcm')
    fetch_file('BetSog_20040312_Goebel_C2-0001-0001-0001.dcm')
    fetch_file('IM-0001-0001.dcm')
    fetch_file('test4.tif')

    fetch_file('16_day1_1_patient_29C93FK6_1.nii')
    fetch_file('filtered_func_data.nii')
    fetch_file('newsirp_final_XML.nii')
    fetch_file('avg152T1_LR_nifti.hdr')
    fetch_file('avg152T1_LR_nifti.img')
    fetch_file('T1w.nii.gz')
    fetch_file('219j_q050.jxr')
    fetch_file('219j_q100.jxr')
    fetch_file('219j_q080.webp')
    fetch_file('219j_q100.webp')
    fetch_file('219j.jp2')

    fetch_file('IMG_1913_16bit_prophoto_ts1024_q90.jp2')
    fetch_file('retina.jp2')
    fetch_file('IMG_1913_16bit_prophoto_q90.jxr')
    fetch_file('IMG_1913_prophoto_q90.webp')

    fetch_file('6J0A3548.CR2')
    fetch_file('DR3A1199.CR3')
    fetch_file('IMG_0184_RGBA.png')

    fetch_file('20150917_05195_DNA-TET-25k-DE20_raw.region_000.sum-all_003-072.mrc')
    fetch_file('29kx_30epi_058_aligned.mrc')
    fetch_file('golgi.mrc')
    fetch_file('Tile_19491580_0_1.mrc')
    fetch_file('dual.rec')
    fetch_file('EMD-3001.map')
    fetch_file('EMD-3197.map')

    # pyramids
    fetch_file('17.czi')
    fetch_file('MultiResolution-Mosaic.czi')
    fetch_file('CMU-1.svs')
    fetch_file('CMU-1.ndpi')
    fetch_file('2017SM09941_3_EVG.tiff')

    #stacks
    fetch_file('16Bit-ZStack.czi')
    fetch_file('40x_RatBrain-AT-2ch-Z-wf.czi')
    fetch_file('8Bit-ZStack.czi')
    fetch_file('BPAE-cells-bin2x2_3chTZ(WF).czi')
    fetch_file('CZT-Stack-Anno.czi')
    fetch_file('Mouse_stomach_20x_ROI_3chZTiles(WF).czi')
    fetch_file('Z-Stack.czi')
    fetch_file('Z-Stack-Anno(RGB).czi')

    # hdf5, Dream3d, Imaris, VQI
    fetch_file('ex_image2.h5')
    fetch_file('ex_image3.h5')
    fetch_file('NEONDSImagingSpectrometerData.h5')
    fetch_file('murks_lens.h5')
    fetch_file('2016-07-26_17.11.03_Sample Prefix_5P298C2_Protocol.ims')
    fetch_file('R18Demo.ims')
    fetch_file('retina_large.ims')
    fetch_file('sparse_dense.VQI.h5')
    fetch_file('vqi_objects_CYX.h5')

    fetch_file('old_cells_02.nd2')
    fetch_file('sample_image.nd2')

    #MD HTD
    fetch_file('MD_HTD_OLD_w1.TIF')
    fetch_file('MD_HTD_XML_w1.TIF')

    # Cellomics
    fetch_file('AS_09125_050117050001_P24f00d0.DIB')
    fetch_file('localhost210727110002_B06f01d0.C01')

    # BIMR
    fetch_file('combinedsubtractions.bimr')

    # BioTek
    fetch_file('A3_01_1_2_Phase_Contrast_001.tif')

    # Akoya QPTIFF
    fetch_file('HnE_3_1x1component_data.tif')
    fetch_file('LuCa-7color_[13860,52919]_1x1component_data.tif')
    fetch_file('HandEcompressed_Scan1.qptiff') # large RGB pyramid
    fetch_file('LuCa-7color_Scan1.qptiff') # large 7ch pyramid

    # Keyence HCS
    fetch_file('Image_W096_P00025_CH4.tif')

    # PerkinElmer HCS
    fetch_file('r02c03f01p01-ch3sk1fk1fl1.tiff')

    # ThermoFisher EVOS
    fetch_file('scan_Plate_R_p00_0_H12f15d4.TIF')

    # BioTek CytationC10
    fetch_file('A1_02_1_13_Bright Field_001.tif')
