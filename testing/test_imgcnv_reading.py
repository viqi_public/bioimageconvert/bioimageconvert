#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_image_read

class TestBioImageConvertReading():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    def test_imgcnv_read_bmp (self):
        assert_image_read( "BMP", "107_07661.bmp" )

    def test_imgcnv_read_jpeg (self):
        assert_image_read( "JPEG", "A01.jpg" )

    def test_imgcnv_read_png_24bit (self):
        assert_image_read( "PNG", "flowers_24bit_nointr.png" )

    def test_imgcnv_read_png_8bit (self):
        assert_image_read( "PNG", "flowers_8bit_gray.png" )

    def test_imgcnv_read_png_8bit_palette (self):
        assert_image_read( "PNG", "flowers_8bit_palette.png" )

    def test_imgcnv_read_biorad_pic (self):
        assert_image_read( "BIORAD-PIC", "MZ2.PIC" )

    def test_imgcnv_read_fluoview_tiff (self):
        assert_image_read( "FLUOVIEW", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif" )

    def test_imgcnv_read_zeiss_lsm_2ch (self):
        assert_image_read( "Zeiss LSM", "combinedsubtractions.lsm" )

    def test_imgcnv_read_zeiss_lsm_1ch (self):
        assert_image_read( "Zeiss LSM 1 ch", "HEK293_Triple_Dish1_set_9.lsm" )

    def test_imgcnv_read_ome_tiff (self):
        assert_image_read( "OME-TIFF", "wta.ome.tif" )

    def test_imgcnv_read_tiff_float (self):
        assert_image_read( "TIFF float", "autocorrelation.tif" )

    def test_imgcnv_read_stk_time (self):
        assert_image_read( "STK", "K560-tax-6-7-7-1.stk" )

    def test_imgcnv_read_stk_time_2 (self):
        assert_image_read( "STK", "MDD2-7.stk" )

    def test_imgcnv_read_stk_zstack (self):
        assert_image_read( "STK", "sxn3_w1RGB-488nm_s1.stk" )

    def test_imgcnv_read_stk_time_3 (self):
        assert_image_read( "STK", "7.5tax10.stk" )

    def test_imgcnv_read_oib_zstack (self):
        assert_image_read( "OIB", "test z1024 Image0004.oib" )

    def test_imgcnv_read_oib_mosaic (self):
        assert_image_read( "OIB", "MB_10X_20100303.oib" )

    def test_imgcnv_read_nanoscope (self):
        assert_image_read( "NANOSCOPE", "AXONEME.002" )

    def test_imgcnv_read_ibw (self):
        assert_image_read( "IBW", "tubule20000.ibw" )

    def test_imgcnv_read_psia (self):
        assert_image_read( "PSIA", "040130Topography001.tif" )

    def test_imgcnv_read_bigtiff (self):
        assert_image_read( "BigTIFF", "test16bit.btf" )

    def test_imgcnv_read_ome_bigtiff (self):
        assert_image_read( "OME-BigTIFF", "bigtiff.ome.btf" )

    def test_imgcnv_read_zeiss_zvi (self):
        assert_image_read( "ZVI", "23D3HA-cy3 psd-gfp-488 Homer-647 DIV 14 - 3.zvi" )

    def test_imgcnv_read_zeiss_zvi_zstack (self):
        assert_image_read( "ZVI", "0022.zvi" )

    def test_imgcnv_read_dcraw_dng (self):
        assert_image_read( "Adobe DNG", "CRW_0136_COMPR.dng" )

    def test_imgcnv_read_jxr (self):
        assert_image_read( "JPEG-XR", "219j_q050.jxr" )

    def test_imgcnv_read_jxr_lossless (self):
        assert_image_read( "JPEG-XR LOSSLESS", "219j_q100.jxr" )

    def test_imgcnv_read_jpeg2000 (self):
        assert_image_read( "JPEG-2000", "219j.jp2" )

    def test_imgcnv_read_webp (self):
        assert_image_read( "WEBP", "219j_q080.webp" )

    def test_imgcnv_read_webp_lossless (self):
        assert_image_read( "WEBP LOSSLESS", "219j_q100.webp" )

    def test_imgcnv_read_dicom (self):
        assert_image_read( "DICOM", "10" )

    def test_imgcnv_read_dicom_2 (self):
        assert_image_read( "DICOM", "0015.DCM" )

    def test_imgcnv_read_dicom_3 (self):
        assert_image_read( "DICOM", "IM-0001-0001.dcm" )

    def test_imgcnv_read_nifti (self):
        assert_image_read( "NIFTI", "16_day1_1_patient_29C93FK6_1.nii" )

    def test_imgcnv_read_nifti_2 (self):
        assert_image_read( "NIFTI", "filtered_func_data.nii" )

    def test_imgcnv_read_nifti_xml (self):
        assert_image_read( "NIFTI", "newsirp_final_XML.nii" )

    def test_imgcnv_read_nifti_hdr (self):
        assert_image_read( "NIFTI", "avg152T1_LR_nifti.hdr" )

    def test_imgcnv_read_nifti_gz (self):
        assert_image_read( "NIFTI", "T1w.nii.gz" )

    # MRC

    def test_imgcnv_read_mrc (self):
        assert_image_read( "MRC", "Tile_19491580_0_1.mrc" )

    def test_imgcnv_read_mrc_2 (self):
        assert_image_read( "MRC", "golgi.mrc" )

    # HDF5, Imaris

    def test_imgcnv_read_hdf5_image (self):
        assert_image_read( "HDF5", "ex_image2.h5" )

    def test_imgcnv_read_hdf5_image2 (self):
        assert_image_read( "HDF5", "ex_image3.h5" )

    def test_imgcnv_read_dream3d (self):
        assert_image_read( "Dream3D", "murks_lens.h5" )

    def test_imgcnv_read_imaris (self):
        assert_image_read( "Imaris", "R18Demo.ims" )
