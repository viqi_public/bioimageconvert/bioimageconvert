#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_video_write

mode = []

class TestBioImageConvertWritingVideo():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    # def test_imgcnv_writevideo_avi (self):
    #     assert_video_write( "AVI", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_quicktime (self):
    #     assert_video_write( "QuickTime", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_mpeg4 (self):
    #     assert_video_write( "MPEG4", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_wmv (self):
    #     assert_video_write( "WMV", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_ogg (self):
    #     assert_video_write( "OGG", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_flv (self):
    #     assert_video_write( "FLV", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_h264 (self):
    #     assert_video_write( "H264", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_h265 (self):
    #     assert_video_write( "H265", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_mkv (self):
    #     assert_video_write( "Matroska", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_webm (self):
    #     assert_video_write( "WEBM", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)

    # def test_imgcnv_writevideo_webm9 (self):
    #     assert_video_write( "WEBM9", "161pkcvampz1Live2-17-2004_11-57-21_AM.tif", fps_test=15)
