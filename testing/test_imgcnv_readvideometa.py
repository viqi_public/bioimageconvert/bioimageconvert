#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_image_video

class TestBioImageConvertReadVideoMeta():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    # def test_imgcnv_videometa_avi_cinepak_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 101
    #     meta_test['image_num_p'] = 101
    #     meta_test['image_num_x'] = 720
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'cinepak'
    #     assert_image_video( "AVI CINEPAK", "122906_3Ax(inverted).avi", meta_test )

    # def test_imgcnv_videometa_avi_cinepak_20fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 37
    #     meta_test['image_num_p'] = 37
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 20
    #     meta_test['video_codec_name'] = 'cinepak'
    #     assert_image_video( "AVI CINEPAK", "241aligned.avi", meta_test )

    # def test_imgcnv_videometa_quicktime_mpeg4_30fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 199
    #     meta_test['image_num_p'] = 199
    #     meta_test['image_num_x'] = 826
    #     meta_test['image_num_y'] = 728
    #     meta_test['video_frames_per_second'] = 30
    #     meta_test['video_codec_name'] = 'mpeg4'
    #     assert_image_video( "QuickTime MPEG4", "3Dstack.tif.3D.mov", meta_test )

    # def test_imgcnv_videometa_avi_raw_30fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 73
    #     meta_test['image_num_p'] = 73
    #     meta_test['image_num_x'] = 1024
    #     meta_test['image_num_y'] = 947
    #     meta_test['video_frames_per_second'] = 30.303
    #     meta_test['video_codec_name'] = 'rawvideo'
    #     assert_image_video( "AVI RAW", "B4nf.RS.RE.z1.5.15.06.3DAnimation.avi", meta_test )

    # def test_imgcnv_videometa_mpeg1_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 2879 # 2878
    #     meta_test['image_num_p'] = 2879 # 2878
    #     meta_test['image_num_x'] = 352
    #     meta_test['image_num_y'] = 240
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'mpeg1video'
    #     assert_image_video( "MPEG", "EleanorRigby.mpg", meta_test )

    # def test_imgcnv_videometa_quicktime_mpeg4_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 301
    #     meta_test['image_num_p'] = 301
    #     meta_test['image_num_x'] = 884
    #     meta_test['image_num_y'] = 845
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'mpeg4'
    #     assert_image_video( "QuickTime MPEG4", "Muller cell z4.oib.3D.mov", meta_test )

    # def test_imgcnv_videometa_avi_raw_9fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 9
    #     meta_test['image_num_p'] = 9
    #     meta_test['image_num_x'] = 316
    #     meta_test['image_num_y'] = 400
    #     meta_test['video_frames_per_second'] = 9.00001
    #     meta_test['video_codec_name'] = 'rawvideo'
    #     assert_image_video( "AVI RAW", "radiolaria.avi", meta_test )

    # def test_imgcnv_videometa_wmv_24fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 2773
    #     meta_test['image_num_p'] = 2773
    #     meta_test['image_num_x'] = 1440
    #     meta_test['image_num_y'] = 1080
    #     meta_test['video_frames_per_second'] = 24
    #     meta_test['video_codec_name'] = 'wmv3'
    #     assert_image_video( "WMV", "Step_into_Liquid_1080.wmv", meta_test )

    # def test_imgcnv_videometa_ogg_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 199
    #     meta_test['image_num_p'] = 199
    #     meta_test['image_num_x'] = 826
    #     meta_test['image_num_y'] = 728
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'theora'
    #     assert_image_video( "OGG", "test.ogv", meta_test )

    # def test_imgcnv_videometa_mpeg2ts_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 36777 # 36776
    #     meta_test['image_num_p'] = 36777 # 36776
    #     meta_test['image_num_x'] = 1440
    #     meta_test['image_num_y'] = 1080
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'mpeg2video'
    #     assert_image_video( "MPEG2 TS (1)", "B01C0201.M2T", meta_test )

    # def test_imgcnv_videometa_mpeg2ts_59fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_x'] = 1440
    #     meta_test['image_num_y'] = 1080
    #     meta_test['video_codec_name'] = 'h264'
    #     meta_test['video_frames_per_second'] = 59.9401
    #     meta_test['image_num_t'] = 17127
    #     assert_image_video( "MPEG2 TS (2)", "Girsh_path3.m2ts", meta_test )

    # def test_imgcnv_videometa_avi_mpeg4_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 28
    #     meta_test['image_num_p'] = 28
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'mpeg4'
    #     assert_image_video( "AVI MPEG4", "out.avi", meta_test )

    # def test_imgcnv_videometa_flv_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 30
    #     meta_test['image_num_p'] = 30
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'flv'
    #     assert_image_video( "Flash video", "out.flv", meta_test )

    # def test_imgcnv_videometa_h263_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 28
    #     meta_test['image_num_p'] = 28
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'mpeg4'
    #     assert_image_video( "MPEG4 H.263", "out.m4v", meta_test )

    # def test_imgcnv_videometa_mkv_h264_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 30
    #     meta_test['image_num_p'] = 30
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'h264'
    #     assert_image_video( "Matroska MPEG4 H.264", "out.mkv", meta_test )

    # def test_imgcnv_videometa_quicktime_h264_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 28
    #     meta_test['image_num_p'] = 28
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'h264'
    #     assert_image_video( "Quicktime MPEG4 H.264", "out.mov", meta_test )

    # # def test_imgcnv_videometa_mpeg2_29fps (self):
    # #    meta_test = {}
    # #    meta_test['image_num_z'] = 1
    # #    meta_test['image_num_t'] = 28
    # #    meta_test['image_num_p'] = 28
    # #    meta_test['image_num_x'] = 640
    # #    meta_test['image_num_y'] = 480
    # #    meta_test['video_frames_per_second'] = 29.97003
    # #    meta_test['video_codec_name'] = 'mpeg2video'
    # #    assert_image_video( "MPEG2", "out.mpg", meta_test )

    # def test_imgcnv_videometa_ogg_theora_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 31
    #     meta_test['image_num_p'] = 31
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'theora'
    #     assert_image_video( "OGG Theora", "out.ogg", meta_test )

    # def test_imgcnv_videometa_mpeg1_vcd_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 31 # 30
    #     meta_test['image_num_p'] = 31 # 30
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'mpeg1video'
    #     assert_image_video( "MPEG1", "out.vcd", meta_test )

    # def test_imgcnv_videometa_wmv_msmpeg4_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 27
    #     meta_test['image_num_p'] = 27
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'msmpeg4'
    #     assert_image_video( "WMV", "out.wmv", meta_test )

    # def test_imgcnv_videometa_webm_vp8_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 30
    #     meta_test['image_num_p'] = 30
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'vp8'
    #     assert_image_video( "WebM", "out.webm", meta_test )

    # def test_imgcnv_videometa_mp4_h264_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 28
    #     meta_test['image_num_p'] = 28
    #     meta_test['image_num_x'] = 640
    #     meta_test['image_num_y'] = 480
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'h264'
    #     assert_image_video( "MPEG4 AVC H.264", "out_h264.mp4", meta_test )

    # def test_imgcnv_videometa_mp4_h265_29fps (self):
    #     meta_test = {}
    #     meta_test['image_num_z'] = 1
    #     meta_test['image_num_t'] = 25
    #     meta_test['image_num_p'] = 25
    #     meta_test['image_num_x'] = 1920
    #     meta_test['image_num_y'] = 1080
    #     meta_test['image_num_c'] = 3
    #     meta_test['video_frames_per_second'] = 29.97
    #     meta_test['video_codec_name'] = 'hevc'
    #     assert_image_video( "MPEG4 AVC H.265", "out_h265.mp4", meta_test )
