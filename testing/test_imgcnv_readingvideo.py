#!/usr/bin/python
__author__    = "Dmitry Fedorov"
__copyright__ = "ViQi Inc"

from bim_test_base import setup_tests, assert_image_read

class TestBioImageConvertReading():

    @classmethod
    def setup_class(cls):
        setup_tests()

    @classmethod
    def teardown_class(cls):
        pass

    #-----------------------------------------------------------------
    # Tests
    #-----------------------------------------------------------------

    # def test_imgcnv_read_quicktime (self):
    #     assert_image_read( "QuickTime", "3Dstack.tif.3D.mov" )

    # def test_imgcnv_read_avi (self):
    #     assert_image_read( "AVI", "radiolaria.avi" )

    # def test_imgcnv_read_ogg (self):
    #     assert_image_read( "OGG", "test.ogv" )

    # def test_imgcnv_read_flv (self):
    #     assert_image_read( "FLV", "out.flv" )

    # def test_imgcnv_read_wmv (self):
    #     assert_image_read( "WMV", "out.wmv" )

    # def test_imgcnv_read_vcd (self):
    #     assert_image_read( "VCD", "out.vcd" )

    # def test_imgcnv_read_mjpeg (self):
    #     assert_image_read( "MJPEG", "out.mjpg" )

    # def test_imgcnv_read_mkv (self):
    #     assert_image_read( "Matroska", "out.mkv" )

    # # def test_imgcnv_read_mpg1 (self):
    # #     assert_image_read( "MPEG1", "EleanorRigby.mpg" )

    # def test_imgcnv_read_m4v (self):
    #     assert_image_read( "MPEG4", "out.m4v" )

    # def test_imgcnv_read_h264 (self):
    #     assert_image_read( "H264", "out_h264.mp4" )

    # def test_imgcnv_read_h265 (self):
    #     assert_image_read( "H265", "out_h265.mp4" )

    # def test_imgcnv_read_webm (self):
    #     assert_image_read( "WebM", "out.webm" )
