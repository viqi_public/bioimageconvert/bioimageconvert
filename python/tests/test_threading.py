import sys
import os
import numpy as np
from concurrent.futures import ThreadPoolExecutor
import concurrent.futures as futures

PATH_CURRENT = os.path.dirname(os.path.realpath(__file__))
PATH_FILES = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/images'))
PATH_TESTS = os.path.abspath(os.path.join(PATH_CURRENT, '../../testing/tests'))

sys.path.append(os.path.join(os.path.dirname(PATH_CURRENT), 'src'))
import libbioimage.libbioimage as bim


def do_resize(i):
    x = np.empty((4,600,800), dtype=np.float32)
    y = bim.resample(x, (10,20))
    del y

class TestLibBioImageThreading():

    @classmethod
    def setup_class(cls):
        # cls.filename_out = os.path.join(PATH_TESTS, 'out.tif')
        # cls.bim_cache = bim.Cache(size=1)
        pass

    @classmethod
    def teardown_class(cls):
        # if cls.bim_cache is not None:
        #     cls.bim_cache.free()
        pass

    #-----------------------------------------------------------------
    # uint
    #-----------------------------------------------------------------

    def test_image_resize_in_threads (self):
        num_jobs = 4
        num_tests = 100
        outputs = [None] * num_tests
        succeeded = 0
        failed = 0
        with ThreadPoolExecutor(max_workers=num_jobs) as executor:
            running = {executor.submit(do_resize, i): i for i in range(num_tests)}
            for future in futures.as_completed(running):
                i = running[future]
                try:
                    outputs[i] = future.result()
                    succeeded += 1
                except BaseException:
                    failed += 1

        assert (succeeded == num_tests)
        assert (failed == 0)
