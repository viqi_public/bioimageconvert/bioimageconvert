import os
import urllib.request
import posixpath

URL_IMAGE_STORE = os.getenv('BIM_TEST_IMAGESTORE_DOWNLOAD_URL', 'https://s3-us-west-2.amazonaws.com/viqi-test-images/')

def ensure_local_file(filename):
    url = posixpath.join(URL_IMAGE_STORE, urllib.parse.quote_plus(os.path.basename(filename)))
    if not os.path.exists(filename):
        urllib.request.urlretrieve(url, filename)
        print('Downloaded {:}'.format(filename))
    if not os.path.exists(filename):
        return None
    return filename
