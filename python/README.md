# Python bindings for libbioimage

## Debug using Visual Studio 2022

Start by setting "Switch as Startup project" to libimgcnv. This will start and activate debug for the dynamic library. Ensure you are in the "Debug" build mode.

Next, set debug preferences in Properties of libimgcnv project.

1. Set "Command" to the interpreter in your virtual environment: "MY_ENV_PATH\Scripts\python.exe"
1. Set "Command Arguments" to the debug script of interest, you could start by using either debug.py inside imgcnv "python" directory or even "libbioimage.py" itself in the package directory, ex:  "PATH_TO_IMGCNV\python\debug.py"
1. Set "Working Directory" to "PATH_TO_IMGCNV\bin\libimgcnv_x64_Debug\" where debug DLL will be built into.

Now you can set break points and simply debug inside Visual Studio.


## Build documentation

sphinx-build -b html sphinx docs