#!/usr/bin/env python

"""
| Copyright (C) 2021-2024, ViQi Inc
| Authors: Dima Fedorov <dima@viqi.org>, Mike Quinn <mquinn@viqi.org>

Python interface for LibBioImage dynamic library

Usage
------

.. code-block:: python

    import numpy as np
    import libbioimage.libbioimage as bim

    # print libbioimage version
    print(bim.version)

    # print supported formats
    print(bim.formats)

    # resize existing matrix
    x = np.random.randint(low=0, high=65535, size=(3,512,512), dtype=np.uint16)
    x = bim.resample(x, (256,256))

    # load image metadata and tiles using handle caching
    bim_cache = bim.Cache(size=1)

    # load metadata dict
    m = bim.meta(filename, bim_cache)

    # read some tiles
    img1 = bim.read(filename, '-tile 512,1,2,3 -resize 256,256,BC -depth 8,d,u -remap 2', bim_cache)
    img2 = bim.read(filename, '-tile 512,1,3,3 -resize 256,256,BC -depth 8,d,u -remap 2', bim_cache)


Notes
------

Current implementation is thread-safe by protecting all dynlib calls within a lock

Use xarray to report images with metadata

API
----
"""

__author__    = "Dmitry Fedorov"
__version__   = "1.7.0"
__copyright__ = "ViQi Inc"
__project__   = "libbioimage"

import os
import sys
import ctypes
import ctypes.util
import numpy as np
#import time
from lxml import etree
from typing import Union, List, Any # Tuple, Optional, Callable

BIM_MIN_REQUIRED_VERSION = [3, 10, 0]

# available dimensions in the memory location order
dimensions_names = ['x','y','c','z','t','serie','fov','rotation','scene','illumination','phase','view','item','spectrum','measure']

import logging
log = logging.getLogger(__name__ if __name__ != "__main__" else __file__.replace('.py', ''))

#__all__ = ['version', 'formats' ]

libbim_filename = 'libimgcnv.so'
if os.name == 'nt':
    libbim_filename = None
    if os.path.isfile('libimgcnv.dll'):
        # dll is found in the current path
        libbim_filename = os.path.abspath('libimgcnv.dll')
    if libbim_filename is None:
        libbim_filename = ctypes.util.find_library('imgcnv')
    if libbim_filename is None:
        libbim_filename = ctypes.util.find_library('libimgcnv')
    if libbim_filename is None:
        libbim_filename = ctypes.util.find_library('libimgcnv.dll')
    if libbim_filename is None:
        cwd = os.getcwd()
        try:
            os.chdir(os.path.dirname(__file__))
            libbim_filename = ctypes.util.find_library('libimgcnv.dll')
        finally:
            os.chdir(cwd)
else:
    libbim_filename = ctypes.util.find_library('imgcnv')
    if libbim_filename is None and os.path.isfile('/usr/local/lib/libimgcnv.so'):
        libbim_filename = '/usr/local/lib/libimgcnv.so'
    if libbim_filename is None:
        libbim_filename = 'libimgcnv.so'

log.debug('Loading library from "%s"', libbim_filename)
libbim = ctypes.cdll.LoadLibrary(libbim_filename)

import threading
_bim_lock = threading.Lock()

#---------------------------------------------------------------
# consts
#---------------------------------------------------------------

formats = {}
'''
dictionary with supported formats
'''

ver_str = ''
'''
string with libbioimage version
'''

version = []
'''
list of numbers with libbioimage version
'''

#---------------------------------------------------------------
# bim handle cache
#---------------------------------------------------------------

class Cache():
    '''
    Cache of the libbioimage handles for fast repeated access.
    This wraps libbioimage C++ cache initialization and freeing.
    '''

    def __init__(self, size: int = 1):
        '''
        Args:
            size: the number of image handles to keep. Use size == 0 to disable handle caching.
        '''
        self.bim_cache = ctypes.pointer(ctypes.c_void_p(0))
        self.init(size=size)

    def __del__(self):
        self.free()

    def init(self, size: int = 1):
        '''
        Args:
            size: the number of image handles to keep. Use size == 0 to disable handle caching.
        '''
        self.free()
        if size > 0:
            with _bim_lock:
                libbim.imgcnv_cache_init(self.bim_cache, size)

    def free (self):
        '''Free all cached handles'''
        if self.bim_cache.contents != ctypes.c_void_p(0):
            with _bim_lock:
                libbim.imgcnv_cache_finish(self.bim_cache)
            self.bim_cache.contents = ctypes.c_void_p(0)

    def get_handle (self):
        '''returns cache handle'''
        return self.bim_cache.contents

    def clear_buffers (self):
        '''clear content of temporary buffers used to transfer data in and out of bim'''
        if self.bim_cache.contents != ctypes.c_void_p(0):
            with _bim_lock:
                libbim.imgcnv_cache_clear_buffers(self.bim_cache)

#---------------------------------------------------------------
# misc
#---------------------------------------------------------------

def _safetypeparse(v: Any) -> Any:
    try:
        v = int(v)
    except ValueError:
        try:
            v = float(v)
        except ValueError:
            pass
    except TypeError: #in case of Nonetype
        pass
    return v

def _tounicode(s: Union[str, bytes, bytearray], encoding='utf-8') -> str:
    if isinstance(s, str) is True:
        return s
    try:
        return s.decode(encoding)
    except (UnicodeEncodeError, UnicodeDecodeError):
        try:
            return s.decode('latin1')
        except (UnicodeDecodeError, UnicodeEncodeError):
            return s.decode('ascii', 'replace')

def _metastr2dict(out: Union[str, bytes, bytearray]) -> dict:
    rd = {}
    for line in _tounicode(out).splitlines():
        if not line:
            continue
        try:
            tag, val = [ l.lstrip() for l in line.split(':', 1) ]
        except ValueError:
            continue
        tag = tag.replace('%3A', ':')
        val = val.replace('\n', '').replace('%3E', '>').replace('%3C', '<').replace('%3A', ':').replace('%22', '"').replace('%0A', '\n')
        if val != '':
            rd[tag] = _safetypeparse(val)
    return rd

def _dict2metastr(meta: dict) -> str:
    out = []
    for k,v in meta.items():
        k = str(k).replace(':', '%3A')
        v = str(v).replace(':', '%3A').replace('<', '%3C').replace('"', '%22').replace('\n', '%0A')
        out.append('{:}:{:}'.format(k, v))
    return '\n'.join(out)

#---------------------------------------------------------------
# NDArray as image misc classes
#---------------------------------------------------------------

def _get_bim_datatype(x: np.ndarray) -> int:
    '''
    const unsigned int DATA_TYPE_UNSIGNED = 1;
    const unsigned int DATA_TYPE_SIGNED = 2;
    const unsigned int DATA_TYPE_FLOAT = 3;
    '''
    if np.issubdtype(x.dtype, np.floating):
        return 3
    elif np.issubdtype(x.dtype, np.integer) and np.iinfo(x.dtype).min < 0:
        return 2
    return 1

def _get_numpy_datatype(bim_datatype: int, bytes_per_sample: int) -> np.dtype:
    '''
    const unsigned int DATA_TYPE_UNSIGNED = 1;
    const unsigned int DATA_TYPE_SIGNED = 2;
    const unsigned int DATA_TYPE_FLOAT = 3;
    '''
    if bim_datatype == 1 and bytes_per_sample == 1:
        return np.uint8
    elif bim_datatype == 1 and bytes_per_sample == 2:
        return np.uint16
    elif bim_datatype == 1 and bytes_per_sample == 4:
        return np.uint32
    elif bim_datatype == 1 and bytes_per_sample == 8:
        return np.uint64
    elif bim_datatype == 2 and bytes_per_sample == 1:
        return np.int8
    elif bim_datatype == 2 and bytes_per_sample == 2:
        return np.int16
    elif bim_datatype == 2 and bytes_per_sample == 4:
        return np.int32
    elif bim_datatype == 2 and bytes_per_sample == 8:
        return np.int64
    elif bim_datatype == 3 and bytes_per_sample == 4:
        return np.float32
    elif bim_datatype == 3 and bytes_per_sample == 8:
        return np.float64

    return np.uint8

def _get_image_num_channels(x: np.ndarray) -> int:
    '''
    returns the number of channels in this image
    '''
    if x.ndim > 2:
        return x.shape[-3]
    return 1

def _get_image_size(x: np.ndarray) -> np.ndarray:
    '''
    returns image size as [W,H]
    '''
    sz = np.array(x.shape[-2:], np.uint64)
    return np.ascontiguousarray(np.flip(sz)) #.copy(order='C')

def _ensure_bim_array(x: np.ndarray, name: str):
    # ensure c contiguous
    if x.data.c_contiguous is not True:
        raise ValueError('%s array must be C contiguous'%name)

    pxsz = x.dtype.itemsize

    # ensure strides are acceptable by libbioimage
    if x.strides[-1] != pxsz:
        raise ValueError('%s array fastest dimension must advance by 1'%name)

    # ensure strides are acceptable by libbioimage
    if x.ndim > 1 and x.strides[-2] != pxsz * x.shape[-1]:
        raise ValueError('%s array fastest dimension must advance by 1'%name)


#---------------------------------------------------------------
# libimgcnv calls
#---------------------------------------------------------------

if os.name == 'nt':
    def _call_imgcnvlib(command, bim_cache=None):
        bim_cache = Cache(size=0) if bim_cache is None else bim_cache

        arr = (ctypes.c_wchar_p * len(command))()
        arr[:] = [_tounicode(i) for i in command]
        res = ctypes.pointer(ctypes.c_char_p())

        try:
            with _bim_lock:
                r = libbim.imgcnv(len(command), arr, res, bim_cache.get_handle())
        except Exception:
            log.exception('Exception calling libbioimage')
            return 100, None

        out = res.contents.value
        with _bim_lock:
            _ = libbim.imgcnv_clear(res)
        return r, out
else:
    def _call_imgcnvlib(command, bim_cache=None):
        bim_cache = Cache(size=0) if bim_cache is None else bim_cache
        command = [c.encode('utf-8') for c in command]

        arr = (ctypes.c_char_p * len(command))()
        arr[:] = command
        res = ctypes.pointer(ctypes.c_char_p())

        try:
            with _bim_lock:
                r = libbim.imgcnv(len(command), arr, res, bim_cache.get_handle())
        except Exception:
            log.exception('Exception calling libbioimage')
            return 100, None

        out = res.contents.value
        with _bim_lock:
            _ = libbim.imgcnv_clear(res)
        return r, out


'''
API
----
'''

#---------------------------------------------------------------
# version
#---------------------------------------------------------------

def _version():
    command = ['imgcnv', '-v']
    r, out = _call_imgcnvlib(command)
    if r>0 or out is None:
        return ''
    return out.decode('utf-8').replace('\n', '')

def _ensure_version_supported(current, required):
    for c,r in zip(current, required):
        if c < r:
            raise ImportError('Existing libbioimage version [%s] is older than required [%s]'%(str(current), str(required)))

# access once and store statically
ver_str = _version()
version = [int(s) for s in ver_str.split('.')]
log.debug('Loaded libbioimage version %s', version)

# test minimum required version is available
_ensure_version_supported(version, BIM_MIN_REQUIRED_VERSION)

#---------------------------------------------------------------
# supported formats
#---------------------------------------------------------------

def _formats():
    command = ['imgcnv', '-fmtxml']
    r, out = _call_imgcnvlib(command)
    if r>0 or out is None:
        return {}

    formats = etree.fromstring( '<formats>%s</formats>'%out )
    codecs = formats.xpath('//codec')
    fmts = {}
    for c in codecs:
        try:
            name = c.get('name')
            support = [t.get('value', '') for t in c.xpath('tag[@name="support"]')]
        except IndexError:
            continue
        fmts[name.lower()] = ','.join(support)
    return fmts

# access once and store statically
formats = _formats()



#---------------------------------------------------------------
# metadata
#---------------------------------------------------------------

def meta(filename: str, bim_cache: Cache = None) -> dict:
    '''
    returns the metadata dictionary for a given file

    Args:
        filename: path to the image file
        bim_cache: optional bim.Cache instance

    Returns:
        metadata in libbioimage format
    '''
    command = ['imgcnv', '-meta', '-i', filename, '-verbose', '0']
    r, out = _call_imgcnvlib(command, bim_cache)
    if r>0 or out is None:
        return {}

    return _metastr2dict(out.decode('utf-8', errors='ignore'))

#---------------------------------------------------------------
# read / write
#---------------------------------------------------------------

def read(filename: str, pipeline: str = None, bim_cache: Cache = None) -> np.ndarray:
    '''
    read the 2D image and process through pipeline if requested,
    if the page or N-D dimensions are not specified in the pipeline it will read the first one

    Args:
        filename: path to the image file
        pipeline: command line like string: '-resize 640,480,BC -depth 8,d,u -remap 2'
        bim_cache: bim.Cache instance

    Returns:
        image ndarray in formats [YX], [CYX]

    Notes:
        to get N-D images one needs to use read_nd function that produces xarrays is formats [ZCYX], [TCYX], [TZCYX], [...TZCYX]

    Examples:

    .. code-block:: python

        bim_cache = bim.Cache(size=1)
        t1 = bim.read(filename, '-tile 512,1,2,3 -resize 256,256,BC -depth 8,d,u -remap 2', bim_cache)
        t2 = bim.read(filename, '-tile 512,1,3,3 -resize 256,256,BC -depth 8,d,u -remap 2', bim_cache)

        r1 = bim.read(filename, '-tile-roi 10,10,100,100,0.5', bim_cache)
        r1 = bim.read(filename, '-tile-roi 1000,1000,1100,1100,1.0', bim_cache)
    '''

    bim_cache = Cache(size=1) if bim_cache is None else bim_cache

    x_sz = np.zeros((2), np.uint64)
    bim_data_type = ctypes.c_uint()
    num_samples = ctypes.c_uint()
    bytes_per_pixel = ctypes.c_uint()

    with _bim_lock:
        libbim.bim_load(
            ctypes.c_char_p(filename.encode('utf-8')),
            ctypes.c_char_p(pipeline.encode('utf-8') if pipeline is not None else 0),
            bim_cache.get_handle(),

            ctypes.byref(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.byref(bytes_per_pixel),
            ctypes.byref(bim_data_type),
        )

    dtype = _get_numpy_datatype(bim_data_type.value, bytes_per_pixel.value)
    out = np.empty((num_samples.value, x_sz[1], x_sz[0]), dtype)

    with _bim_lock:
        libbim.bim_read(
            bim_cache.get_handle(),

            num_samples,
            ctypes.c_void_p(x_sz.ctypes.data), #x_sz, x.ctypes.shape_as(ctypes.c_uint64),

            bytes_per_pixel,
            bim_data_type,
            ctypes.c_void_p(out.ctypes.data)
        )

    bim_cache.clear_buffers()
    return out

def read_region(
        filename: str,
        level: int = None, scale: float = None,
        roi: tuple[int] = None, bbox: tuple[int] = None,
        pipeline: str = None,
        bim_cache: Cache = None) -> np.ndarray:
    '''
    read the 2D image region and process through pipeline if requested,
    if the page or nd dimensions are not specified in the pipeline it will read the first one

    Args:
        filename: path to the image file
        level: requested power-of-two pyramidal level, something like: [0, 1, 2, ...], only provide either scale or level
        scale: requested scale for the image pyramid, something like: [1.0, 0.5, 0.25, ...], only provide either scale or level
        roi: tuple containing [i,j,i2,j2] positions at requested resolution, in image coordinates [y,x,y2,x2], only provide either roi or bbox
        bbox: tuple containing (i,j,Ni,Nj) positions at requested resolution, in image coordinates (y,x,height,width), only provide either roi or bbox
        pipeline: command line like string: '-resize 640,480,BC -depth 8,d,u -remap 2'
        bim_cache: bim.Cache instance

    Returns:
        image ndarray in formats [YX], [CYX]

    Examples:

    .. code-block:: python

        bim_cache = bim.Cache(size=1)
        r1 = bim.read_region(filename, scale=0.5, bbox=(10,10,90,90), bim_cache)
        r2 = bim.read_region(filename, scale=1.0, bbox=(1000,1000,512,512), bim_cache)
    '''

    if level is None and scale is None:
        raise ValueError('Either level or scale must be provided')
    if level is not None and scale is not None:
        raise ValueError('Only one of either level or scale must be provided')
    if roi is None and bbox is None:
        raise ValueError('Either roi or bbox must be provided')
    if roi is not None and bbox is not None:
        raise ValueError('Only one of either roi or bbox must be provided')

    if roi is not None:
        x = roi[1]
        y = roi[0]
        x2 = roi[3]
        y2 = roi[2]
    elif bbox is not None:
        # Convert to ROI
        x = bbox[1]
        y = bbox[0]
        x2 = x + bbox[3] - 1
        y2 = y + bbox[2] - 1

    if level is not None:
        r = level
    elif scale is not None:
        r = scale

    crd = '%s,%s,%s,%s,%s'%(x,y,x2,y2,r)
    pipeline = pipeline or ''
    pipeline = '%s %s %s'%('-tile-roi', crd, pipeline)
    return read(filename, pipeline, bim_cache)

def read_tile(
        filename: str,
        level: int = None,
        tile_id: tuple[int] = None,
        tile_size: int = None,
        pipeline: str = None,
        bim_cache: Cache = None) -> np.ndarray:
    '''
    read the 2D image tile and process through pipeline if requested,
    if the page or nd dimensions are not specified in the pipeline it will read the first one

    Args:
        filename: path to the image file
        level: requested power-of-two pyramidal level, pyramidal levels like [0, 1, 2, ...] correspond to scales: [1.0, 0.5, 0.25, ...]
        tile_id: tuple containing (id_i, id_j) grid positions at requested resolution, in image coordinates (id_y, id_x)
        tile_size: tile size in pixels
        pipeline: command line like string: '-resize 640,480,BC -depth 8,d,u -remap 2'
        bim_cache: bim.Cache instance

    Returns:
        image ndarray in formats [YX], [CYX]

    Examples:

    .. code-block:: python

        bim_cache = bim.Cache(size=1)
        t1 = bim.read_tile(filename, level=2, tile_size=512, tile_id=(1,2), pipeline='-depth 8,d,u -remap 2', bim_cache)
        t2 = bim.read_tile(filename, level=2, tile_size=512, tile_id=(1,3), pipeline='-depth 8,d,u -remap 2', bim_cache)
    '''

    if level is None:
        raise ValueError('scale must be provided')
    if tile_id is None:
        raise ValueError('tile_id must be provided')
    if tile_size is None:
        raise ValueError('tile_size must be provided')

    x = tile_id[1]
    y = tile_id[0]
    crd = '%s,%s,%s,%s'%(tile_size,x,y,level)
    pipeline = pipeline or ''
    pipeline = '%s %s %s'%('-tile', crd, pipeline)
    return read(filename, pipeline, bim_cache)


try:
    import xarray as xr

    def read_nd(filename: str, ranges: dict = None, pipeline: str = None, bim_cache=None) -> xr.DataArray:
        '''
        read N-D image (or region within) and process through pipeline if requested

        Args:
            filename: path to the image file
            ranges: a dictionary of dimensions_names with ranges of planes as None (meaning all), int, list() or range()
                    accepted dimensions are: 'z','t','serie','fov','rotation','scene','illumination','phase','view','item','spectrum','measure'
                    to be supported are: 'x','y','c', for now, -tile-roi and -remap pipeline arguments
            pipeline: command line like string: '-resize 640,480,BC -depth 8,d,u -remap 2'
            bim_cache: bim.Cache instance

        Returns:
            image xarray in formats [ZCYX], [TCYX], [TZCYX], [...TZCYX]

        Examples:

        .. code-block:: python

            bim_cache = bim.Cache(size=1)
            zstack = bim.read_nd(filename, ranges={'z': None}, bim_cache)

            zroi = bim.read_nd(filename, ranges={'z': range(5,10)}, bim_cache)

            zroi2 = bim.read_nd(filename, ranges={'z': range(5,10), 't': 13}, bim_cache)
        '''
        bim_cache = Cache(size=1) if bim_cache is None else bim_cache
        pipeline = pipeline or ''

        m = meta(filename, bim_cache)

        # normalize ranges
        ranges = ranges or {}

        # dima: need additional update
        # use -tile-roi and -remap if 'x','y','c' are set

        ranges.setdefault('x', None) # None defines whole range
        ranges.setdefault('y', None) # None defines whole range
        ranges.setdefault('c', None) # None defines whole range

        # dimensions_names = ['x','y','c','z','t','serie','fov','rotation','scene','illumination','phase','view','item','spectrum','measure']

        # define image dimensions
        dims = []
        sizes = []

        # go through all the requested and available dimensions to build the output image N-D size, dims and slices
        for dim in reversed(dimensions_names):
            if dim not in ranges:
                continue
            sz = m.get('image_num_%s'%dim, 1)
            if sz <= 1 and dim not in ['x','y','c']:
                continue
            dims.append(dim)

            if ranges[dim] is None:
                ranges[dim] = range(sz)
            elif type(ranges[dim]) is int:
                sz = 1
                ranges[dim] = [ranges[dim]]
            elif type(ranges[dim]) in [list, range]:
                sz = len(ranges[dim])
            sizes.append(sz)

        pos = dict([(d, i) for i,d in enumerate(dims)])

        # print(ranges)
        # print(dims)
        # print(sizes)
        # print(pos)

        a = None
        sla = [slice(0,i) for i,d in zip(sizes, dims)]
        sls = [None for d in dims]
        def set_slices(dim_name, i):
            if dim_name in pos:
                sla[pos[dim_name]] = i
                sls[pos[dim_name]] = '%s:%s'%(dim_name,i)

        # -slice - N-D slices to extract, should be followed by dimension positions separated by comma, ex: -slice z:2,fov:42,spectrum:200;220;333
        #   The available dimensions will depend on the image, the list includes: z,t,serie,fov,rotation,scene,illumination,phase,view,item,spectrum,measure
        # ['measure', 'spectrum', 'item', 'view', 'phase', 'illumination', 'scene', 'rotation', 'fov', 'serie', 't', 'z']
        for i0 in ranges.get('measure', [None]):
            set_slices('measure', i0)
            for i1 in ranges.get('spectrum', [None]):
                set_slices('spectrum', i1)
                for i2 in ranges.get('item', [None]):
                    set_slices('item', i2)
                    for i3 in ranges.get('view', [None]):
                        set_slices('view', i3)
                        for i4 in ranges.get('phase', [None]):
                            set_slices('phase', i4)
                            for i5 in ranges.get('illumination', [None]):
                                set_slices('illumination', i5)
                                for i6 in ranges.get('scene', [None]):
                                    set_slices('scene', i6)
                                    for i7 in ranges.get('rotation', [None]):
                                        set_slices('rotation', i7)
                                        for i8 in ranges.get('fov', [None]):
                                            set_slices('fov', i8)
                                            for i9 in ranges.get('serie', [None]):
                                                set_slices('serie', i9)
                                                for i10 in ranges.get('t', [None]):
                                                    set_slices('t', i10)
                                                    for i11 in ranges.get('z', [None]):
                                                        set_slices('z', i11)
                                                        sl_nda = tuple(sla)
                                                        sl_str = ','.join([i for i in sls if i is not None])
                                                        pipeline2 = '-slice %s %s'%(sl_str, pipeline)

                                                        # TODO: support -slice option in read

                                                        # print(sl_nda)
                                                        # print(sl_str)
                                                        # print(pipeline2)

                                                        plane = read(filename, pipeline2, bim_cache)
                                                        if a is None:
                                                            a = np.empty(tuple(sizes), plane.dtype)
                                                        a[sl_nda] = plane

        xa = xr.DataArray(a, dims = dims)
        xa.attrs.update(m)
        return xa

except ImportError:
    log.warning('xarray could not be imported, disabling read_nd functionality')

def write(x: np.ndarray | xr.DataArray, filename: str, fmt: str = 'tiff', meta: dict = None):
    '''
    write will store and N-D image into desired file format

    Args:
        x: input image in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        filename: filename to write to
        fmt: output format as defined in bim.formats
        meta: dict with any metadata to write in bim v3 schema

    Examples:

    .. code-block:: python

        x = np.empty((4,200,400), dtype=np.float32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        bim.write(x, filename_out, 'tiff')
    '''

    if type(x) is xr.DataArray:
        # meta = x.attr
        x = x.data

    # temporary conventionality restrictions
    if len(x.shape) > 3:
        raise ValueError('Currently only supports 2D and 2D+channels arrays')

    if filename is None or len(filename)==0:
        raise ValueError('Valid filename is needed')
    if fmt is None or len(fmt)==0:
        raise ValueError('Valid format is needed')

    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)

    if meta is not None:
        meta = _dict2metastr(meta).encode('utf-8')

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places
    _ensure_bim_array(x, 'input')
    _ensure_bim_array(x_sz, 'input size')

    with _bim_lock:
        libbim.bim_write(
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            ctypes.c_char_p(filename.encode('utf-8')),
            ctypes.c_char_p(fmt.encode('utf-8')),
            ctypes.c_char_p(meta if meta is not None else 0),
        )

# def write_plane(x, filename, fmt='tiff', meta=None, bim_cache=None):
#     '''
#     write N-D image per plane, each plane must have all required channels
#     the first write must have proper meta dict consecutive writes don't need it

#     bim_cache is required for this to work, close the cache or open another file afterwards

#     x: in supported matrix format: [Y X], [Y X C]
#     filename: filename to write to
#     fmt: output format as defined in bim.formats
#     meta: dict with any metadata to write in bim v3 schema
#     '''
#     raise ValueError('NOT YET IMPLEMENTED !!!!!')
#     if bim_cache is None:
#         raise ValueError('bim_cache is required for plane by plane writing')

#     # # const unsigned int DATA_TYPE_UNSIGNED = 1;
#     # # const unsigned int DATA_TYPE_SIGNED = 2;
#     # # const unsigned int DATA_TYPE_FLOAT = 3;

#     # c_data_type = 1
#     # if np.issubdtype(x.dtype, np.floating):
#     #     c_data_type = 3
#     # elif np.issubdtype(x.dtype, np.integer) and np.iinfo(x.dtype).min < 0:
#     #     c_data_type = 2

#     # filename_cst = (ctypes.c_char_p * len(filename))()
#     # filename_cst[:] = filename

#     # fmt_cst = (ctypes.c_char_p * len(fmt))()
#     # fmt_cst[:] = fmt

#     # meta_str = _dict2metastr(meta)
#     # meta_cst = (ctypes.c_char_p * len(meta_str))()
#     # meta_cst[:] = meta_str

#     # libbim.bim_write_plane(
#     #     ctypes.c_uint(x.ndim),
#     #     x.ctypes.shape,
#     #     x.ctypes.strides,
#     #     ctypes.c_uint(x.dtype.itemsize),
#     #     ctypes.c_uint(c_data_type),
#     #     ctypes.c_void_p(x.ctypes.data),

#         # ctypes.c_uint(len(filename)),
#         # filename_cst,

#     #     ctypes.c_uint(len(fmt)),
#     #     fmt_cst,

#     #     ctypes.c_uint(len(meta_str)),
#     #     meta_cst,

#     #     bim_cache.get_handle()
#     # )
#     pass


#---------------------------------------------------------------
# interpolations
#---------------------------------------------------------------

ResizeMethods = {
    'nn': 0,
    'bl': 1,
    'bc': 2,
    'lanczos': 3,
    'bessel': 4,
    'point': 5,
    'box': 6,
    'hermite': 7,
    'hanning': 8,
    'hamming': 9,
    'blackman': 10,
    'gaussian': 11,
    'quadratic': 12,
    'catrom': 13,
    'mitchell': 14,
    'sinc': 15,
    'blackmanbessel': 16,
    'blackmansinc': 17,
}

# note: instead of using ctypes matching and conversion tests,
# we try to do this more efficiently ourselves
# CARRAY_P = np.ctypeslib.ndpointer(flags="C")
# CARRAY_P_1D_UINT64 = np.ctypeslib.ndpointer(dtype=np.uint64, ndim=1, flags="C")
# libbim.bim_resample.argtypes = [ctypes.c_uint,
#                                 CARRAY_P_1D_UINT64,
#                                 ctypes.c_uint,
#                                 ctypes.c_uint,
#                                 ctypes.c_void_p,
#                                 ctypes.c_uint,
#                                 CARRAY_P_1D_UINT64,
#                                 ctypes.c_void_p]
# libbim.bim_resample.restype = ctypes.c_int

def _resize_op(x, new_size, flt='bc', out=None, f=None):
    '''
    internal operation supporting both resize and resample ops
    '''

    # temporary conventionality restrictions
    if len(x.shape) > 3:
        raise ValueError('Currently only supports 2D and 2D+channels arrays')

    if len(new_size) < 2:
        raise ValueError('new_size must contain at least 2 dimensions')
    if len(new_size) > 2: # need to update to support 3D
        raise ValueError('new_size can only support 2D interpolation for now')
    if out is not None and out.shape[-2:] != new_size:
        raise ValueError('output array does not match the required size')
    if out is not None and out.ndim > 2 and out.shape[-3] != x.shape[-3]:
        raise ValueError('output array does not match the required size')

    if out is None:
        sz = np.array(x.shape, int)
        new_size = np.array(new_size, int)
        sz[-2:] = new_size[-2:]
        out = np.empty(sz, dtype=x.dtype)

    ResizeMethod = ResizeMethods.get(flt.lower(), 2)
    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)
    o_sz = _get_image_size(out)

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places
    _ensure_bim_array(x, 'input')
    _ensure_bim_array(out, 'output')
    _ensure_bim_array(x_sz, 'input size')
    _ensure_bim_array(o_sz, 'output size')

    with _bim_lock:
        f(
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data), #x_sz, x.ctypes.shape_as(ctypes.c_uint64),

            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            ctypes.c_uint(ResizeMethod),

            ctypes.c_void_p(o_sz.ctypes.data), #o_sz, #out.ctypes.shape_as(ctypes.c_uint64),
            ctypes.c_void_p(out.ctypes.data)
        )

    return out


def resample(x: np.ndarray, new_size: tuple, flt: str = 'bc', out: np.ndarray = None) -> np.ndarray:
    '''
    resample will resample 2D planes for each dimension, this means it will operate on an ND data but
    only resample each 2D plane individually, we will add 3D support by accepting new_size with len==3

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        new_size: (Y X) is the new size in pixels
        flt: one of supported kernels
            'NN', 'BL', 'BC', 'lanczos', 'bessel', 'point', 'box', 'hermite',
            'hanning', 'hamming', 'blackman', 'gaussian', 'quadratic', 'catrom',
            'mitchell', 'sinc', 'blackmanbessel', 'blackmansinc'
        out: optional output image, must be of proper size as defined in new_size plus any other dimensions from input

    Returns:
        image ndarray in formats [YX], [CYX]

    Notes:
        this function does not support interleaved channels and expects data to be in
        separate channel memory layout

    Examples:

    .. code-block:: python

        x = np.empty((4,200,400), dtype=np.float32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        y = bim.resample(x, (100,200))
    '''

    return _resize_op(x, new_size, flt=flt, out=out, f=libbim.bim_resample)

def resize(x: np.ndarray, new_size: tuple, flt: str = 'bc', out: np.ndarray = None) -> np.ndarray:
    '''
    resize is similar to resample with the difference of using an image pyramid for very large
    images and difference between input and output sizes, it will use the closest pyramid level and
    then use resampling from there

    resize will resize 2D planes for each dimension, this means it will operate on an ND data but
    only resize each 2D plane individually, we will add 3D support by accepting new_size with len==3

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        new_size: (Y X) is the new size in pixels
        flt: one of supported kernels
            'NN', 'BL', 'BC', 'lanczos', 'bessel', 'point', 'box', 'hermite',
            'hanning', 'hamming', 'blackman', 'gaussian', 'quadratic', 'catrom',
            'mitchell', 'sinc', 'blackmanbessel', 'blackmansinc'
        out: optional output image, must be of proper size as defined in new_size plus any other dimensions from input

    Returns:
        image ndarray in formats [YX], [CYX]

    Notes:
        this function does not support interleaved channels and expects data to be in
        separate channel memory layout

    Examples:

    .. code-block:: python

        x = np.empty((4,200,400), dtype=np.float32)
        x[0].fill(1)
        x[1].fill(2)
        x[2].fill(3)
        x[3].fill(4)
        y = bim.resize(x, (100,200))
    '''

    return _resize_op(x, new_size, flt=flt, out=out, f=libbim.bim_resize)

#---------------------------------------------------------------
# filtering
#---------------------------------------------------------------

def filter_gaussian(x: np.ndarray, kernel_size: list = [3,3], sigma: float = 1.0, out: np.ndarray = None) -> np.ndarray:
    '''
    filter_gaussian will apply gaussian filter to all 2D planes for each dimension,
    this means it will operate on an ND data but only filter each 2D plane individually,
    we will add 3D support by accepting kernel_size with len==3

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        kernel_size: [Y X] is the kernel_size in pixels
        sigma: sigma for gaussian kernel definition
        out: optional output image, must be same size as input

    Returns:
        image ndarray in formats [YX], [CYX]
    '''

    if len(kernel_size) < 2:
        raise ValueError('kernel_size must contain at least 2 dimensions')
    if len(kernel_size) > 2: # need to update to support 3D
        raise ValueError('kernel_size can only support 2D convolutions for now')

    # temporary conventionality restrictions
    if len(x.shape) > 3:
        raise ValueError('Currently only supports 2D and 2D+channels arrays')

    if out is None:
        out = np.empty(x.shape, dtype=x.dtype)

    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)

    ks = np.array(list(reversed(kernel_size)), np.uint) # convert into XY order from YX

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places
    _ensure_bim_array(x, 'input')
    _ensure_bim_array(out, 'output')
    _ensure_bim_array(x_sz, 'input size')
    _ensure_bim_array(ks, 'kernel size')

    with _bim_lock:
        libbim.bim_filter_gaussian(
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            ctypes.c_uint(len(kernel_size)),
            ctypes.c_void_p(ks.ctypes.data),
            ctypes.c_double(sigma),

            ctypes.c_void_p(out.ctypes.data)
        )

    return out

def filter_median(x: np.ndarray, kernel_size: list = [3,3], out: np.ndarray = None) -> np.ndarray:
    '''
    filter_median will apply median filter to all 2D planes for each dimension,
    this means it will operate on an ND data but only filter each 2D plane individually,
    we will add 3D support by accepting kernel_size with len==3

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        kernel_size: [Y X] is the kernel_size in pixels
        out: optional output image, must be same size as input

    Returns:
        image ndarray in formats [YX], [CYX]
    '''
    if len(kernel_size) < 2:
        raise ValueError('kernel_size must contain at least 2 dimensions')
    if len(kernel_size) > 2: # need to update to support 3D
        raise ValueError('kernel_size can only support 2D convolutions for now')

    if out is None:
        out = np.empty(x.shape, dtype=x.dtype)

    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)

    ks = np.array(list(reversed(kernel_size)), np.uint) # convert into XY order from YX

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places
    _ensure_bim_array(x, 'input')
    _ensure_bim_array(out, 'output')
    _ensure_bim_array(x_sz, 'input size')
    _ensure_bim_array(ks, 'kernel size')

    with _bim_lock:
        libbim.bim_filter_median(
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            ctypes.c_uint(len(kernel_size)),
            ctypes.c_void_p(ks.ctypes.data),

            ctypes.c_void_p(out.ctypes.data)
        )

    return out

def filter_mrs(x: np.ndarray,
               levels: int = 0,
               base: float = None, noise: float = 1.0, background: float = 1.0,
               midrange: List[int] = None,
               normalize_in: bool = True, normalize_out: bool = True, trim: bool = True,
               out: np.ndarray = None):
    '''
    Multi-Resolution filter

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        levels: number of resolution levels to create, 0 = automatic, negative values
                reduce levels form automatic setting, positives define exact numbers
        base: optional value for the base level
        noise: amount of noise filtering, where 0 is to remove everything and 1.0 to
               disable de-noising
        background: amount of background flattening, where 0 is perfectly flat and 1.0 to
               disable flattening
        midrange: optional list of levels to remove mid-range noise
        normalize_in: normalize input to floating point [0, 1] range
        normalize_out: normalize output to original range
        trim: trim output signal ot [0, 1] range
        out: optional output image, must be same size as input

    Returns:
        image ndarray in formats [YX], [CYX]
    '''

    # temporary conventionality restrictions
    if len(x.shape) > 3:
        raise ValueError('Currently only supports 2D and 2D+channels arrays')

    if normalize_in is True:
        dt = x.dtype
        minv = x.min()
        rng = x.max()-minv
        x = (x.astype(np.float32) - minv) / rng

    if out is None:
        out = np.empty(x.shape, dtype=x.dtype)

    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places
    _ensure_bim_array(x, 'input')
    _ensure_bim_array(out, 'output')
    _ensure_bim_array(x_sz, 'input size')

    with _bim_lock:
        libbim.bim_filter_mrs(
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            ctypes.c_void_p(out.ctypes.data),

            ctypes.c_uint(levels),
            ctypes.c_float(noise),
            ctypes.c_float(background),
            ctypes.c_float(base if base is not None else 0),
            ctypes.c_char_p(midrange),
            ctypes.c_bool(trim),
        )

    if normalize_in is True and normalize_out is True:
        out = ((out*rng)+minv).astype(dt)

    return out

def filter_regiongrow(x: np.ndarray, 
                      region_size: int = 100, 
                      minimum_convexity: float = 1.0, 
                      boundary_probability: np.ndarray = None, 
                      out: np.ndarray = None) -> np.ndarray:
    '''
    Region growing for label images of all 2D planes for each dimension,
    this means it will operate on an ND data but only filter each 2D plane individually,
    we will add 3D support later

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        region_size: is the expected region_size in pixels
        minimum_convexity: [0,1] minimum convexity of regions
        boundary_probability: uint8 array of cell boundary probability at each pixel
        out: optional output image, must be same size as input

    Returns:
        image ndarray in formats [YX], [CYX]

    Examples:

    .. code-block:: python

        x = np.empty((1,550,550), dtype=np.int32)
        y100 = bim.filter_regiongrow(x, region_size = 100, minimum_convexity = 0.95, boundary_probability=None)
    '''
    if region_size < 1:
        raise ValueError('region_size is too small')

    if minimum_convexity < 0.0 or minimum_convexity > 1.0:
        raise ValueError('minimum_convexity must be in [0,1]')

    if ((boundary_probability is not None) and (boundary_probability.dtype != np.uint8)):
        raise TypeError('boundary probability must be of type np.uint8')


    if out is None:
        out = np.empty(x.shape, dtype=x.dtype)

    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places

    _ensure_bim_array(x, 'input')
    _ensure_bim_array(out, 'output')
    _ensure_bim_array(x_sz, 'input size')

    boundary_argument = None
    if boundary_probability is not None:
        _ensure_bim_array(boundary_probability, 'boundary probability')
        boundary_argument = ctypes.c_void_p(boundary_probability.ctypes.data)

    with _bim_lock:
        libbim.bim_filter_regiongrow(
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            ctypes.c_uint(region_size),
            ctypes.c_double(minimum_convexity),
            boundary_argument,
            ctypes.c_void_p(out.ctypes.data)
        )

    return out

#---------------------------------------------------------------
# pipeline processing
#---------------------------------------------------------------

def process(x: np.ndarray, pipeline: str = None) -> np.ndarray:
    '''
    process the input matrix through the requested pipeline on 2D planes for each dimension,
    this means it will operate on an ND data but only filter each 2D plane individually,

    Args:
        x: in supported matrix format: [YX], [CYX], [ZCYX], [TCYX], [TZCYX], [...TZCYX]
        pipeline: command line like string: '-resize 640,480,BC -depth 8,d,u -remap 2'

    Returns:
        image ndarray in formats [YX], [CYX]
    '''

    # temporary conventionality restrictions
    if len(x.shape) > 3:
        raise ValueError('Currently only supports 2D and 2D+channels arrays')

    bim_cache = Cache(size=1)

    bim_data_type = _get_bim_datatype(x)
    num_samples = _get_image_num_channels(x)
    x_sz = _get_image_size(x)

    out_sz = np.zeros((2), np.uint64)
    out_data_type = ctypes.c_uint()
    out_samples = ctypes.c_uint()
    out_bytes_per_pixel = ctypes.c_uint()

    # ensure array are acceptable by libbioimage
    # x = np.ascontiguousarray(x) # keep the exception that will help find inefficient places
    _ensure_bim_array(x, 'input')

    with _bim_lock:
        res = libbim.bim_process(
            ctypes.c_char_p(pipeline.encode('utf-8') if pipeline is not None else 0),
            bim_cache.get_handle(),

            # input image
            ctypes.c_uint(num_samples),
            ctypes.c_void_p(x_sz.ctypes.data),
            ctypes.c_uint(x.dtype.itemsize),
            ctypes.c_uint(bim_data_type),
            ctypes.c_void_p(x.ctypes.data),

            # output dims
            ctypes.byref(out_samples),
            ctypes.c_void_p(out_sz.ctypes.data),
            ctypes.byref(out_bytes_per_pixel),
            ctypes.byref(out_data_type),
        )
    if res > 0:
        return

    dtype = _get_numpy_datatype(out_data_type.value, out_bytes_per_pixel.value)
    sz = [] if out_samples.value < 2 else [out_samples.value]
    sz.append(out_sz[1])
    sz.append(out_sz[0])
    out = np.empty(tuple(sz), dtype)

    _ensure_bim_array(out, 'output')
    with _bim_lock:
        libbim.bim_read(
            bim_cache.get_handle(),

            out_samples,
            ctypes.c_void_p(out_sz.ctypes.data),

            out_bytes_per_pixel,
            out_data_type,
            ctypes.c_void_p(out.ctypes.data)
        )

    bim_cache.clear_buffers()
    return out

#---------------------------------------------------------------
# testing
#---------------------------------------------------------------

def run_tests():
    import numpy as np
    from timeit import default_timer as timer
    # import skimage.io

    # print supported formats
    print(version)
    #print(formats)

    # resize existing matrix
    #x = np.random.randint(low=0, high=65535, size=(512,512), dtype=np.uint16)

    # filename = 'A01.jpg'
    # x = skimage.io.imread(filename)
    # x = np.ascontiguousarray(np.moveaxis(x, -1, 0)) # convert interleaved to separate image representation
    # log.debug('x flags: %s', x.flags)

    x = np.empty((4000,6000), dtype=np.uint16)
    x.fill(1234)
    start = timer()
    y = resample(x, (2000,2000))
    end = timer()
    print('libbioimage resample runtime: ', end - start)
    print (y.shape)
    print (y[0,0])

    start = timer()
    y = resize(x, (2000,2000))
    end = timer()
    print('libbioimage resize runtime: ', end - start)

    # fn_out = 'A01_resized.tif'
    # skimage.io.imsave(fn_out, y)

    # # load image metadata and tiles using handle caching
    # bim_cache = Cache(size=1)

    # m = meta(filename, bim_cache)
    # img1 = read(filename, '-tile 512,1,2,3 -resize 256,256,BC -depth 8,d,u -remap 2', bim_cache)
    # img2 = read(filename, '-tile 512,1,3,3 -resize 256,256,BC -depth 8,d,u -remap 2', bim_cache)

if __name__ == "__main__":
    d = 0
    run_tests()
    # d += compare_runtimes(N=4000, dt=np.uint16)
    # d += compare_runtimes(N=4000, dt=np.float32)
    # d += compare_runtimes(N=4000, dt=np.float64)

    del libbim
    sys.exit(d)
