#!/bin/bash -e
set -x
env

distribution=$DISTRIBUTION

# This script is executed within the container as root.  It assumes
# that source code with debian packaging files can be found at
# /source-ro and that resulting packages are written to /output after
# succesful build.  These directories are mounted as docker volumes to
# allow files to be exchanged between the host and the container.


workspace=$PWD
PLUGIN_PACKAGE_DIR=${PLUGIN_PACKAGE_DIR:-$workspace/packages/$distribution}


build_package () {
    dependencies=${PLUGIN_DEPENDENCIES:-$workspace/dependencies}
# Install extra dependencies that were provided for the build (if any)
#   Note: dpkg can fail due to dependencies, ignore errors, and use
#   apt-get to install those afterwards
    [[ -d $dependencies ]] && dpkg -i $dependencies/*.deb || (apt-get update --allow-releaseinfo-change  -qq && apt-get -f install -y --no-install-recommends)

    mkdir -p /build $PLUGIN_PACKAGE_DIR

    # Make read-write copy of source code
    cp -a $workspace /build/source
    #ls -r /build/source/libsrc/libbioimg/..//libCZI/Src/libCZI
    cd /build/source
    if [ ! -z "$distribution" ] ; then
        sed -i -e "1 s/(\(.*\)-\(.*\))/(\1~$distribution-\2)/" debian/changelog
        sed -i -e "1 s/unstable/$distribution/g" debian/changelog
    fi

    # Install build dependencies
    apt-get update -qq
    mk-build-deps -ir -t "apt-get -o Debug::pkgProblemResolver=yes -y --no-install-recommends"

    # Build packages
    debuild --preserve-env -b ${PLUGIN_BUILD_OPTIONS:=-uc -us} --jobs=auto

    # Copy packages to output dir with user's permissions
    #chown -R $USER:$GROUP /build
    # Copy w/o errors for missing files
    cp -a /build/*.{deb,ddeb,changes,buildinfo}  $PLUGIN_PACKAGE_DIR 2>/dev/null || :
}


upload_package() {
    if [ ! -f ~/.ssh/id_rsa ] && [ ! -z "$UPLOAD_SSH_KEY" ] ; then
        mkdir -p ~/.ssh
        echo "$UPLOAD_SSH_KEY" > ~/.ssh/id_rsa
        cat <<EOF > ~/.ssh/config
host *
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
EOF
        chmod -R go-rw ~/.ssh
    fi


    cat <<EOF > /root/.dput.cf
[debian]
fqdn = ${UPLOAD_HOST}
incoming = ${UPLOAD_INCOMING}
login = ${UPLOAD_LOGIN}
allow_unsigned_uploads = 1
method = scp
distributions = ${DISTRIBUTION}
post_upload_command = ${UPLOAD_POST_COMMAND}
${UPLOAD_CONFIG}
EOF
    cat /root/.dput.cf
    ls -l $PLUGIN_PACKAGE_DIR
    for changes in $PLUGIN_PACKAGE_DIR/*.changes ; do
        if [ -e "$changes" ] && dput debian "$changes"; then
            echo rm -f $PLUGIN_PACKAGE_DIR/*.{changes,deb,buildinfo,upload}
        else
            echo "Problem dput packages"
            return 1
        fi
    done
}

while [[ $# -gt 0 ]] ; do
    arg="$1"; shift
    case $arg in
        --help|-h)
            usage
            exit 0
            ;;
        "build")
            build_package
            ;;
        "upload")
            upload_package
            ;;
        "bash"|"sh")
            exec /bin/bash
            ;;
        *)
            echo "Illegal param $1"
            usage
            exit 0
            ;;
    esac
done
