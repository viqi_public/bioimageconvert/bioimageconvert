prefix=/usr
BINDIR=$(prefix)/bin
LIBDIR=$(prefix)/lib
LIBBIM=libsrc/libbioimg
LIBRAW=libsrc/libraw
LIBVPX=libsrc/libvpx
LIBWEBP=libsrc/libwebp
LCMS2=libsrc/lcms2
LIBX264=libsrc/libx264
LIBX265=libsrc/libx265
LIBOPENJPEG=libsrc/openjpeg
FFMPEG=libsrc/ffmpeg
LIBJPEGTURBO=libsrc/libjpeg-turbo
LIBGDCM=libsrc/gdcm
LIBGDCMBIN=libsrc/gdcmbin
LIBHDF=libsrc/libhdf5
LIBS=libs/linux
LIBTIFF=libsrc/libtiff
LIBPROJ=libsrc/proj
BUILD_PROJ=build/proj
LIBGEOTIFF=libsrc/libgeotiff/libgeotiff
BUILD_GEOTIFF=build/libgeotiff
LIBEXIV=libsrc/exiv2
BUILD_EXIV=build/exiv2
LIBOPENSLIDE=libsrc/openslide
BUILD_OPENSLIDE=build/openslide
LIBDICOM=libsrc/libdicom
BUILD_LIBDICOM=build/libdicom
LIBJXR=libsrc/jxrlib
LIBCZI=libsrc/libczi
BUILD_CZI=build/libczi
LIBS_PKG=libs/build_libs_2-4-0_linux.zip
PKG_CONFIG_PATH=$LIBVPX:$LIBX264:$LIBX265/build/linux
QMAKEOPTS=
VERSION=3.0.0
QMAKE=qmake

all : build_dirs buildlibs imgcnv

build_dirs:
	@echo "Starting library build into $(LIBS)"
	(mkdir -p $(LIBS))

$(LIBS)/libtiff.a:
	@echo
	@echo
	@echo "Building libtiff in $(LIBTIFF)"
	(cd $(LIBTIFF) && ./autogen.sh)
	(cd $(LIBTIFF) && ./configure --with-pic --enable-static --enable-cxx )
	$(MAKE) -C $(LIBTIFF)
	#(cd $(LIBTIFF) && $(MAKE) $(MAKEFLAGS))
	#(cd $(LIBTIFF) && $(MAKE))
	(cp $(LIBTIFF)/libtiff/.libs/libtiff.a $(LIBS)/libtiff.a)
	(cp $(LIBTIFF)/libtiff/.libs/libtiffxx.a $(LIBS)/libtiffxx.a)

$(LIBS)/libproj.a:
	@echo
	@echo
	@echo "Building proj in $(LIBPROJ)"
	(cd $(LIBPROJ) && export LIBTIFF_CFLAGS=-I../../$(LIBTIFF)/libtiff )
	(cd $(LIBPROJ) && export LIBTIFF_LIBS=../../$(LIBS)/libtiff.a )
	# (cd $(LIBPROJ) && ./autogen.sh)
	# (cd $(LIBPROJ) && ./configure --with-pic --enable-static --without-curl )
	# $(MAKE) -C $(LIBPROJ)
	# (cp $(LIBPROJ)/src/.libs/libproj.a $(LIBS)/libproj.a)
	(mkdir -p $(BUILD_PROJ))
	(cd $(BUILD_PROJ) && cmake ../../libsrc/proj \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_C_FLAGS=-fPIC \
		-DCMAKE_CXX_FLAGS=-fPIC -Wno-deprecated -Wno-deprecated-declarations \
		-DCMAKE_POSITION_INDEPENDENT_CODE=ON \
		-DCMAKE_CXX_WARNINGS=-Wno-deprecated -Wno-deprecated-declarations \
		-DBUILD_APPS:bool=off \
		-DBUILD_SHARED_LIBS:bool=off \
		-DBUILD_TESTING:bool=off \
		-DCMAKE_BUILD_TYPE=Release \
		-DENABLE_CURL:bool=off \
		-DTIFF_INCLUDE_DIR=../../$(LIBTIFF)/libtiff \
		-DTIFF_LIBRARY_RELEASE=../../$(LIBS)/libtiff.a )
	$(MAKE) -C $(BUILD_PROJ)
	(cp $(BUILD_PROJ)/lib/libproj.a $(LIBS)/libproj.a)

$(LIBS)/libgeotiff.a: $(LIBS)/libproj.a $(LIBS)/libtiff.a
	@echo
	@echo
	@echo "Building libgeotiff in $(LIBGEOTIFF)"
	# we are using system libs though only for tools that are not being used, should be no issues here
	(cd $(LIBGEOTIFF) && export LIBTIFF_CFLAGS=-I../../../$(LIBTIFF)/libtiff )
	(cd $(LIBGEOTIFF) && export LIBTIFF_LIBS=../../../$(LIBS)/libtiff.a )
	(cd $(LIBGEOTIFF) && export PROJ_CFLAGS=-I../../../$(LIBPROJ)/src )
	(cd $(LIBGEOTIFF) && export PROJ_LIBS=../../../$(LIBS)/libproj.a )
	(cd $(LIBGEOTIFF) && ./autogen.sh)
	#(cd $(LIBGEOTIFF) && ./configure --with-pic --enable-static --with-proj=../../../$(LIBPROJ)/src --with-libtiff=../../../$(LIBTIFF)/libtiff --enable-shared=no)
	#(cd $(LIBGEOTIFF) && ./configure --with-pic --enable-static --with-libtiff=../../../$(LIBTIFF)/libtiff)
	(cd $(LIBGEOTIFF) && ./configure --with-pic --enable-static)
	#(cd $(LIBGEOTIFF) && $(MAKE) $(MAKEFLAGS)) #make dist
	$(MAKE) -C $(LIBGEOTIFF)
	#(cd $(LIBGEOTIFF) && $(MAKE))
	(cp $(LIBGEOTIFF)/.libs/libgeotiff.a $(LIBS)/libgeotiff.a)


$(LIBS)/libdicom.a:
	@echo
	@echo
	@echo "Building libdicom in $(LIBDICOM)"
	(mkdir -p $(BUILD_LIBDICOM))
	(meson setup $(BUILD_LIBDICOM) $(LIBDICOM) \
		--buildtype release \
		--default-library static \
		-Dc_args="-fPIC" \
		-Dcpp_args="-fPIC")
	(meson compile -C $(BUILD_LIBDICOM))
	(cp $(BUILD_LIBDICOM)/libdicom.a $(LIBS)/libdicom.a)

# -Dpic=true # starting with 0.36.0
# -Dinclude_directories="-I../../$(LIBTIFF)/libtiff"
# -Dlink_args="../../$(LIBS)/libtiff.a"
$(LIBS)/libopenslide.a: $(LIBS)/libtiff.a $(LIBS)/libdicom.a
	@echo
	@echo
	@echo "Building openslide in $(LIBOPENSLIDE)"
	(mkdir -p $(BUILD_OPENSLIDE))
	(meson setup $(BUILD_OPENSLIDE) $(LIBOPENSLIDE) \
		--buildtype release \
		--default-library static \
		-Dc_args="-fPIC -I../../$(LIBTIFF)/libtiff -I../../$(LIBDICOM)/include" \
		-Dcpp_args="-fPIC -I../../$(LIBTIFF)/libtiff -I../../$(LIBDICOM)/include")
	(meson compile -C $(BUILD_OPENSLIDE))
	# (cp $(BUILD_OPENSLIDE)/common/libopenslide-common.a $(LIBS)/libopenslide-common.a)
	(cp $(BUILD_OPENSLIDE)/src/libopenslide.a $(LIBS)/libopenslide.a)

$(LIBS)/libexiv2.a:
	@echo
	@echo
	@echo "Building libexiv in $(LIBEXIV)"
	(mkdir -p $(BUILD_EXIV))
	(cd $(BUILD_EXIV) && cmake ../../libsrc/exiv2 \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_C_FLAGS=-fPIC \
		-DCMAKE_CXX_FLAGS=-fPIC -Wno-deprecated -Wno-deprecated-declarations \
		-DCMAKE_POSITION_INDEPENDENT_CODE=ON \
		-DCMAKE_CXX_WARNINGS=-Wno-deprecated -Wno-deprecated-declarations \
		-DEXIV2_BUILD_SAMPLES:bool=off \
		-DEXIV2_BUILD_EXIV2_COMMAND:bool=off \
		-DBUILD_SHARED_LIBS:bool=off -DEXIV2_ENABLE_INIH:bool=off -DEXIV2_ENABLE_BROTLI:bool=off)
	#(cd $(BUILD_EXIV) && $(MAKE) $(MAKEFLAGS))
	#(cd $(BUILD_EXIV) && $(MAKE))
	$(MAKE)  -C $(BUILD_EXIV)
	(cp $(BUILD_EXIV)/lib/libexiv2.a $(LIBS)/libexiv2.a)
	#(cp $(BUILD_EXIV)/lib/libexiv2-xmp.a $(LIBS)/libexiv2-xmp.a)

$(LIBS)/libjpegxr.a:
	@echo
	@echo
	@echo "Building jxrlib in $(LIBJXR)"
	#(cd $(LIBJXR) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C $(LIBJXR)
	(cp $(LIBJXR)/build/libjpegxr.a $(LIBS)/libjpegxr.a)
	(cp $(LIBJXR)/build/libjxrglue.a $(LIBS)/libjxrglue.a)

$(LIBS)/libczi.a:
	@echo
	@echo
	@echo "Building libczi in $(LIBCZI)"
	-mkdir -p $(BUILD_CZI)
	(cd $(BUILD_CZI) && cmake ../../libsrc/libczi \
	    -DCMAKE_BUILD_TYPE=Release \
	    -DCMAKE_C_FLAGS=-fPIC \
	    -DCMAKE_CXX_FLAGS=-fPIC \
	    -DCMAKE_POSITION_INDEPENDENT_CODE=ON \
	    -DLIBCZI_BUILD_UNITTESTS:bool=off \
	    -DLIBCZI_BUILD_CZICMD:bool=off \
	    -DLIBCZI_BUILD_DYNLIB:bool=off)
	$(MAKE) -C  $(BUILD_CZI)
	#(cd $(BUILD_CZI) && $(MAKE) $(MAKEFLAGS))
	#(cd $(BUILD_CZI) && $(MAKE))
	(cp $(BUILD_CZI)/Src/libCZI/liblibCZIStatic.a $(LIBS)/libczi.a)

buildlibs: build_dirs $(LIBS)/libopenslide.a $(LIBS)/libexiv2.a $(LIBS)/libczi.a $(LIBS)/libjpegxr.a $(LIBS)/libgeotiff.a
	@echo "Ensured dependencies"

install:
	install -d $(DESTDIR)$(BINDIR)
	install bin/imgcnv $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(LIBDIR)
	install bin/libimgcnv.so.3 $(DESTDIR)$(LIBDIR)

qmake:
	(cd $(LIBBIM) && $(QMAKE) $(QMAKEOPTS) bioimage.pro)
	(cd src && $(QMAKE) $(QMAKEOPTS) imgcnv.pro)
	(cd src_dylib && $(QMAKE) $(QMAKEOPTS) libimgcnv.pro)

imgcnv: buildlibs qmake
	@echo
	@echo
	@echo "Building libbioimage in $(LIBBIM)"
	#(cd $(LIBBIM) && $(MAKE))
	$(MAKE) -C $(LIBBIM)

	@echo
	@echo
	@echo "Building imgcnv"
	#(cd src && $(MAKE))
	$(MAKE) -C src

	@echo
	@echo
	@echo "Building libimgcnv"
	#(cd src_dylib && $(MAKE))
	$(MAKE) -C src_dylib

full: buildlibs qmake
	-mkdir -p build/.generated/obj
	-mkdir -p $(LIBS)

	@echo
	@echo
	@echo "Building libbioimage in $(LIBBIM)"
	(cd $(LIBBIM) && $(MAKE))

	@echo
	@echo
	@echo "Building libvpx 1.5.0 in $(LIBVPX)"
	#git clone https://chromium.googlesource.com/webm/libvpx
	#git checkout v1.5.0
	(cd $(LIBVPX) && chmod -f u+x configure)
	(cd $(LIBVPX) && chmod -f u+x build/make/version.sh)
	(cd $(LIBVPX) && chmod -f u+x build/make/rtcd.pl)
	(cd $(LIBVPX) && chmod -f u+x build/make/gen_asm_deps.sh)
	(cd $(LIBVPX) && chmod -f u+x build/make/gen_asm_deps.sh)
	(cd $(LIBVPX) && ./configure --enable-vp8 --enable-vp9 --enable-pic --disable-examples --disable-unit-tests --disable-docs )
	#(cd $(LIBVPX) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C $(LIBVPX)
	(cp $(LIBVPX)/libvpx.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libwebp 0.4.3 in $(LIBWEBP)"
	(cd $(LIBWEBP) && chmod -f u+x configure)
	(cd $(LIBWEBP) && chmod -f u+x autogen.sh)
	(cd $(LIBWEBP) && chmod -f u+x config.guess)
	(cd $(LIBWEBP) && chmod -f u+x config.sub)
	(cd $(LIBWEBP) && ./configure --with-pic --enable-libwebpmux --enable-libwebpdemux --enable-libwebpdecoder)
	#(cd $(LIBWEBP) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C  $(LIBWEBP)
	(cp $(LIBWEBP)/src/.libs/libwebp.a $(LIBS)/)
	(cp $(LIBWEBP)/src/mux/.libs/libwebpmux.a $(LIBS)/)
	(cp $(LIBWEBP)/src/demux/.libs/libwebpdemux.a $(LIBS)/)

	@echo
	@echo
	@echo "Building lcms 2.7.0 in $(LCMS2)"
	(cd $(LCMS2) && chmod -f u+x configure)
	(cd $(LCMS2) && chmod -f u+x autogen.sh)
	(cd $(LCMS2) && chmod -f u+x config.guess)
	(cd $(LCMS2) && chmod -f u+x config.sub)
	(cd $(LCMS2) && ./configure --with-pic --enable-shared=no --without-zlib )
	#(cd $(LCMS2) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C  $(LCMS2)
	(cp $(LCMS2)/src/.libs/liblcms2.a $(LIBS)/)

	@echo
	@echo
	@echo "Building openjpeg 2.1.0 in $(LIBOPENJPEG)"
	(cd $(LIBOPENJPEG) && cmake . -DCMAKE_C_FLAGS=-fPIC -DBUILD_SHARED_LIBS:bool=off )
	#(cd $(LIBOPENJPEG) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C $(LIBOPENJPEG)
	(cp $(LIBOPENJPEG)/bin/libopenjp2.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libx264 20150223 in $(LIBX264)"
	(cd $(LIBX264) && chmod -f u+x configure)
	(cd $(LIBX264) && chmod -f u+x version.sh)
	(cd $(LIBX264) && chmod -f u+x config.guess)
	(cd $(LIBX264) && chmod -f u+x config.sub)
	(cd $(LIBX264) && ./configure --enable-pic --enable-static --disable-opencl )
	#(cd $(LIBX264) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C $(LIBX264)
	(cp $(LIBX264)/libx264.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libx265 20150509 in $(LIBX265)"
	#(cd $(LIBX265)/build/linux && chmod -f u+x make-Makefiles.bash)
	#(cd $(LIBX265)/build/linux && bash make-Makefiles.bash )
	(cd $(LIBX265)/build/linux && cmake -G "Unix Makefiles" ../../source -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC )
	#(cd $(LIBX265)/build/linux && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C $(LIBX265)
	(cp $(LIBX265)/build/linux/libx265.a $(LIBS)/)

	@echo
	@echo
	@echo "Building libjpeg-turbo 1.4.0 in $(LIBJPEGTURBO)"
	(cd $(LIBJPEGTURBO) && chmod -f u+x configure)
	(cd $(LIBJPEGTURBO) && chmod -f u+x config.guess)
	(cd $(LIBJPEGTURBO) && chmod -f u+x config.sub)
	(cd $(LIBJPEGTURBO) && ./configure --enable-pic --enable-static --enable-shared=no )
	(cd $(LIBJPEGTURBO) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C  $(LIBJPEGTURBO)
	(cp $(LIBJPEGTURBO)/.libs/libturbojpeg.a $(LIBS)/)
	@echo
	@echo
	@echo "Building libGDCM 2.4.4 in $(LIBGDCM)"
	-mkdir -p $(LIBGDCMBIN)
	(cd $(LIBGDCMBIN) && cmake ../gdcm -DCMAKE_CXX_FLAGS=-fPIC -DCMAKE_C_FLAGS=-fPIC -DGDCM_USE_OPENJPEG_V2=ON )
	(cd $(LIBGDCMBIN) && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C $(LIBGDCMBIN)
	(cp $(LIBGDCMBIN)/bin/*.a $(LIBS)/gdcm/)

	@echo
	@echo
	@echo "Building ffmpeg 2.6.3 in $(FFMPEG)"
	(cd $(FFMPEG)/ffmpeg && chmod -f u+x configure)
	(cd $(FFMPEG)/ffmpeg && chmod -f u+x version.sh)
	-mkdir -p $(FFMPEG)/ffmpeg-obj
	(cd $(FFMPEG)/ffmpeg-obj && ../ffmpeg/configure \
		--enable-static --disable-shared --enable-pic --enable-gray \
		--prefix=../ffmpeg-out \
		--extra-cflags="-fPIC -I../../libvpx -I../../libx264 -I../../libx265/source" \
		--extra-cxxflags="-fPIC -I../../libvpx -I../../libx264 -I../../libx265/source" \
		--extra-ldflags="-fPIC -L../../libvpx -L../../libx264 -L../../libx265/build/linux" \
		--enable-gpl --enable-version3 --enable-runtime-cpudetect --enable-pthreads --enable-swscale \
		--disable-ffserver --disable-ffplay --disable-network --disable-ffmpeg --disable-devices \
		--disable-frei0r --disable-libass --disable-libcelt --disable-libopencore-amrnb --disable-libopencore-amrwb \
		--disable-libfreetype --disable-libgsm --disable-libmp3lame --disable-libnut --disable-librtmp \
		--disable-libspeex --disable-libvorbis \
		--enable-bzlib --enable-zlib \
		--disable-libopenjpeg --disable-libschroedinger \
		--enable-libtheora --enable-libvpx --enable-libx264 --enable-encoder=libx264 --enable-libx265 --enable-libxvid \
		--disable-libvo-aacenc --disable-libvo-amrwbenc )

	(cd $(FFMPEG)/ffmpeg-obj && $(MAKE) $(MAKEFLAGS) all install)

	(cp -f $(FFMPEG)/ffmpeg-out/lib/libavcodec.a $(LIBS)/)
	(cp -f $(FFMPEG)/ffmpeg-out/lib/libavformat.a $(LIBS)/)
	(cp -f $(FFMPEG)/ffmpeg-out/lib/libavutil.a $(LIBS)/)
	(cp -f $(FFMPEG)/ffmpeg-out/lib/libswscale.a $(LIBS)/)
	-mkdir -p $(FFMPEG)/include
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libavcodec $(FFMPEG)/include/)
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libavformat $(FFMPEG)/include/)
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libavutil $(FFMPEG)/include/)
	(cp -Rf $(FFMPEG)/ffmpeg-out/include/libswscale $(FFMPEG)/include/)


	@echo
	@echo
	@echo "Building imgcnv"
	#(cd src && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C src

	@echo
	@echo
	@echo "Building libimgcnv"
	#(cd src_dylib && $(MAKE) $(MAKEFLAGS))
	$(MAKE) -C src_dylib

clean: qmake
	(cd src && $(MAKE) clean)
	rm -rf build/.generated *.o *~
	rm -rf $(LIBBIM)/*.o $(LIBBIM)/*~ $(LIBBIM)/.qmake.stash
	(cd $(LIBBIM) && $(MAKE) clean)
#	(cd $(LIBVPX) && $(MAKE) clean)
	rm -rf $(LIBBIM)/Makefile src/Makefile src_dylib/Makefile



cleanfull: clean
	(cd $(LIBVPX) && $(MAKE) clean)
	(cd $(LIBX264) && $(MAKE) clean)
	rm -rf $(FFMPEG)/ffmpeg-obj
	rm -rf $(FFMPEG)/ffmpeg-out
#	(cd $(FFMPEG)/ffmpeg && $(MAKE) clean)

distclean: clean
	#m -rf $(LIBS) $(LIBS_PKG) $(LIBCZI) $(LIBHDF)
	rm -f bin/imgcnv
	rm -f bin/libimgcnv.so*



.FORCE: imgcnv
