/*****************************************************************************
  NANOSCOPE
  Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    01/10/2005 12:17 - First creation
    02/08/2005 22:30 - Support for incomplete image sections
    09/12/2005 17:34 - updated to api version 1.3
    2021-11-09 15:50 - cleaned-up

  ver : 4
*****************************************************************************/

#ifndef BIM_NANOSCOPE_FORMAT_H
#define BIM_NANOSCOPE_FORMAT_H

#include <cstdio>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>
#include <xstring.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *nanoscopeGetFormatHeader(void);
}

namespace bim {

#define BIM_NANOSCOPE_MAGIC_SIZE 11
const char nanoscopeMagic[12] = "\\*File list";
const char nanoscopeMagicF[18] = "\\*Force file list";

class NanoscopeImg {
public:
    NanoscopeImg() : width(0), height(0), data_offset(0), xR(0.0), yR(0.0), zR(0.0) {}

    size_t width = 0, height = 0;
    size_t data_offset=0;
    bim::xstring metaText;
    double xR, yR, zR; // pixel resolution for XYZ
    std::string data_type;
};

typedef std::vector<NanoscopeImg> NanoImgVector;

class NanoscopeParams {
public:
    NanoscopeParams() { }
    bim::ImageInfo i;
    bim::xstring metaText;
    NanoImgVector imgs;
    size_t channels = 1;
    size_t num_pages = 1;
};

//----------------------------------------------------------------------------
// LineReader
//----------------------------------------------------------------------------

class LineReader {
public:
    LineReader(FormatHandle *newHndl);
    ~LineReader();

    bim::xstring line();
    const char *line_c_str();

    bool readNextLine();
    bool isEOLines();

protected:
    bim::xstring prline;

private:
    FormatHandle *fmtHndl=0;
    std::vector<char> buf;
    size_t bufpos=0;
    size_t bufsize=0;
    bool eolines=false;

    bool loadBuff();
};


class TokenReader : public LineReader {
public:
    TokenReader(FormatHandle *newHndl);

    bool isKeyTag();
    bool isImageTag();
    bool isEndTag();
    bool compareTag(const bim::xstring &tag); // compare exactly the tag
    bool isTag(const bim::xstring &tag);      // only verify if this token contains tag
    int readParamInt(const bim::xstring &tag);
    void readTwoParamDouble(const bim::xstring &tag, double &p1, double &p2);
    void readTwoDoubleRemString(const bim::xstring &tag, double &p1, double &p2, bim::xstring &str);
    std::string readImageDataString(const bim::xstring &tag);
};

} // namespace bim

#endif // BIM_NANOSCOPE_FORMAT_H
