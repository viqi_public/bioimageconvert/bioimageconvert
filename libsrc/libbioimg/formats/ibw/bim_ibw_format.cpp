/*****************************************************************************
  Igor binary file format v5 (IBW)
  Copyright (c) 2005 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    10/19/2005 16:03 - First creation
    2021-11-09 15:50 - cleaned-up

  ver : 2
*****************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cfloat>
#include <climits>
#include <cmath>
#include <cstring>

#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>
#include <xdatetime.h>

#include "bim_ibw_format.h"

using namespace bim;

//****************************************************************************
// INTERNAL STRUCTURES
//****************************************************************************

void swapBinHeader5(BinHeader5 *bh) {
    swapShort((uint16 *)&bh->checksum);
    swapLong((uint32 *)&bh->wfmSize);

    swapLong((uint32 *)&bh->formulaSize);
    swapLong((uint32 *)&bh->noteSize);
    swapLong((uint32 *)&bh->dataEUnitsSize);
    swapLong((uint32 *)&bh->sIndicesSize);

    swapLong((uint32 *)&bh->wfmSize);
    swapLong((uint32 *)&bh->wfmSize);

    for (int i = 0; i < IBW_MAXDIMS; ++i) {
        swapLong((uint32 *)&bh->dimEUnitsSize[i]);
        swapLong((uint32 *)&bh->dimLabelsSize[i]);
    }
}

void swapWaveHeader5(WaveHeader5 *wh) {
    swapLong((uint32 *)&wh->creationDate);
    swapLong((uint32 *)&wh->modDate);
    swapLong((uint32 *)&wh->npnts);
    swapShort((uint16 *)&wh->type);
    swapShort((uint16 *)&wh->fsValid);
    swapDouble((float64 *)&wh->topFullScale);
    swapDouble((float64 *)&wh->botFullScale);

    for (int i = 0; i < IBW_MAXDIMS; ++i) {
        swapLong((uint32 *)&wh->nDim[i]);
        swapDouble((float64 *)&wh->sfA[i]);
        swapDouble((float64 *)&wh->sfB[i]);
    }
}

void ibwReadNote(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    IbwParams *par = (IbwParams *)fmtHndl->internalParams;

    size_t buf_size = par->bh.noteSize;
    par->note.resize(buf_size + 1);
    char *buf = &par->note[0];
    buf[buf_size] = '\0';

    if (xseek(fmtHndl, par->notes_offset, SEEK_SET) != 0) return;
    if (xread(fmtHndl, buf, buf_size, 1) != 1) return;

    for (int i = 0; i < buf_size; ++i)
        if (buf[i] == 0x0d) buf[i] = 0x0a;
}

void ibwGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    IbwParams *ibwPar = (IbwParams *)fmtHndl->internalParams;
    ImageInfo *info = &ibwPar->i;
    //*info = initImageInfo();

    if (fmtHndl->stream == NULL) return;
    if (xseek(fmtHndl, 0, SEEK_SET) != 0) return;
    if (xread(fmtHndl, &ibwPar->bh, 1, sizeof(BinHeader5)) != sizeof(BinHeader5)) return;

    // test if little-endian
    if (memcmp(&ibwPar->bh.version, ibwMagicWin, BIM_IBW_MAGIC_SIZE) == 0)
        ibwPar->little_endian = true;
    else
        ibwPar->little_endian = false;

    // swap structure elements if running on Big endian machine...
    if ((bim::bigendian) && (ibwPar->little_endian == true)) {
        swapBinHeader5(&ibwPar->bh);
    }

    if (xread(fmtHndl, &ibwPar->wh, 1, sizeof(WaveHeader5)) != sizeof(WaveHeader5)) return;

    // swap structure elements if running on Big endian machine...
    if ((bim::bigendian) && (ibwPar->little_endian == true)) {
        swapWaveHeader5(&ibwPar->wh);
    }

    /*
      info->resUnits = BIM_RES_mim;
      info->xRes = nimg.xR / nimg.width;
      info->yRes = nimg.yR / nimg.height;
      */

    // get correct type size
    switch (ibwPar->wh.type) {
        case NT_CMPLX:
            ibwPar->real_bytespp = 8;
            ibwPar->real_type = bim::DataType::TAG_SRATIONAL;
            break;
        case NT_FP32:
            ibwPar->real_bytespp = 4;
            ibwPar->real_type = bim::DataType::TAG_FLOAT;
            break;
        case NT_FP64:
            ibwPar->real_bytespp = 8;
            ibwPar->real_type = bim::DataType::TAG_DOUBLE;
            break;
        case NT_I8:
            ibwPar->real_bytespp = 1;
            ibwPar->real_type = bim::DataType::TAG_BYTE;
            break;
        case NT_I16:
            ibwPar->real_bytespp = 2;
            ibwPar->real_type = bim::DataType::TAG_SHORT;
            break;
        case NT_I32:
            ibwPar->real_bytespp = 4;
            ibwPar->real_type = bim::DataType::TAG_LONG;
            break;
        default:
            ibwPar->real_bytespp = 1;
            ibwPar->real_type = bim::DataType::TAG_BYTE;
    }

    // prepare needed vars
    ibwPar->data_offset = sizeof(BinHeader5) + sizeof(WaveHeader5) - 4; //the last pointer is actually initial data
    ibwPar->data_size = ibwPar->wh.npnts * ibwPar->real_bytespp;
    ibwPar->formula_offset = ibwPar->data_offset + ibwPar->data_size;
    ibwPar->notes_offset = ibwPar->formula_offset + ibwPar->bh.formulaSize;

    // set image parameters
    info->width = ibwPar->wh.nDim[0];
    info->height = ibwPar->wh.nDim[1];
    info->samples = 1;
    info->number_pages = ibwPar->wh.nDim[2];
    info->imageMode = bim::ImageModes::IM_GRAYSCALE;
    //info->number_z = info->number_pages;

    // by now we'll normalize all data
    info->depth = 8;
    info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    if (info->samples > 1)
        info->imageMode = bim::ImageModes::IM_MULTI;

    ibwReadNote(fmtHndl);
}

//----------------------------------------------------------------------------
// READ PROC
//----------------------------------------------------------------------------

static int read_ibw_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    IbwParams *ibwPar = (IbwParams *)fmtHndl->internalParams;
    ImageInfo *info = &ibwPar->i;
    if (fmtHndl->stream == NULL) return 1;

    // get needed page
    if (fmtHndl->pageNumber >= info->number_pages) fmtHndl->pageNumber = info->number_pages - 1;
    uint64 page = fmtHndl->pageNumber;

    //allocate image
    ImageBitmap *img = fmtHndl->image;
    if (allocImg(fmtHndl, info, img) != 0) return 1;

    //read page
    bim::uint64 ch_num_points = info->width * info->height;
    bim::uint64 ch_size = ch_num_points * ibwPar->real_bytespp;
    uchar *chbuf = new uchar[ch_size];


    bim::uint64 ch_offset = ibwPar->data_offset + (ch_size * page);

    xprogress(fmtHndl, 0, 10, "Reading IBW");

    if (xseek(fmtHndl, ch_offset, SEEK_SET) != 0) return 1;
    if (xread(fmtHndl, chbuf, ch_size, 1) != 1) return 1;

    // swap if neede
    if ((bim::bigendian) && (ibwPar->little_endian == true)) {
        if ((ibwPar->wh.type == NT_FP32) || (ibwPar->wh.type == NT_I32))
            swapArrayOfLong((uint32 *)chbuf, ch_size / 4);

        if (ibwPar->wh.type == NT_FP64)
            swapArrayOfDouble((float64 *)chbuf, ch_size / 8);

        if (ibwPar->wh.type == NT_I16)
            swapArrayOfShort((uint16 *)chbuf, ch_size / 2);

        if (ibwPar->wh.type == NT_CMPLX)
            swapArrayOfLong((uint32 *)chbuf, ch_size / 4);
    }

    // normalize and copy
    if (ibwPar->wh.type == NT_FP32) {
        float max_val = FLT_MIN;
        float min_val = FLT_MAX;
        float *pb = (float *)chbuf;
        long x = 0;

        // find min and max
        for (x = 0; x < ch_num_points; ++x) {
            if (*pb > max_val) max_val = *pb;
            if (*pb < min_val) min_val = *pb;
            ++pb;
        }

        double range = ((double)max_val - min_val) / 256.0;
        if (range == 0) range = 256;

        pb = (float *)chbuf;
        uchar *p = (uchar *)img->bits[0];

        // direct data copy
        for (x = 0; x < ch_num_points; ++x) {
            *p = bim::trim<uint8, int>(bim::round<int>(((double) * pb - min_val) / range));
            ++pb;
            ++p;
        }

        /*
    // copy transposing the data
    long line_size = info->width;
    long y=0;

    for (y=0; y<info->height; ++y)
    {
      p = ( (uchar *) img->bits[0]) + y;
      for (x=0; x<info->width; ++x)
      {
        *p = iTrimUC ( (*pb - min_val) / range );
        ++pb;
        p+=line_size;
      }
    }
    */

    } // if (ibwPar->wh.type == NT_FP32)

    delete[] chbuf;
    return 0;
}


//----------------------------------------------------------------------------
// META DATA PROC
//----------------------------------------------------------------------------

bim::uint ibw_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    IbwParams *par = (IbwParams *)fmtHndl->internalParams;

    //hash->set_value( bim::PIXEL_RESOLUTION_X, par->pixel_size[0] );
    //hash->set_value( bim::PIXEL_RESOLUTION_Y, par->pixel_size[1] );
    //hash->set_value( bim::PIXEL_RESOLUTION_Z, par->pixel_size[2] );

    // Parse some of the notes
    hash->parse_ini(par->note, ":", "IBW/");

    //Date: Tue, Sep 20, 2005
    //Time: 10:58:49 AM
    bim::DateTime dt = bim::DateTime::from_strings(hash->get_value("IBW/Date"), hash->get_value("IBW/Time"), "%a, %b %d, %Y", "%I:%M:%S %p");
    hash->set_value(bim::DOCUMENT_DATETIME, dt.to_string_iso8601());

    return 0;
}


//****************************************************************************
// FORMAT DEMANDED FUNTIONS
//****************************************************************************


//----------------------------------------------------------------------------
// PARAMETERS, INITS
//----------------------------------------------------------------------------

int ibwValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_IBW_MAGIC_SIZE) return -1;
    if (memcmp(magic, ibwMagicWin, BIM_IBW_MAGIC_SIZE) == 0) return 0;
    if (memcmp(magic, ibwMagicMac, BIM_IBW_MAGIC_SIZE) == 0) return 0;
    return -1;
}

FormatHandle ibwAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void ibwCloseImageProc(FormatHandle *fmtHndl);
void ibwReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    ibwCloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------
void ibwCloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    if (fmtHndl->internalParams != NULL) {
        IbwParams *ibwPar = (IbwParams *)fmtHndl->internalParams;
        delete ibwPar;
    }
    fmtHndl->internalParams = NULL;
}

bim::uint ibwOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) ibwCloseImageProc(fmtHndl);
    fmtHndl->internalParams = (void *)new IbwParams();

    fmtHndl->io_mode = io_mode;
    xopen(fmtHndl);
    if (!fmtHndl->stream) {
        ibwCloseImageProc(fmtHndl);
        return 1;
    };

    if (io_mode == bim::ImageIOModes::IO_READ) {
        ibwGetImageInfo(fmtHndl);
        return 0;
    }
    return 1;
}

//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint ibwGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    IbwParams *ibwPar = (IbwParams *)fmtHndl->internalParams;

    return (bim::uint)ibwPar->i.number_pages;
}


ImageInfo ibwGetImageInfoProc(FormatHandle *fmtHndl, bim::uint) {
    if (fmtHndl == NULL) return ImageInfo();
    IbwParams *ibwPar = (IbwParams *)fmtHndl->internalParams;
    return ibwPar->i;
}

//----------------------------------------------------------------------------
// READ/WRITE
//----------------------------------------------------------------------------

bim::uint ibwReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;
    fmtHndl->pageNumber = page;
    return read_ibw_image(fmtHndl);
}

bim::uint ibwWriteImageProc(FormatHandle *) {
    return 1;
}



//****************************************************************************
//
// EXPORTED FUNCTION
//
//****************************************************************************

FormatItem ibwItems[1] = {{ 
    "IBW",                        // short name, no spaces
    "AFM: Igor binary file format v5", // Long format name
    "ibw",                        // pipe "|" separated supported extension list
    1,                            //canRead;      // 0 - NO, 1 - YES
    0,                            //canWrite;     // 0 - NO, 1 - YES
    1,                            //canReadMeta;  // 0 - NO, 1 - YES
    0,                            //canWriteMeta; // 0 - NO, 1 - YES
    0,                            //canWriteMultiPage;   // 0 - NO, 1 - YES
    { 0, 0, 0, 1, 0, 0, 0, 1 } // constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
}};

FormatHeader ibwHeader = {

    sizeof(FormatHeader),
    "2.0.0",
    "IBW",
    "IBW",

    12,                 // 0 or more, specify number of bytes needed to identify the file
    { 1, 1, ibwItems }, //dimJpegSupported,

    ibwValidateFormatProc,
    ibwAquireFormatProc, //AquireFormatProc
    ibwReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    ibwOpenImageProc,  //OpenImageProc
    ibwCloseImageProc, //CloseImageProc

    // info
    ibwGetNumPagesProc,  //GetNumPagesProc
    ibwGetImageInfoProc, //GetImageInfoProc


    // read/write
    ibwReadImageProc, //ReadImageProc
    NULL,             //WriteImageProc
    NULL,             //ReadImageTileProc
    NULL,             //WriteImageTileProc
    NULL,             //ReadImageLineProc
    NULL,             //WriteImageLineProc
    NULL,             //ReadImageRegionProc
    NULL,             //WriteImageRegionProc
    ibw_append_metadata, //AppendMetaDataProc
};

extern "C" {
FormatHeader *ibwGetFormatHeader(void) { 
    return &ibwHeader; 
}
} // extern C
