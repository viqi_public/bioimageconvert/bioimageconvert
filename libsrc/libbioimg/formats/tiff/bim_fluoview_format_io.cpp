/*****************************************************************************
  TIFF FLUOVIEW IO
  Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>

  IMPLEMENTATION

  Programmer: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
    03/29/2004 22:23 - First creation

  Ver : 2
*****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>

#include <bim_img_format_utils.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>
#include <xdatetime.h>

#include "bim_tiff_format.h"
#include "bim_tiny_tiff.h"
#include "memio.h"
#include "xtiffio.h"

#include <Eigen/Dense>

using namespace bim;

unsigned int tiffGetNumberOfPages(TiffParams *tiffpar);

void change_0_to_n(char *str, long size) {
    for (long i = 0; i < size; i++)
        if (str[i] == '\0') str[i] = '\n';
}

//----------------------------------------------------------------------------
// PSIA MISC FUNCTIONS
//----------------------------------------------------------------------------

bool isValidTiffFluoview(TiffParams *par) {
    if (!par) return false;
    if (par->tiff->tif_flags & TIFF_BIGTIFF) return false;
    return (par->ifds.tagPresentInFirstIFD(BIM_MMHEADER) &&
            par->ifds.tagPresentInFirstIFD(BIM_MMSTAMP) &&
            par->ifds.tagPresentInFirstIFD(BIM_MMUSERBLOCK));
}

bool isValidTiffAndor(TiffParams *par) {
    if (!par) return false;
    if (par->tiff->tif_flags & TIFF_BIGTIFF) return false;
    return (par->ifds.tagPresentInFirstIFD(BIM_MMHEADER) &&
            par->ifds.tagPresentInFirstIFD(BIM_MMSTAMP) &&
            par->ifds.tagPresentInFirstIFD(BIM_ANDORBLOCK));
}

void doSwabMMHEAD(MM_BIM_INFO *mmdi) {
    TIFFSwabLong((bim::uint32 *)&mmdi->Size);
    TIFFSwabDouble((double *)&mmdi->Origin);
    TIFFSwabDouble((double *)&mmdi->Resolution);
}

void doSwabMMHEAD(MM_HEAD *mmh) {
    TIFFSwabShort((bim::uint16 *)&mmh->HeaderFlag);
    TIFFSwabLong((bim::uint32 *)&mmh->Data);
    TIFFSwabLong((bim::uint32 *)&mmh->NumberOfColors);
    TIFFSwabLong((bim::uint32 *)&mmh->MM_256_Colors);
    TIFFSwabLong((bim::uint32 *)&mmh->MM_All_Colors);
    TIFFSwabLong((bim::uint32 *)&mmh->CommentSize);
    TIFFSwabLong((bim::uint32 *)&mmh->Comment);
    TIFFSwabLong((bim::uint32 *)&mmh->SpatialPosition);
    TIFFSwabShort((bim::uint16 *)&mmh->MapType);
    TIFFSwabLong((bim::uint32 *)&mmh->MapMin);
    TIFFSwabLong((bim::uint32 *)&mmh->MapMax);
    TIFFSwabLong((bim::uint32 *)&mmh->MinValue);
    TIFFSwabLong((bim::uint32 *)&mmh->MaxValue);
    TIFFSwabLong((bim::uint32 *)&mmh->Map);
    TIFFSwabLong((bim::uint32 *)&mmh->Gamma);
    TIFFSwabLong((bim::uint32 *)&mmh->Offset);
    TIFFSwabLong((bim::uint32 *)&mmh->ThumbNail);
    TIFFSwabLong((bim::uint32 *)&mmh->UserFieldSize);
    TIFFSwabLong((bim::uint32 *)&mmh->UserFieldHandle);

    doSwabMMHEAD(&mmh->Gray);
    for (int i = 0; i < SPATIAL_DIMENSION; ++i)
        doSwabMMHEAD(&mmh->DimInfo[i]);
}

void printMM_BIM_INFO(MM_BIM_INFO *mmdi) {
    printf("\nMM_BIM_INFO Name: %s\n", mmdi->Name);
    printf("MM_BIM_INFO size: %d\n", mmdi->Size);
    printf("MM_BIM_INFO origin: %f\n", mmdi->Origin);
    printf("MM_BIM_INFO resolution: %f\n", mmdi->Resolution);
    printf("MM_BIM_INFO Units: %s\n", mmdi->Units);
}

void printMMHEADER(MM_HEAD *mmh) {
    printf("\nMMHEAD HeaderFlag: %.4X\n", mmh->HeaderFlag);
    printf("\nMMHEAD ImageType: %.1X\n", mmh->ImageType);
    printf("\nMMHEAD Name: %s\n", mmh->Name);
    printf("\nMMHEAD Status: %.1X\n", mmh->Status);
    printf("\nMMHEAD Data: %.8X\n", mmh->Data);
    printf("\nMMHEAD NumberOfColors: %.8X\n", mmh->NumberOfColors);
    printf("\nMMHEAD MM_256_Colors: %.8X\n", mmh->MM_256_Colors);
    printf("\nMMHEAD MM_All_Colors: %.8X\n", mmh->MM_All_Colors);
    printf("\nMMHEAD CommentSize: %.8X\n", mmh->CommentSize);
    printf("\nMMHEAD Comment: %.8X\n", mmh->Comment);

    for (int i = 0; i < SPATIAL_DIMENSION; ++i)
        printMM_BIM_INFO(&mmh->DimInfo[i]);
}

int fluoviewGetInfo(TiffParams *par) {
    if (par == NULL) return 1;
    if (par->tiff == NULL) return 1;
    if (!par->ifds.isValid()) return 1;

    ImageInfo *info = &par->info;
    FluoviewInfo *fvi = &par->fluoviewInfo;
    MM_HEAD *fvInfo;

    if (!par->ifds.tagPresentInFirstIFD(BIM_MMHEADER)) return 1;
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 1;


    uchar *buf = NULL;
    bim::uint64 size;
    bim::uint16 type;
    ifd->readTag(BIM_MMHEADER, size, type, &buf);
    if (!buf || size < sizeof(MM_HEAD)) return 1;
    if (TinyTiff::bigendian) doSwabMMHEAD((MM_HEAD *)buf);

    fvi->head = *(MM_HEAD *)buf;
    _TIFFfree(buf);
    fvInfo = &fvi->head;

    fvi->ch = 1;
    fvi->pages = 1;
    fvi->t_frames = 1;
    fvi->z_slices = 1;

    //---------------------------------------------------------------
    // retreive dimension parameters
    // Typical dimension names include "X", "Y", "Z", "T", "Ch", "Ani"
    // Fluoview order is: XYZTC -> we obtain XYCZT
    //---------------------------------------------------------------
    for (int d = 0; d < SPATIAL_DIMENSION; d++) {
        xstring name = fvInfo->DimInfo[d].Name;
        name = name.removeSpacesBoth().toLowerCase();

        if (name == "ch")
            fvi->ch = fvInfo->DimInfo[d].Size;
        else if (name == "t")
            fvi->t_frames = fvInfo->DimInfo[d].Size;
        else if (name == "z") {
            fvi->z_slices = fvInfo->DimInfo[d].Size;
            //fvi->zR = fvInfo->DimInfo[d].Resolution;
        } /* else if (name == "x")
            fvi->xR = fvInfo->DimInfo[d].Resolution;
        else if (name == "y")
            fvi->yR = fvInfo->DimInfo[d].Resolution;*/
    }

    par->info.number_pages = tiffGetNumberOfPages(par);
    fvi->pages_tiff = par->info.number_pages;
    fvi->pages = par->info.number_pages / fvi->ch;
    par->info.number_pages = fvi->pages;

    par->info.samples = (bim::uint32)fvi->ch;
    if (par->info.samples > 1)
        par->info.imageMode = bim::ImageModes::IM_MULTI;
    else
        par->info.imageMode = bim::ImageModes::IM_GRAYSCALE;

    bim::uint16 bitspersample = 1;
    TIFFGetField(par->tiff, TIFFTAG_BITSPERSAMPLE, &bitspersample);
    par->info.depth = bitspersample;


    //--------------------------------------------------------------------
    // get channel names
    //--------------------------------------------------------------------
    /*
    fvi->sample_names.resize(fvi->ch);
    for (int i = 0; i < fvi->ch; i++) {
        fvi->sample_names[i] = xstring::xprintf("Channel %d", i + 1);
    }

    if (ifd->tagPresent(270)) {
        fvi->meta.parse_ini(ifd->readTagString(270));

        // Extract channel names
        fvi->sample_names.resize(fvi->ch);
        for (int i = 0; i < fvi->ch; ++i) {
            fvi->sample_names[i] = fvi->meta.get_value(xstring::xprintf("Acquisition Parameters/Channel %d Dye", i + 1), xstring::xprintf("Channel %d", i + 1));
        }

        // ----------------------------------------------------
        // Get desired channel to display mapping
        // ----------------------------------------------------
        fvi->channel_mapping.resize(fvi->ch);
        for (unsigned int sample = 0; sample < (bim::uint)fvi->ch; ++sample) {
            bim::xstring lutkey = xstring::xprintf("LUT Ch%d/RGB 255", sample);
            if (fvi->meta.hasKey(lutkey)) {
                fvi->channel_mapping[sample] = bim::ColorF32::from_string_8bit(fvi->meta.get_value(lutkey), "\t");
            } else {
                fvi->channel_mapping[sample] = bim::ColorF32::default_color(sample, fvi->sample_names[sample], fvi->ch);
            }
        }
    }
    */

    //---------------------------------------------------------------
    // define dims
    //---------------------------------------------------------------

    info->number_z = fvi->z_slices;
    info->number_t = fvi->t_frames;

    if (fvi->z_slices > 1) {
        info->number_dims = 4;
        //info->dimensions[3].dim = DIM_Z;
    }

    if (fvi->t_frames > 1) {
        info->number_dims = 4;
        //info->dimensions[3].dim = DIM_T;
    }

    if ((fvi->z_slices > 1) && (fvi->t_frames > 1)) {
        info->number_dims = 5;
        //info->dimensions[3].dim = DIM_Z;
        //info->dimensions[4].dim = DIM_T;
    }

    return 0;
}

// in fluoview luts are saved as INI text in tag: 270 - Image Description
void fluoviewInitPalette(TiffParams *par, ImageInfo *info) {
    if (!info && !par) return;
    if (par->subType != tstFluoview) return;
    if (par->fluoviewInfo.ch > 1) return;

    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return;
    if (!ifd->tagPresent(270)) return;


    bim::uint64 buf_size;
    bim::uint16 buf_type;
    uchar *buf = NULL;
    ifd->readTag(270, buf_size, buf_type, &buf);
    change_0_to_n((char *)buf, (long)buf_size);

    info->lut.count = 0;
    for (int i = 0; i < 256; i++) info->lut.rgba[i] = i * 256;

    char *line = strstr((char *)buf, "[LUT Ch0]");
    if (line)
        for (int i = 0; i < 256; i++) {
            line = strstr(line, xstring::xprintf("RGB %d=", i).c_str());
            int r = i, g = i, b = i;
            if (line)
                sscanf(line, xstring::xprintf("RGB %d=%%d\t%%d\t%%d\n", i).c_str(), &r, &g, &b);
            info->lut.rgba[i] = xRGB(r, g, b);
        }
    info->lut.count = 256;

    _TIFFfree(buf);
}


void fluoviewGetCurrentPageInfo(TiffParams *par) {
    if (par == NULL) return;
    ImageInfo *info = &par->info;

    if (par->subType == tstFluoview || par->subType == tstAndor) {
        fluoviewInitPalette(par, info);
        //FluoviewInfo *fvi = &par->fluoviewInfo;
        //info->resUnits = bim::ResolutionUnits::RES_um;
        //info->xRes = fvi->xR;
        //info->yRes = fvi->yR;
    }
}

//----------------------------------------------------------------------------
// READ/WRITE FUNCTIONS
//----------------------------------------------------------------------------

bim::uint fluoviewReadPlane(FormatHandle *fmtHndl, TiffParams *par, size_t plane) {
    if (par == 0) return 1;
    if (par->tiff == 0) return 1;
    if (!(par->subType == tstFluoview || par->subType == tstAndor)) return 1;

    TIFF *tif = par->tiff;
    FluoviewInfo *fvi = &par->fluoviewInfo;
    ImageInfo *ii = &par->info;
    if (!tif) return 1;

    bim::uint16 bitspersample = 1;
    TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bitspersample);

    ii->samples = fvi->ch;
    //ii->imageMode = IM_GRAYSCALE;
    ii->depth = bitspersample;

    ImageBitmap *img = fmtHndl->image;
    if (allocImg(fmtHndl, ii, img) != 0) return 1;
    size_t lineSize = getLineSizeInBytes(img);

    //--------------------------------------------------------------------
    // read data
    //--------------------------------------------------------------------
    for (unsigned int sample = 0; sample < (unsigned int)fvi->ch; ++sample) {
        // switch to correct page in original TIFF
        bim::uint64 dirNum = fmtHndl->pageNumber + sample * (fvi->pages_tiff / fvi->ch);
        TIFFSetDirectory(tif, (tdir_t)dirNum);
        if (TIFFIsTiled(tif)) continue;

        uchar *p = (uchar *)img->bits[sample];
        for (unsigned int y = 0; y < img->i.height; y++) {
            xprogress(fmtHndl, y * (sample + 1), img->i.height * fvi->ch, "Reading FLUOVIEW");
            if (xtestAbort(fmtHndl) == 1) break;
            TIFFReadScanline(tif, p, y, 0);
            p += lineSize;
        } // for y
    } // for sample

    TIFFSetDirectory(tif, (uint32_t)fmtHndl->pageNumber);
    return 0;
}

//----------------------------------------------------------------------------
// Metadata hash
//----------------------------------------------------------------------------

void parse_metadata_fluoview(FormatHandle *fmtHndl, TagMap *hash, const TagMap &meta) {
    TiffParams *par = (TiffParams *)fmtHndl->internalParams;
    //ImageInfo *info = &par->info;
    FluoviewInfo *fvi = &par->fluoviewInfo;
    //MM_HEAD *fvInfo = &fvi->head;

    //----------------------------------------------------------------------------
    // Add all other tags as Custom tags
    //----------------------------------------------------------------------------
    hash->set_values(meta, "Fluoview/");
    hash->eraseKeysStartingWith("Fluoview/LUT Ch");

    //----------------------------------------------------------------------------
    // Parsing some additional specific tags
    //----------------------------------------------------------------------------

    // magnification
    if (hash->hasKey("Fluoview/Acquisition Parameters/Magnification")) {
        double mag = bim::objective_parse_magnification(hash->get_value("Fluoview/Acquisition Parameters/Magnification"));
        if (mag > 0) hash->set_value(bim::OBJECTIVE_MAGNIFICATION, mag);
    }

    // objective
    if (hash->hasKey("Fluoview/Acquisition Parameters/Objective Lens")) {
        //hash->set_value(bim::OBJECTIVE_DESCRIPTION, hash->get_value("Fluoview/Acquisition Parameters/Objective Lens"));
        bim::parse_objective_from_string(hash->get_value("Fluoview/Acquisition Parameters/Objective Lens"), hash);
    }

    //---------------------------------------
    //Date=02-17-2004
    //Time=11:54:50
    bim::DateTime dt = bim::DateTime::from_strings(hash->get_value("Fluoview/Acquisition Parameters/Date"), hash->get_value("Fluoview/Acquisition Parameters/Time"), "%m-%d-%Y", "%H:%M:%S");
    hash->set_value(bim::DOCUMENT_DATETIME, dt.to_string_iso8601());
}

void parse_metadata_andor(FormatHandle *fmtHndl, TagMap *hash, TagMap &meta) {
    TiffParams *par = (TiffParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->info;
    FluoviewInfo *fvi = &par->fluoviewInfo;
    MM_HEAD *fvInfo = &fvi->head;

    //----------------------------------------------------------------------------
    // Additional info from Tag 270
    //----------------------------------------------------------------------------
    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return;
    xstring tag_270 = ifd->readTagString(270);

    // now parse out of standard Andor blocks
    std::deque<std::string> ProtocolDescription = TagMap::iniGetBlock(tag_270, "Protocol Description");
    // simply add lines, needed parsing
    if (ProtocolDescription.size() > 0) {
        std::string pp;
        std::deque<std::string>::const_iterator itt = ProtocolDescription.begin();
        while (itt != ProtocolDescription.end()) {
            pp += *itt;
            pp += "\n";
            ++itt;
        }
        meta.set_value("Protocol Description", pp);
    }

    std::deque<std::string> RegionInfo = TagMap::iniGetBlock(tag_270, "Region Info (Fields)");
    // simply add lines, needed parsing
    if (RegionInfo.size() > 0) {
        std::string pp;
        std::deque<std::string>::const_iterator itt = RegionInfo.begin();
        while (itt != RegionInfo.end()) {
            pp += *itt;
            pp += "\n";
            ++itt;
        }
        meta.set_value("Region Info (Fields)", pp);
    }



    hash->set_values(meta, "Andor/");

    //----------------------------------------------------------------------------
    // Parsing some additional specific tags
    //----------------------------------------------------------------------------
    //bim::TagMap::const_iterator it;

    /*
      // objective
      it = hash->find("Fluoview/Acquisition Parameters/Objective Lens");
      if (it != hash->end() )
        hash->set_value( bim::OBJECTIVE_DESCRIPTION, it->second );

      // magnification
      it = hash->find("Fluoview/Acquisition Parameters/Magnification");
      if (it != hash->end() )
        hash->set_value( bim::OBJECTIVE_MAGNIFICATION, it->second );
    */

    //---------------------------------------
    //Date=06/08/2010
    //Time=10:10:36 AM

    bim::DateTime dt = bim::DateTime::from_strings(meta.get_value("Created/Date"), meta.get_value("Created/Time"), "%m/%d/%Y", "%I:%M:%S %p");
    hash->set_value(bim::DOCUMENT_DATETIME, dt.to_string_iso8601());

    //----------------------------------------------------------------------------
    // Parsing stage
    //----------------------------------------------------------------------------

    //parse XYFields
    std::deque<Eigen::Vector3d> xyFields;
    std::vector<xstring> XYFields = meta.get_value("XYZScan/XYFields").split("\t");
    if (XYFields.size() > 0 && XYFields[0].toInt() < static_cast<int>(XYFields.size())) {
        for (size_t i = 1; i < XYFields.size(); ++i) {
            std::vector<xstring> XYZ = XYFields[i].split(",");
            if (XYZ.size() < 3) continue;
            xyFields.push_back(Eigen::Vector3d(XYZ[0].toDouble(), XYZ[1].toDouble(), XYZ[2].toDouble()));
        }
    }

    //parse MontageOffsets
    std::deque<Eigen::Vector3d> montageOffsets;
    std::vector<xstring> MontageOffsets = meta.get_value("XYZScan/MontageOffsets").split("\t");
    if (MontageOffsets.size() > 0 && MontageOffsets[0].toInt() < static_cast<int>(MontageOffsets.size())) {
        for (size_t i = 1; i < MontageOffsets.size(); ++i) {
            std::vector<xstring> XYZ = MontageOffsets[i].split(",");
            if (XYZ.size() < 3) continue;
            montageOffsets.push_back(Eigen::Vector3d(XYZ[0].toDouble(), XYZ[1].toDouble(), XYZ[2].toDouble()));
        }
    }

    // write stage coordinates
    std::vector<bim::xstring> stage_positions;
    for (size_t i = 0; i < xyFields.size(); ++i) {
        for (size_t j = 0; j < montageOffsets.size(); ++j) {
            Eigen::Vector3d v = xyFields[i] + montageOffsets[j];
            //hash->set_value(xstring::xprintf(bim::STAGE_POSITION_TEMPLATE_X.c_str(), p), v[0]);
            //hash->set_value(xstring::xprintf(bim::STAGE_POSITION_TEMPLATE_Y.c_str(), p), v[1]);
            //hash->set_value(xstring::xprintf(bim::STAGE_POSITION_TEMPLATE_Z.c_str(), p), v[2]);
            stage_positions.push_back(xstring::xprintf("%f,%f,%f", v[0], v[1], v[2]));
        }
    }
    hash->set_value(bim::COORDINATES_POSITIONS_STAGE, xstring::join(stage_positions, ";"));
}

bim::uint append_metadata_fluoview(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;

    TiffParams *par = (TiffParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->info;
    FluoviewInfo *fvi = &par->fluoviewInfo;
    MM_HEAD *fvInfo = &fvi->head;

    hash->set_value(bim::IMAGE_NUM_Z, (bim::uint)fvi->z_slices);
    hash->set_value(bim::IMAGE_NUM_T, (bim::uint)fvi->t_frames);
    hash->set_value(bim::IMAGE_NUM_C, (bim::uint)fvi->ch);

    //----------------------------------------------------------------------------
    // DIMENSIONS
    //----------------------------------------------------------------------------

    for (size_t i = 0; i < SPATIAL_DIMENSION; i++) {
        xstring name = fvInfo->DimInfo[i].Name;
        name = name.removeSpacesBoth().toLowerCase();

        double res = fvInfo->DimInfo[i].Resolution;
        xstring units = fvInfo->DimInfo[i].Units;
        units = units.removeSpacesBoth();
        if (units == "um") units = bim::PIXEL_RESOLUTION_UNIT_MICRONS;
        if (units == "�m") units = bim::PIXEL_RESOLUTION_UNIT_MICRONS;
        if (units == "s") units = bim::PIXEL_RESOLUTION_UNIT_SECONDS;

        if (name == "x") {
            hash->set_value(bim::PIXEL_RESOLUTION_X, res);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, units);
        } else if (name == "y") {
            hash->set_value(bim::PIXEL_RESOLUTION_Y, res);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, units);
        } else if (name == "z") {
            hash->set_value(bim::PIXEL_RESOLUTION_Z, res);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, units);
        } else if (name == "t") {
            hash->set_value(bim::PIXEL_RESOLUTION_T, res);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_T, units);
        }
    }

    //----------------------------------------------------------------------------
    // Add all other tags as Custom tags
    //----------------------------------------------------------------------------

    TinyTiff::IFD *ifd = par->ifds.firstIfd();
    if (!ifd) return 0;
    TagMap meta;
    meta.parse_ini(ifd->readTagString(270), "=");
    
    //----------------------------------------------------------------------------
    // Channel names and preferred mapping
    //----------------------------------------------------------------------------

    for (unsigned int i = 0; i < fvi->ch; ++i) {
        xstring ch_path = xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);

        // channel name       
        xstring name = meta.get_value(xstring::xprintf("Acquisition Parameters/Channel %d Dye", i + 1), xstring::xprintf("Channel %d", i + 1));
        hash->set_value(ch_path + bim::CHANNEL_INFO_NAME, name);

        xstring k = xstring::xprintf("Acquisition Parameters/Channel %d", i + 1);
        if (meta.hasKey(k)) {
            hash->set_value(ch_path + bim::CHANNEL_INFO_MODALITY, meta.get_value(k));
        }

        // Get desired channel to display mapping
        bim::ColorF32 c = bim::ColorF32::default_color(i, name, fvi->ch);
        k = xstring::xprintf("LUT Ch%d/RGB 255", i);
        if (meta.hasKey(k)) {
            c = bim::ColorF32::from_string_8bit(meta.get_value(k), "\t");
        }
        hash->set_value(ch_path + bim::CHANNEL_INFO_COLOR, c.to_string_float());
    }

    //----------------------------------------------------------------------------
    // Additional info
    //----------------------------------------------------------------------------

    if (par->subType == tstFluoview)
        parse_metadata_fluoview(fmtHndl, hash, meta);
    else if (par->subType == tstAndor)
        parse_metadata_andor(fmtHndl, hash, meta);

    return 0;
}
