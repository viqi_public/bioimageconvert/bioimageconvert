/*****************************************************************************
  PerkinElmer QP definitions 
  Copyright (c) 2021-2022, ViQi Inc
 
  Author: Dima V. Fedorov <mailto:dima@viqi.org>
    
  History:
    2021-10-05 16:01 - First creation

  Ver : 1
*****************************************************************************/

#ifndef BIM_PERKINELMER_QP_FORMAT_H
#define BIM_PERKINELMER_QP_FORMAT_H

#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <xstring.h>
#include <tag_map.h>

//----------------------------------------------------------------------------
// PerkinElmer QP Structure
//----------------------------------------------------------------------------

namespace bim {

class PerkinElmerQPInfo {
public:
    PerkinElmerQPInfo() {}

    bool separated_channels = false;
    int num_sep_channels = 0;
    bool pix_resolution_in_tiff_tag = false;
    bim::TagMap meta;
    bim::TagMap meta_channels;
};

} // namespace bim

#endif // BIM_PERKINELMER_QP_FORMAT_H
