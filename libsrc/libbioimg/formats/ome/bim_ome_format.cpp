/*****************************************************************************
  OME XML file format (Open Microscopy Environment)
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    11/21/2005 15:43 - First creation
    2021-11-09 15:50 - cleaned-up

  ver : 2
*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <float.h>
#include <limits.h>
#include <math.h>

#include <xstring.h>

#include "bim_ome_format.h"

using namespace bim;

//****************************************************************************
// internal
//****************************************************************************

const char *bool_types[2] = { "false", "true" };

//----------------------------------------------------------------------------
// BASE 64 ENC/DEC
//----------------------------------------------------------------------------

// encoding LUT as described in RFC1113
static const char cb64[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

// decoding LUT, created by Bob Trower
static const char cd64[] = "|$$$}rstuvwxyz{$$$$$$$>?@ABCDEFGHIJKLMNOPQRSTUVW$$$$$$XYZ[\\]^_`abcdefghijklmnopq";

// encode 3 8-bit binary bytes as 4 '6-bit' characters
void b64_encodeblock(unsigned char in[3], unsigned char out[4], size_t len) {
    out[0] = cb64[in[0] >> 2];
    out[1] = cb64[((in[0] & 0x03) << 4) | ((in[1] & 0xf0) >> 4)];
    out[2] = (unsigned char)(len > 1 ? cb64[((in[1] & 0x0f) << 2) | ((in[2] & 0xc0) >> 6)] : '=');
    out[3] = (unsigned char)(len > 2 ? cb64[in[2] & 0x3f] : '=');
}

// decode 4 '6-bit' characters into 3 8-bit binary bytes
void b64_decodeblock(unsigned char in[4], unsigned char out[3]) {
    out[0] = (unsigned char)(in[0] << 2 | in[1] >> 4);
    out[1] = (unsigned char)(in[1] << 4 | in[2] >> 2);
    out[2] = (unsigned char)(((in[2] << 6) & 0xc0) | in[3]);
}


//----------------------------------------------------------------------------
// WRITE PROC
//----------------------------------------------------------------------------

const char *omePixelType(ImageBitmap *img) {
    std::string pt = "Uint8";
    if (img->i.depth == 16 && img->i.pixelType == bim::DataFormat::FMT_UNSIGNED) pt = "Uint16";
    if (img->i.depth == 32 && img->i.pixelType == bim::DataFormat::FMT_UNSIGNED) pt = "Uint32";
    if (img->i.depth == 8 && img->i.pixelType == bim::DataFormat::FMT_SIGNED) pt = "int8";
    if (img->i.depth == 16 && img->i.pixelType == bim::DataFormat::FMT_SIGNED) pt = "int16";
    if (img->i.depth == 32 && img->i.pixelType == bim::DataFormat::FMT_SIGNED) pt = "int32";
    if (img->i.depth == 32 && img->i.pixelType == bim::DataFormat::FMT_FLOAT) pt = "float";
    if (img->i.depth == 64 && img->i.pixelType == bim::DataFormat::FMT_FLOAT) pt = "double";
    return pt.c_str();
}

void omeWriteImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    OmeParams *omePar = (OmeParams *)fmtHndl->internalParams;
    if (fmtHndl->stream == NULL) return;
    ImageBitmap *img = fmtHndl->image;

    bim::xstring str;

    // write info

    //<Image Name = "P1W1S1" PixelSizeX = "0.2" PixelSizeY = "0.2" PixelSizeZ = "0.2">
    if (img->i.resUnits == bim::ResolutionUnits::RES_um) {
        str += xstring::xprintf("  <Image Name = \"%s\" PixelSizeX=\"%f\" PixelSizeY=\"%f\">\n", fmtHndl->fileName, img->i.xRes, img->i.yRes);
    } else {
        str += xstring::xprintf("  <Image Name = \"%s\" PixelSizeX=\"0.0\" PixelSizeY=\"0.0\">\n", fmtHndl->fileName);
    }
    str += "    <CreationDate>1111-11-11T11:11:11</CreationDate>\n";

    //<Pixels DimensionOrder = "XYCZT"
    //PixelType = "int16"
    //BigEndian = "true"
    //SizeX = "20"
    //SizeY = "20"
    //SizeZ = "5"
    //SizeC = "1"
    //SizeT = "6">
    str += "    <Pixels DimensionOrder = \"XYCZT\" ";

    str += xstring::xprintf("PixelType = \"%s\" ", omePixelType(img));
    str += xstring::xprintf("BigEndian = \"%s\" ", bool_types[bim::bigendian]);
    str += xstring::xprintf("SizeX = \"%d\" ", img->i.width);
    str += xstring::xprintf("SizeY = \"%d\" ", img->i.height);
    str += xstring::xprintf("SizeZ = \"%d\" ", img->i.number_z);
    str += xstring::xprintf("SizeC = \"%d\" ", img->i.samples);
    str += xstring::xprintf("SizeT = \"%d\">\n", img->i.number_t);

    xwrite(fmtHndl, (void *)str.c_str(), 1, str.size());
}


static int write_ome_image(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    OmeParams *omePar = (OmeParams *)fmtHndl->internalParams;

    if (fmtHndl->pageNumber == 0) omeWriteImageInfo(fmtHndl);
    ImageBitmap *img = fmtHndl->image;

    // write channels
    for (unsigned int sample = 0; sample < img->i.samples; ++sample) {
        // now write pixels
        std::string str = "      <Bin:BinData Compression=\"none\">";
        xwrite(fmtHndl, (void *)str.c_str(), 1, str.size());

        // now dump base64 bits
        uchar *p = (uchar *)img->bits[sample];
        size_t size_left = getImgSizeInBytes(img);
        uchar ascii_quatro[4];

        while (size_left > 0) {
            b64_encodeblock(p, ascii_quatro, size_left);
            xwrite(fmtHndl, (void *)ascii_quatro, 1, 4);
            p += 3;
            size_left -= 3;
        }

        str = "</Bin:BinData>\n";
        xwrite(fmtHndl, (void *)str.c_str(), 1, str.size());
    }

    return 0;
}



//****************************************************************************
// MISC
//****************************************************************************

OmeParams::OmeParams() {
}

OmeParams::~OmeParams() {
}

//****************************************************************************
// INTERNAL STRUCTURES
//****************************************************************************

void omeGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    OmeParams *omePar = (OmeParams *)fmtHndl->internalParams;
    ImageInfo *info = &omePar->i;
    //*info = ImageInfo();

    /*
      if (fmtHndl->stream == NULL) return;
      if (xseek(fmtHndl, 0, SEEK_SET) != 0) return;
      if ( xread( fmtHndl, &ibwPar->bh, 1, sizeof(BinHeader5) ) != sizeof(BinHeader5)) return;
    */


    // set image parameters
    info->width = 1;
    info->height = 1;
    info->samples = 1;
    info->number_pages = 1;
    info->imageMode = bim::ImageModes::IM_GRAYSCALE;

    // by now we'll normalize all data
    info->depth = 8;
    info->pixelType = bim::DataFormat::FMT_UNSIGNED;
}

void omeWriteOmeHeader(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    if (fmtHndl->stream == NULL) return;

    std::string str;

    // write header
    str += "<?xml version = \"1.0\" encoding = \"UTF-8\"?>\n";
    str += "\n";
    str += "<!--\n";
    str += "#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
    str += "# Generated by:\n";
    str += "#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
    str += "-->\n";
    str += "\n";
    str += "<OME xmlns = \"http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd\"\n";
    str += "     xmlns:STD = \"http://www.openmicroscopy.org/XMLschemas/STD/RC2/STD.xsd\"\n";
    str += "     xmlns:Bin = \"http://www.openmicroscopy.org/XMLschemas/BinaryFile/RC1/BinaryFile.xsd\"\n";
    str += "     xmlns:xsi = \"http://www.w3.org/2001/XMLSchema-instance\"\n";
    str += "     xsi:schemaLocation = \"http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd http://www.openmicroscopy.org/XMLschemas/OME/FC/ome.xsd http://www.openmicroscopy.org/XMLschemas/STD/RC2/STD.xsd http://www.openmicroscopy.org/XMLschemas/STD/RC2/STD.xsd\">\n";
    str += "\n";

    xwrite(fmtHndl, (void *)str.c_str(), 1, str.size());
}

void omeWriteOmeEnd(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    if (fmtHndl->stream == NULL) return;

    std::string str = "</OME>\n";
    xwrite(fmtHndl, (void *)str.c_str(), 1, str.size());
}

void omeWriteImageEnd(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    if (fmtHndl->stream == NULL) return;

    std::string str = "";
    str += "    </Pixels>\n";
    str += "  </Image>\n";

    xwrite(fmtHndl, (void *)str.c_str(), 1, str.size());
}


//----------------------------------------------------------------------------
// PARAMETERS, INITS
//----------------------------------------------------------------------------

int omeValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_OME_MAGIC_SIZE) return -1;
    //if (memcmp( magic, ibwMagicWin, BIM_IBW_MAGIC_SIZE ) == 0) return 0;
    //if (memcmp( magic, ibwMagicMac, BIM_IBW_MAGIC_SIZE ) == 0) return 0;
    return -1;
}

FormatHandle omeAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void omeCloseImageProc(FormatHandle *fmtHndl);
void omeReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    omeCloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------
void omeCloseImageProc(FormatHandle *fmtHndl) {
    if (!fmtHndl) return;

    if (fmtHndl->io_mode == bim::ImageIOModes::IO_WRITE) {
        omeWriteImageEnd(fmtHndl);
        omeWriteOmeEnd(fmtHndl);
    }

    xclose(fmtHndl);

    if (fmtHndl->internalParams) {
        OmeParams *omePar = (OmeParams *)fmtHndl->internalParams;
        delete omePar;
    }
    fmtHndl->internalParams = NULL;
}

bim::uint omeOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (!fmtHndl) return 1;
    if (io_mode == bim::ImageIOModes::IO_READ) return 1;

    if (fmtHndl->internalParams != NULL) omeCloseImageProc(fmtHndl);
    fmtHndl->internalParams = (void *)new bim::OmeParams();
    bim::OmeParams *par = (bim::OmeParams *)fmtHndl->internalParams;

    fmtHndl->io_mode = io_mode;
    xopen(fmtHndl);
    if (!fmtHndl->stream) {
        omeCloseImageProc(fmtHndl);
        return 1;
    };

    if (io_mode == bim::ImageIOModes::IO_READ) {
        return 1;
        omeGetImageInfo(fmtHndl);
    } else {
        omeWriteOmeHeader(fmtHndl);
    }
    return 0;
}

//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint omeGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    OmeParams *omePar = (OmeParams *)fmtHndl->internalParams;

    return (bim::uint)omePar->i.number_pages;
}


ImageInfo omeGetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    if (fmtHndl == NULL) return ImageInfo();
    OmeParams *omePar = (OmeParams *)fmtHndl->internalParams;
    return omePar->i;
}

//----------------------------------------------------------------------------
// READ/WRITE
//----------------------------------------------------------------------------

bim::uint omeReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;

    fmtHndl->pageNumber = page;
    //return read_ome_image( fmtHndl );
    return 0;
}

bim::uint omeWriteImageProc(FormatHandle *fmtHndl) {
    return write_ome_image(fmtHndl);
}



//****************************************************************************
//
// EXPORTED FUNCTION
//
//****************************************************************************

FormatItem omeItems[1] = {
    { "OME",                 // short name, no spaces
      "Open Microscopy XML", // Long format name
      "ome",                 // pipe "|" separated supported extension list
      0,                     //canRead;      // 0 - NO, 1 - YES
      1,                     //canWrite;     // 0 - NO, 1 - YES
      0,                     //canReadMeta;  // 0 - NO, 1 - YES
      1,                     //canWriteMeta; // 0 - NO, 1 - YES
      1,                     //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 1, 0, 0, 0, 1 } }
};

FormatHeader omeHeader = {

    sizeof(FormatHeader),
    "1.0.0",
    "OME CODEC",
    "OME CODEC",

    12,                 // 0 or more, specify number of bytes needed to identify the file
    { 1, 1, omeItems }, //dimJpegSupported,

    omeValidateFormatProc,
    // begin
    omeAquireFormatProc, //AquireFormatProc
    // end
    omeReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    omeOpenImageProc,  //OpenImageProc
    omeCloseImageProc, //CloseImageProc

    // info
    omeGetNumPagesProc,  //GetNumPagesProc
    omeGetImageInfoProc, //GetImageInfoProc


    // read/write
    omeReadImageProc,  //ReadImageProc
    omeWriteImageProc, //WriteImageProc
    NULL,              //ReadImageTileProc
    NULL,              //WriteImageTileProc
    NULL,              //ReadImageLineProc
    NULL,              //WriteImageLineProc
    NULL,              //ReadImageRegionProc
    NULL,              //WriteImageRegionProc
    NULL,              // AppendMetaDataProc
};

extern "C" {

FormatHeader *omeGetFormatHeader(void) {
    return &omeHeader;
}

} // extern C
