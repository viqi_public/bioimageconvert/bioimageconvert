/*****************************************************************************
  Nikon ND2 support
  Copyright (c) 2019, ViQI Inc

  Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
  2017-10-25 11:50:40 - First creation

  ver : 1
  *****************************************************************************/

#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <iostream>
#include <limits>
#include <map>
#include <memory>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <bim_format_misc.h>
#include <bim_lcms_parse.h>
#include <bim_metatags.h>
#include <tag_map.h>
#include <xcolor.h>
#include <xconf.h>
#include <xstring.h>
#include <xtypes.h>
#include <xunits.h>
#include <xdatetime.h>

#include "bim_nd2_format.h"

#include <pugixml.hpp>
#include <zlib.h>

using namespace bim;

//****************************************************************************
// bim::ND2Params
//****************************************************************************

bim::ND2Params::ND2Params() {

}

bim::ND2Params::~ND2Params() {
    if (this->fmtHndl->stream) {
        xopen(this->fmtHndl);
        this->fmtHndl->stream = NULL;
    }
}

void bim::ND2Params::open(FormatHandle *fmtHndl, bim::ImageIOModes io_mode) {
    this->fmtHndl = fmtHndl;
    this->fmtHndl->io_mode = io_mode;
    xopen(this->fmtHndl);
}

bool bim::ND2Params::check_version() {
    xseek(fmtHndl, 0, SEEK_SET);
    ND2_CHUNK_METADATA metadata = { 0 };
    bim::xread(fmtHndl, &metadata, 1, 16);
    if (memcmp(&metadata.magic, nd2_magic_number, BIM_FORMAT_ND2_MAGIC_SIZE) != 0) return false;

    xstring header(38 + 1, 0);
    bim::xread(fmtHndl, &header[0], 1, 38);
    if (!header.startsWith("ND2 FILE SIGNATURE CHUNK NAME01!Ver")) return false;
    header = header.replace("ND2 FILE SIGNATURE CHUNK NAME01!Ver", "");
    this->version_str = header;
    std::vector<xstring> version = header.split(".");
    if (version.size() < 2) return false;
    this->version_major = version[0].toInt();
    this->version_minor = version[1].toInt();
    if (this->version_major > this->supported_version) return false;
    return true;
}

void bim::ND2Params::read_label(const bim::uint64 &offset, ND2_LABEL &label) {
    ND2_CHUNK_METADATA metadata;
    bim::xseek(fmtHndl, offset, SEEK_SET);
    bim::xread(fmtHndl, &metadata, 1, sizeof(ND2_CHUNK_METADATA));
    if (memcmp(&metadata.magic, nd2_magic_number, BIM_FORMAT_ND2_MAGIC_SIZE) != 0) return;

    bim::uint64 data_offset = offset + 16 + metadata.relative_offset;
    bim::uint64 data_length = metadata.data_length;
    int label_length = std::min<int>(32, metadata.relative_offset);
    label.data_offset = (bim::uint32) data_offset;
    label.data_length = data_length;
}

void bim::ND2Params::read_label_data(const ND2_LABEL &label, std::vector<bim::uint8> &buffer) {
    if (label.data_length == 0) return;
    buffer.resize(label.data_length);
    xseek(fmtHndl, label.data_offset, SEEK_SET);
    bim::xread(fmtHndl, &buffer[0], 1, label.data_length);
}

void bim::ND2Params::parse_filemap() {
    xseek(fmtHndl, -1 * sizeof(ND2_CHUNKMAP_END), SEEK_END);
    ND2_CHUNKMAP_END ending = { 0 };
    bim::xread(fmtHndl, &ending, 1, sizeof(ND2_CHUNKMAP_END));
    if (memcmp(ending.header, nd2_signature_chunkmap, 32) != 0) return;

    ND2_LABEL label = { 0 };
    this->read_label(ending.offset, label);
    std::vector<bim::uint8> buffer;
    this->read_label_data(label, buffer);

    size_t pos = 0;
    while (pos < buffer.size()) {
        std::string name;
        char v = 'a';
        while (pos < buffer.size()) {
            v = *(char *)&buffer[pos];
            pos += 1;
            name += v;
            if (v == '!') break;
        }
        if (name == nd2_signature_chunkmap) break;
        ND2_FILEMAP_ENTRY *entry = (ND2_FILEMAP_ENTRY *)&buffer[pos];
        pos += 16;
        if (entry->offset2 == 0) break;
        this->label_offsets.set_value(name, (unsigned int)entry->offset);
    }
}

void bim::ND2Params::parse_metadata_value(std::vector<bim::uint8> &buffer, xstring path, size_t &pos, int data_type) {
    if (data_type == bim::ND2Params::dtUInt8) {
        if (pos + 1 >= buffer.size()) return;
        bim::uint8 v = *(bim::uint8 *)&buffer[pos];
        this->attributes.set_value(path, v);
        pos += 1;
    } else if (data_type == bim::ND2Params::dtInt32) {
        if (pos + 4 >= buffer.size()) return;
        bim::int32 v = *(bim::int32 *)&buffer[pos];
        this->attributes.set_value(path, v);
        pos += 4;
    } else if (data_type == bim::ND2Params::dtUInt32) {
        if (pos + 4 >= buffer.size()) return;
        bim::uint32 v = *(bim::uint32 *)&buffer[pos];
        this->attributes.set_value(path, v);
        pos += 4;
    } else if (data_type == bim::ND2Params::dtInt64) {
        if (pos + 8 >= buffer.size()) return;
        bim::int64 v = *(bim::int64 *)&buffer[pos];
        this->attributes.set_value(path, (int)v);
        pos += 8;
    } else if (data_type == bim::ND2Params::dtUInt64) {
        if (pos + 8 >= buffer.size()) return;
        bim::uint64 v = *(bim::uint64 *)&buffer[pos];
        this->attributes.set_value(path, (int)v);
        pos += 8;
    } else if (data_type == bim::ND2Params::dtFloat64) {
        if (pos + 8 >= buffer.size()) return;
        bim::float64 v = *(bim::float64 *)&buffer[pos];
        this->attributes.set_value(path, v);
        pos += 8;
    } else if (data_type == bim::ND2Params::dtPointer) {
        if (pos + 8 >= buffer.size()) return;
        bim::uint64 v = *(bim::uint64 *)&buffer[pos];
        this->attributes.set_value(path, (int)v);
        pos += 8;
    } else if (data_type == bim::ND2Params::dtString) {
        std::string str;
        char v = 'a';
        while (v != 0 && pos < buffer.size()) {
            v = *(char *)&buffer[pos];
            pos += 2;
            str += v;
        }
        if (str.size() > 0 && str[0] != 0) {
            this->attributes.set_value(path, str);
        }
    } else if (data_type == bim::ND2Params::dtArray) {
        bim::uint64 size = *(bim::uint64 *)&buffer[pos];
        pos += 8;
        if (size == 0 || pos + size >= buffer.size()) return;
        std::vector<char> v(size);
        memcpy(&v[0], &buffer[pos], size);
        this->attributes.set_value(path, v);
        pos += size;
    } else if (data_type == 10) {
        return; // deprecated
    } else if (data_type == 11) {
        ND2_METADATA_ITEM *item = (ND2_METADATA_ITEM *)&buffer[pos];
        pos += 12;
        if (pos >= buffer.size()) return;
        this->parse_image_metadata(buffer, path, pos, item->count);
        pos += item->count * 8;
    }
}

void bim::ND2Params::parse_image_metadata(std::vector<bim::uint8> &buffer, xstring path, size_t &pos, size_t max_count) {
    size_t count = 0;
    ND2_METADATA_ENTRY initial = { 1 };
    initial.data_type = 1;
    ND2_METADATA_ENTRY *entry = &initial;
    while (entry->data_type > 0 && count < max_count) {
        if (pos >= buffer.size()) return;
        entry = (ND2_METADATA_ENTRY *)&buffer[pos];
        pos += 2;
        if (entry->data_type < 1 && entry->data_type > 11) break;
        xstring name = path;
        if (entry->name_length > 0) {
            name = xstring::FromFixed16((const char *)&buffer[pos], entry->name_length * 2);
            pos += entry->name_length * 2;
            name = path.size() > 0 ? path + "/" + name : name;
        }
        this->parse_metadata_value(buffer, name, pos, entry->data_type);
        ++count;
    }
}

void bim::ND2Params::parse_metadata_hierarchical(const xstring &name, const xstring &path) {
    bim::uint64 offset = this->label_offsets.get_value_unsigned(name, 0);
    if (offset > 0) {
        ND2_LABEL label = { 0 };
        this->read_label(offset, label);
        std::vector<bim::uint8> buffer;
        this->read_label_data(label, buffer);
        size_t pos = 0;
        this->parse_image_metadata(buffer, path, pos);
    }
}


void nd2_xml_walker(pugi::xml_node node, bim::xstring path, TagMap *hash) {
    bim::xstring tag = node.name();
    if (tag.size() < 1) return;
    bool ignore_tag = tag == "no_name" || tag == "variant";
    if (!ignore_tag) {
        if (path.size() > 0)
            path += "/" + tag;
        else
            path += tag;
        bim::xstring value = node.attribute("value").value();
        if (value.size() > 0) {
            hash->set_value(path, value);
        }
    }

    // iterate over children
    for (pugi::xml_node child = node.first_child(); child; child = child.next_sibling()) {
        nd2_xml_walker(child, path, hash);
    }
}

void bim::ND2Params::parse_metadata_xml(const xstring &name, const xstring &path) {
    bim::uint64 offset = this->label_offsets.get_value_unsigned(name, 0);
    if (offset > 0) {
        ND2_LABEL label;
        this->read_label(offset, label);
        std::vector<bim::uint8> buffer;
        this->read_label_data(label, buffer);

        pugi::xml_document doc;
        if (doc.load_buffer(&buffer[0], buffer.size())) {
            try {
                pugi::xml_node variant = doc.select_node("variant").node();
                nd2_xml_walker(variant, path, &this->attributes);
            } catch (...) {
                // do nothing
            }
        }
    }
}

void bim::ND2Params::parse_metadata_ndcontrol_v1() {
    bim::uint64 offset = this->label_offsets.get_value_unsigned("CustomDataVar|NDControlV1_0!", 0);
    if (offset > 0) {
        ND2_LABEL label;
        this->read_label(offset, label);
        std::vector<bim::uint8> buffer;
        this->read_label_data(label, buffer);

        pugi::xml_document doc;
        if (doc.load_buffer(&buffer[0], buffer.size())) {
            try {
                pugi::xpath_node_set nodes = doc.select_nodes("variant/NDControl/LoopSize/no_name");
                for (pugi::xpath_node_set::const_iterator it = nodes.begin(); it != nodes.end(); ++it) {
                    pugi::xml_node node = it->node();
                    this->number_dims.push_back(node.attribute("value").as_int());
                }
            } catch (...) {
                // do nothing
            }
        }
    }
}

void bim::ND2Params::read_image_metadata() {
    if (this->version_major == 2) {
        this->parse_metadata_xml("ImageAttributes!", "SLxImageAttributes"); // v2
    } else {
        this->parse_metadata_hierarchical("ImageAttributesLV!", ""); // v3
    }
    this->parse_metadata_ndcontrol_v1();

    // read image labels
    size_t i = 0;
    while (this->label_offsets.hasKey(xstring::xprintf("ImageDataSeq|%d!", i))) {
        bim::uint64 offset = this->label_offsets.get_value_unsigned(xstring::xprintf("ImageDataSeq|%d!", i), 0);
        ND2_LABEL label = { 0 };
        this->read_label(offset, label);
        this->ImageDataSeq.push_back(label);
        ++i;
    }
}

void nd2_parse_info_string(bim::ND2Params *par, const xstring &name, const xstring &path_initial = "") {
    xstring s = par->attributes.get_value(name);
    std::vector<xstring> path;
    if (path_initial.size() > 0) {
        path.push_back(path_initial);
    }
    size_t offset = 0;
    for (const xstring line : s.split("\r\n")) {
        xstring l = line.lstrip(" ");
        if (!l.contains(":")) continue;
        if (l.startsWith("- "))
            l = l.replace("- ", " ");
        size_t current_offset = line.size() - l.size();
        if (current_offset < offset && path.size() > 0) {
            path.pop_back();
            offset = current_offset;
        }
        std::vector<xstring> v = l.strip(" ").split(":");
        xstring name = v[0].strip(" ");
        if (v.size() < 2) {
            path.push_back(name);
            offset = current_offset;
            continue;
        }
        offset = current_offset;
        xstring value = v[1].strip(" ");
        par->attributes.set_value(xstring::join(path, "/") + "/" + name, value);
    }
}

void nd2_parse_info_string_v2(bim::ND2Params *par, const xstring &name, const xstring &path_initial = "") {
    xstring s = par->attributes.get_value(name);
    std::vector<xstring> path;
    xstring previous_name;
    if (path_initial.size() > 0) {
        path.push_back(path_initial);
    }
    size_t offset = 0;
    unsigned plane = 0;
    for (const xstring line : s.split("\r\n")) {
        xstring l = line.lstrip(" ");
        if (!l.contains(":")) continue;
        if (l.startsWith("- "))
            l = l.replace("- ", " ");
        size_t current_offset = line.size() - l.size();
        while (current_offset < offset && path.size() > 0) {
            path.pop_back();
            --offset;
        }
        std::vector<xstring> v = l.strip(" ").split(":");
        xstring name = v[0].strip(" ");
        if (current_offset > offset) {
            if (current_offset == 1 && name == "Name") {
                ++plane;
                path.push_back(bim::xstring::xprintf("Plane #%d", plane));
            } else {
                path.push_back(previous_name);
            }
            offset = current_offset;
        }
        previous_name = name;
        if (v.size() < 2) continue;
        xstring value = v[1].strip(" ");
        par->attributes.set_value(xstring::join(path, "/") + "/" + name, value);
    }
}

/*
CustomData|Z1!
CustomData|Z!
CustomData|Y! - bin - - 17166336
CustomData|X! - bin - - 17162240
CustomData|TimeSourceCache! - - -
CustomData|RoiRleGlobal_v1! - bin - - 17096704
CustomData|Camera_ExposureTime1! - bin - - 17158144
CustomData|AcqTimesCache!
CustomData|AcqTimes2Cache!
CustomData|AcqFramesCache!

CustomDataVar|StreamDataV1_0! - XML - - 17195008
CustomDataVar|OpenViewV1_0! - XML with bin - -
CustomDataVar|LUTDataV1_0! - XML - - 17113088
*/

void bim::ND2Params::read_all_metadata() {
    if (this->version_major == 2 && this->version_minor == 0) {
        this->parse_metadata_xml("ImageTextInfo!", "RLxImageTextInfo");
        this->parse_metadata_xml("ImageMetadata!", "SLxExperiment");
        this->parse_metadata_xml("ImageMetadataSeq|0!", "SLxPictureMetadata");
        this->parse_metadata_xml("ImageCalibration|0!", "SLxCalibration");
        this->parse_metadata_xml("ImageEvents!", "SLxExperimentRecord");

        // parse specific fields and create our metadata
        nd2_parse_info_string_v2(this, "RLxImageTextInfo/TextInfoItem_5", "Metadata"); // dima: needs different processing
    } else if (this->version_major == 2 && this->version_minor > 0) {
        this->parse_metadata_xml("ImageTextInfo!", "SLxImageTextInfo");
        this->parse_metadata_xml("ImageMetadata!", "SLxExperiment");
        this->parse_metadata_xml("ImageMetadataSeq|0!", "SLxPictureMetadata");
        this->parse_metadata_xml("ImageCalibration|0!", "SLxCalibration");
        this->parse_metadata_xml("ImageEvents!", "SLxExperimentRecord");

        // parse specific fields and create our metadata
        nd2_parse_info_string(this, "SLxImageTextInfo/TextInfoItem_5");
        nd2_parse_info_string(this, "SLxImageTextInfo/TextInfoItem_6", "Capture");
    } else { // v3
        this->parse_metadata_hierarchical("ImageTextInfoLV!", "");
        this->parse_metadata_hierarchical("ImageMetadataLV!", "");
        this->parse_metadata_hierarchical("ImageMetadataSeqLV|0!", "");
        this->parse_metadata_hierarchical("ImageCalibrationLV|0!", "");
        this->parse_metadata_hierarchical("ImageEventsLV!", "");

        // parse specific fields and create our metadata
        nd2_parse_info_string(this, "SLxImageTextInfo/TextInfoItem_5");
        nd2_parse_info_string(this, "SLxImageTextInfo/TextInfoItem_6", "Capture");
    }

    //this->parse_metadata_xml("CustomDataVar|StreamDataV1_0!", "");
    //this->parse_metadata_xml("CustomDataVar|OpenViewV1_0!", "");
    //this->parse_metadata_xml("CustomDataVar|LUTDataV1_0!", "");

    this->parse_metadata_xml("CustomDataVar|GrabberCameraSettingsV1_0!", "");
    this->parse_metadata_xml("CustomDataVar|CustomDataV2_0!", "");
    this->parse_metadata_xml("CustomDataVar|AppInfo_V1_0!", "AppInfo");
    this->parse_metadata_xml("CustomDataVar|AcqTimeV1_0!", "");
}

int nd2_uncompress_zlib(Bytef *dest, uLongf *destLen, const Bytef *source, uLong sourceLen) {
    z_stream stream;
    int err;

    stream.next_in = (z_const Bytef *)source;
    stream.avail_in = (uInt)sourceLen;
    // Check for source > 64K on 16-bit machine:
    if ((uLong)stream.avail_in != sourceLen) return Z_BUF_ERROR;

    stream.next_out = dest;
    stream.avail_out = (uInt)*destLen;
    if ((uLong)stream.avail_out != *destLen) return Z_BUF_ERROR;

    stream.zalloc = (alloc_func)0;
    stream.zfree = (free_func)0;

    err = inflateInit(&stream);
    if (err != Z_OK) return err;

    err = inflateReset2(&stream, 31);
    if (err != Z_OK) {
        inflateEnd(&stream);
        return err;
    }

    err = inflate(&stream, Z_FINISH);
    if (err != Z_STREAM_END) {
        inflateEnd(&stream);
        if (err == Z_NEED_DICT || (err == Z_BUF_ERROR && stream.avail_in == 0))
            return Z_DATA_ERROR;
        return err;
    }
    *destLen = stream.total_out;

    err = inflateEnd(&stream);
    return err;
}

void bim::ND2Params::read_image(const bim::uint64 &idx, ImageInfo *info, std::vector<bim::uint8> &buffer) {
    std::vector<bim::uint8> temp;
    this->read_label_data(this->ImageDataSeq[idx], temp);

    // frame buffer has 8 bytes offset for timestamp
    size_t required_buffer_size = info->width * info->height * info->samples * (info->depth / 8);

    // check for compression, 2 means RAW data, 1 seems to mean ZIP compression
    if (this->compression == 2) {
        if (temp.size() - 8 < required_buffer_size) return;
        buffer.resize(temp.size() - 8); // use original buffer size because of padding and larger stride
        memcpy(&buffer[0], &temp[8], buffer.size());
    } else if (this->compression == 1) {
        buffer.resize(required_buffer_size);
        uLongf outsz = (uLongf)required_buffer_size;
        if (nd2_uncompress_zlib(&buffer[0], &outsz, &temp[8], static_cast<uLong>(temp.size() - 8)) != Z_OK) {
            buffer.resize(0);
            return;
        }
    }
}


//****************************************************************************
// required funcs
//****************************************************************************

int nd2ValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < BIM_FORMAT_ND2_MAGIC_SIZE) return -1;
    unsigned char *mag_num = (unsigned char *)magic;
    if (memcmp(mag_num, nd2_magic_number, BIM_FORMAT_ND2_MAGIC_SIZE) == 0) return 0;
    return -1;
}

FormatHandle nd2AquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void nd2CloseImageProc(FormatHandle *fmtHndl);
void nd2ReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    nd2CloseImageProc(fmtHndl);
}


//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------

void nd2GetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    //*info = ImageInfo();
    XConf *conf = fmtHndl->arguments;

    par->parse_filemap();
    par->read_image_metadata();

    info->width = par->attributes.get_value_int("SLxImageAttributes/uiWidth", 0);
    info->height = par->attributes.get_value_int("SLxImageAttributes/uiHeight", 0);
    info->depth = par->attributes.get_value_int("SLxImageAttributes/uiBpcInMemory", 0);
    info->samples = par->attributes.get_value_int("SLxImageAttributes/uiComp", 1);
    info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    int pixel_type = par->attributes.get_value_int("SLxImageAttributes/ePixelType", 0);
    if (pixel_type == 2) // DIMA ?????
        info->pixelType = bim::DataFormat::FMT_FLOAT;

    par->line_stride_bytes = static_cast<int>(info->width * info->samples * (info->depth / 8));
    par->line_stride_bytes = par->attributes.get_value_int("SLxImageAttributes/uiWidthBytes", par->line_stride_bytes);
    par->sequence_count = par->attributes.get_value_int("SLxImageAttributes/uiSequenceCount", 1);
    par->compressionParam = par->attributes.get_value_int("SLxImageAttributes/dCompressionParam", 0);
    par->compression = par->attributes.get_value_int("SLxImageAttributes/eCompression", 0);
    par->bpcSignificant = par->attributes.get_value_int("SLxImageAttributes/uiBpcSignificant", 0);
    par->tileHeight = par->attributes.get_value_int("SLxImageAttributes/uiTileHeight", 0);
    par->tileWidth = par->attributes.get_value_int("SLxImageAttributes/uiTileWidth", 0);
    par->virtualComponents = par->attributes.get_value_int("SLxImageAttributes/uiVirtualComponents", 0);

    info->imageMode = bim::ImageModes::IM_MULTI;
    if (par->number_dims.size() > 3) {
        // fix malformed case when t indicates the number of sequences in the image
        if (par->number_dims[0] > 0 && static_cast<int>(par->number_dims[0]) == par->sequence_count &&
            par->number_dims[1] == 0 && par->number_dims[2] == 0 && par->number_dims[3] == 0) {
            par->number_dims[0] = 1;
            par->number_dims[1] = par->sequence_count;
        }

        info->number_t = std::max<unsigned int>(par->number_dims[0], 1);
        info->number_z = std::max<unsigned int>(par->number_dims[2], 1);
        info->number_pages = par->ImageDataSeq.size();
        info->number_dims = 5;
    } else {
        info->number_z = 1;
        info->number_t = 1;
        info->number_pages = std::max(par->sequence_count, 1);
        info->number_dims = 2;
        par->number_dims.resize(4, 1);
        par->number_dims[1] = par->sequence_count;
    }
}

void nd2CloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    if (fmtHndl->internalParams == NULL) return;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint nd2OpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) nd2CloseImageProc(fmtHndl);
    bim::ND2Params *par = new bim::ND2Params();
    fmtHndl->internalParams = (void *)par;

    if (io_mode == bim::ImageIOModes::IO_WRITE) {
        return 1;
    }

    try {
        par->open(fmtHndl, io_mode);
        if (par->check_version() != true) return 1;
        nd2GetImageInfo(fmtHndl);
    } catch (...) {
        nd2CloseImageProc(fmtHndl);
        return 1;
    }

    return 0;
}


//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint nd2GetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    return static_cast<bim::uint>(info->number_pages);
}

ImageInfo nd2GetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    if (fmtHndl == NULL) return ImageInfo();
    fmtHndl->pageNumber = page_num;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    XConf *conf = fmtHndl->arguments;
    // no need to re-read the image since all series will be the same size
    return par->i;
}


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

bim::uint nd2_append_metadata(FormatHandle *fmtHndl, TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (!hash) return 1;
    if (isCustomReading(fmtHndl)) return 1;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    par->read_all_metadata();

    hash->set_values(par->attributes, "Nikon/");
    hash->delete_tag("Nikon/SLxImageTextInfo/TextInfoItem_5");
    hash->delete_tag("Nikon/SLxImageTextInfo/TextInfoItem_6");
    hash->delete_tag("Nikon/RLxImageTextInfo/TextInfoItem_5");
    hash->delete_tag("Nikon/RLxImageTextInfo/TextInfoItem_6");

    // parse specific fields
    xstring path;

    // add date time
    hash->set_value(bim::DOCUMENT_VERSION, par->version_str);

    //hash->set_value_from_old_key("Nikon/RLxImageTextInfo/TextInfoItem_9", path + bim::DOCUMENT_DATETIME); //v2
    //hash->set_value_from_old_key("Nikon/SLxImageTextInfo/TextInfoItem_9", path + bim::DOCUMENT_DATETIME); //v3
    xstring date = hash->get_value("Nikon/SLxImageTextInfo/TextInfoItem_9"); //v3
    if (date.size() == 0)
        date = hash->get_value("Nikon/RLxImageTextInfo/TextInfoItem_9"); //v2
    if (date.size() > 0) //'06/06/2017  11:15:06'
        hash->set_value(bim::DOCUMENT_DATETIME, bim::DateTime::from_string(date, "%d/%m/%Y  %H:%M:%S").to_string_iso8601());

    hash->delete_tag("Nikon/RLxImageTextInfo/TextInfoItem_9");
    hash->delete_tag("Nikon/SLxImageTextInfo/TextInfoItem_9");

    // delete old objective tag
    hash->delete_tag("Nikon/RLxImageTextInfo/TextInfoItem_13");
    hash->delete_tag("Nikon/SLxImageTextInfo/TextInfoItem_13");

    // dimensions
    if (par->number_dims.size() > 3) {
        hash->set_value(bim::IMAGE_DIMENSIONS, "XYZT");
        hash->set_value(bim::IMAGE_NUM_T, std::max<unsigned int>(par->number_dims[0], 1));
        hash->set_value(bim::IMAGE_NUM_Z, std::max<unsigned int>(par->number_dims[2], 1));
        if (par->number_dims[1] > 0)
            //hash->set_value(bim::IMAGE_NUM_FOV, par->number_dims[1]);
            hash->set_value(bim::IMAGE_NUM_SERIES, par->number_dims[1]);

        double rz = hash->get_value_double("Nikon/SLxExperiment/ppNextLevelEx/ppNextLevelEx/uLoopPars/dZStep", 0.0);
        if (rz > 0) {
            hash->set_value(bim::PIXEL_RESOLUTION_Z, rz);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
        }

        //Nikon/Metadata/Step: 5 um
        xstring rzs = hash->get_value("Nikon/Metadata/Step");
        std::vector<xstring> rzsv = rzs.split(" ");
        if (rzsv.size() > 0) {
            hash->set_value(bim::PIXEL_RESOLUTION_Z, rzsv[0].toDouble(0));
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
        }

        double rt = hash->get_value_double("Nikon/SLxExperiment/uLoopPars/pPeriod/_00/dPeriod", 0.0);
        if (rt > 0) {
            hash->set_value(bim::PIXEL_RESOLUTION_T, rt / 1000.0);
            hash->set_value(bim::PIXEL_RESOLUTION_UNIT_T, bim::PIXEL_RESOLUTION_UNIT_SECONDS);
        }
    }

    // resolution
    double rx = hash->get_value_double("Nikon/SLxCalibration/dCalibration", 0);
    if (rx > 0) {
        hash->set_value(bim::PIXEL_RESOLUTION_X, rx);
        hash->set_value(bim::PIXEL_RESOLUTION_Y, rx);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
        hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    }

    // objective
    path = bim::xstring::xprintf(bim::OBJECTIVE_INFO_TEMPLATE.c_str(), 0);

    hash->set_value_from_old_key("Nikon/SLxCalibration/sObjective", path + bim::OBJECTIVE_NAME);
    hash->set_value_from_old_key("Nikon/Metadata/Numerical Aperture", path + bim::OBJECTIVE_NUMERICAL_APERTURE);
    hash->set_value_from_old_key("Nikon/Metadata/Refractive Index", path + bim::OBJECTIVE_REFRACTIVE_INDEX);

    xstring obj = hash->get_value("Nikon/SLxCalibration/sObjective");
    bim::parse_objective_from_string(obj, hash);
    hash->set_value_from_old_key(bim::OBJECTIVE_MAGNIFICATION, path + bim::OBJECTIVE_MAGNIFICATION_X);

    // channels
    for (uint64 i = 0; i < info->samples; ++i) {
        xstring path_in = bim::xstring::xprintf("Nikon/Metadata/Plane #%d/", i + 1);
        path = bim::xstring::xprintf(bim::CHANNEL_INFO_TEMPLATE.c_str(), i);
        xstring path_channel = bim::xstring::xprintf("Nikon/SLxPictureMetadata/sPicturePlanes/sPlaneNew/a%d", i);
        if (par->version_major == 2)
            path_channel = bim::xstring::xprintf("Nikon/SLxPictureMetadata/sPicturePlanes/sPlane/a%d", i);

        xstring ch_modality = hash->get_value(path_in + "Modality");
        xstring name = hash->get_value(path_in + "Name");
        if (name.size() == 0)
            name = hash->get_value(path_channel + "/sDescription");
        if (name.size() == 0)
            name = hash->get_value(path_channel + "/sOpticalConfigName");
        if (name.size() == 0)
            name = hash->get_value(path_channel + "/pFilterPath/m_pFilter/m_sName");

        hash->set_value(path + bim::CHANNEL_INFO_NAME, name);
        //hash->set_value(bim::xstring::xprintf(bim::CHANNEL_NAME_TEMPLATE.c_str(), i), name); // old format

        hash->set_value_from_old_key(path_in + "Modality", path + bim::CHANNEL_INFO_MODALITY);
        if (ch_modality.toLowerCase().contains("brightfield")) {
            hash->set_value(path + bim::CHANNEL_INFO_COLOR, "1.0,1.0,1.0");
            //hash->set_value(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i), "255,255,255");
        } else {
            hash->set_value(path + bim::CHANNEL_INFO_FLUOR, name);
        }
        hash->set_value_from_old_key(path_in + "Microscope Settings/Zoom", path + bim::CHANNEL_INFO_ZOOM); // v2
        hash->set_value_from_old_key(path_in + "Zoom", path + bim::CHANNEL_INFO_ZOOM);                     //v3
        hash->set_value_from_old_key(path_in + "Camera Settings/Binning", path + bim::CHANNEL_INFO_BINNING);
        hash->set_value_from_old_key(path_in + "Camera Settings/Gain", path + bim::CHANNEL_INFO_DETECTOR_GAIN);

        std::vector<xstring> exposure = hash->get_value(path_in + "Camera Settings/Exposure", "").split(" ");
        if (exposure.size() == 2) {
            hash->set_value(path + bim::CHANNEL_INFO_EXPOSURE, exposure[0]);
            hash->set_value(path + bim::CHANNEL_INFO_EXPOSURE_UNITS, exposure[1]);
        }
        if (obj.size() > 0) {
            hash->set_value(path + bim::CHANNEL_INFO_OBJECTIVE, obj);
        }

        // parse per-plane metadata

        // wavelenghts
        if (!ch_modality.toLowerCase().contains("brightfield")) {
            //Nikon/SLxPictureMetadata/sPicturePlanes/sPlaneNew/a1/pFilterPath/m_pFilter/m_ExcitationSpectrum/pPoint/Point0/dWavelength: 0
            double ex_p1 = hash->get_value_double(path_channel + "/pFilterPath/m_pFilter/m_ExcitationSpectrum/pPoint/Point0/dWavelength", 0.0);
            double ex_p2 = hash->get_value_double(path_channel + "/pFilterPath/m_pFilter/m_ExcitationSpectrum/pPoint/Point1/dWavelength", 0.0);
            if (ex_p1 > 0 && ex_p2 > 0) {
                hash->set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, xstring::xprintf("%.0f-%.0f", ex_p1, ex_p2));
            } else if (ex_p1 > 0) {
                hash->set_value(path + bim::CHANNEL_INFO_EX_WAVELENGTH, ex_p1);
            }

            //Nikon/SLxPictureMetadata/sPicturePlanes/sPlaneNew/a1/pFilterPath/m_pFilter/m_EmissionSpectrum/pPoint/Point0/dWavelength: 607
            double em_p1 = hash->get_value_double(path_channel + "/pFilterPath/m_pFilter/m_EmissionSpectrum/pPoint/Point0/dWavelength", 0.0);
            double em_p2 = hash->get_value_double(path_channel + "/pFilterPath/m_pFilter/m_EmissionSpectrum/pPoint/Point1/dWavelength", 0.0);
            if (em_p1 > 0 && em_p2 > 0) {
                hash->set_value(path + bim::CHANNEL_INFO_EM_WAVELENGTH, xstring::xprintf("%.0f-%.0f", em_p1, em_p2));
            } else if (em_p1 > 0) {
                hash->set_value(path + bim::CHANNEL_INFO_EM_WAVELENGTH, em_p1);
            }
        }

        // color
        bim::Color<float> color((bim::uint32)hash->get_value_int(path_channel + "/pFilterPath/m_pFilter/m_uiColor", 0));
        hash->set_value(path + bim::CHANNEL_INFO_COLOR, color.to_string_float());
        //hash->set_value(xstring::xprintf(bim::CHANNEL_COLOR_TEMPLATE.c_str(), i), color.to_string_8bit());
    }

    return 0;
}

//----------------------------------------------------------------------------
// READ
//----------------------------------------------------------------------------

template<typename T>
void copy_nd2_to_bitmap(uint64 W, uint64 H, void *buffer, uint64 samples, uint64 stride_in, ImageBitmap *bmp) {
    uint64 stride_out = W;
    for (uint64 c = 0; c < samples; ++c) {
        for (uint64 y = 0; y < H; ++y) {
            const T *pin = (const T *)((const char *)buffer + (y * stride_in));
            T *p = ((T *)bmp->bits[c]) + (y * stride_out);
            for (uint64 x = 0; x < W; ++x) {
                p[x] = pin[c];
                pin += samples;
            }
        } // for y
    }     // for c
}

int nd2_get_image_number(FormatHandle *fmtHndl, const bim::uint &page) {
    fmtHndl->pageNumber = page;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    int fov = -1;
    int z = -1;
    int t = -1;
    //if (conf && conf->keyExists("-slice-fov")) fov = conf->getValueInt("-slice-fov", 0);
    if (conf && conf->keyExists("-slice-serie")) fov = conf->getValueInt("-slice-serie", 0);
    if (conf && conf->keyExists("-slice-z")) z = conf->getValueInt("-slice-z", 0);
    if (conf && conf->keyExists("-slice-t")) t = conf->getValueInt("-slice-t", 0);

    int nfov = par->number_dims.size() > 3 ? std::max<int>(par->number_dims[1], 1) : 1;
    if (info->number_z > 1 && info->number_t <= 1 && z < 0) {
        z = page;
    } else if (info->number_z <= 1 && info->number_t > 1 && t < 0) {
        t = page;
    } else if (info->number_z > 1 && info->number_t > 1 && z < 0 && t < 0) {
        t = static_cast<int>(floor(page / info->number_z));
        z = static_cast<int>(page - t * info->number_z);
    }

    int nz = std::max<int>(static_cast<int>(info->number_z), 1);
    int nt = std::max<int>(static_cast<int>(info->number_t), 1);
    z = std::max<int>(z, 0);
    fov = std::max<int>(fov, 0);
    t = std::max<int>(t, 0);

    return (fov * nz) + t * (nfov * nz) + z;
}

bim::uint nd2ReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    fmtHndl->pageNumber = page;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;

    ImageBitmap *bmp = fmtHndl->image;
    if (allocImg(fmtHndl, info, bmp) != 0) return 1;

    bim::uint64 idx = nd2_get_image_number(fmtHndl, page);
    std::vector<bim::uint8> buffer;
    par->read_image(idx, info, buffer);
    if (buffer.size() == 0) return 1;

    // copy the buffer
    int bytes = par->line_stride_bytes;
    if (info->depth == 8 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        copy_nd2_to_bitmap<bim::uint8>(info->width, info->height, &buffer[0], info->samples, bytes, bmp);
    } else if (info->depth == 16 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        copy_nd2_to_bitmap<bim::uint16>(info->width, info->height, &buffer[0], info->samples, bytes, bmp);
    } else if (info->depth == 32 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        copy_nd2_to_bitmap<bim::uint32>(info->width, info->height, &buffer[0], info->samples, bytes, bmp);
    } else if (info->depth == 64 && info->pixelType == bim::DataFormat::FMT_UNSIGNED) {
        copy_nd2_to_bitmap<bim::uint64>(info->width, info->height, &buffer[0], info->samples, bytes, bmp);
    } else if (info->depth == 32 && info->pixelType == bim::DataFormat::FMT_FLOAT) {
        copy_nd2_to_bitmap<bim::float32>(info->width, info->height, &buffer[0], info->samples, bytes, bmp);
    } else if (info->depth == 64 && info->pixelType == bim::DataFormat::FMT_FLOAT) {
        copy_nd2_to_bitmap<bim::float64>(info->width, info->height, &buffer[0], info->samples, bytes, bmp);
    }

    return 0;
}

bim::uint nd2ReadImageLevelProc(FormatHandle *fmtHndl, bim::uint page, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    // N/A
    return 0;
}

bim::uint nd2ReadImageTileProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 xid, bim::uint64 yid, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    // N/A
    return 0;
}

bim::uint nd2ReadImageRegionProc(FormatHandle *fmtHndl, bim::uint page, bim::uint64 x1, bim::uint64 y1, bim::uint64 x2, bim::uint64 y2, bim::uint level) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    bim::ND2Params *par = (bim::ND2Params *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    // N/A
    return 0;
}

//****************************************************************************
// exported
//****************************************************************************

#define BIM_ND2_NUM_FORMATS 1

FormatItem nd2Items[BIM_ND2_NUM_FORMATS] = {
    {                   //0
      "ND2",            // short name, no spaces
      "Nikon: NIS ND2", // Long format name
      "nd2",            // pipe "|" separated supported extension list
      1,                //canRead;      // 0 - NO, 1 - YES
      0,                //canWrite;     // 0 - NO, 1 - YES
      1,                //canReadMeta;  // 0 - NO, 1 - YES
      0,                //canWriteMeta; // 0 - NO, 1 - YES
      0,                //canWriteMultiPage;   // 0 - NO, 1 - YES
      //TDivFormatConstrains constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
      { 0, 0, 0, 0, 0, 0, 0, 0 } }
};

FormatHeader nd2Header = {

    sizeof(FormatHeader),
    "1.0.1",
    "Nikon",
    "Nikon",

    BIM_FORMAT_ND2_MAGIC_SIZE,
    { 1, BIM_ND2_NUM_FORMATS, nd2Items },

    nd2ValidateFormatProc,
    nd2AquireFormatProc, //AquireFormatProc
    nd2ReleaseFormatProc, //ReleaseFormatProc

    // params
    NULL, //AquireIntParamsProc
    NULL, //LoadFormatParamsProc
    NULL, //StoreFormatParamsProc

    // image begin
    nd2OpenImageProc,  //OpenImageProc
    nd2CloseImageProc, //CloseImageProc

    // info
    nd2GetNumPagesProc,  //GetNumPagesProc
    nd2GetImageInfoProc, //GetImageInfoProc

    // read/write
    nd2ReadImageProc,       //ReadImageProc
    NULL,                   //WriteImageProc
    nd2ReadImageTileProc,   //ReadImageTileProc
    NULL,                   //WriteImageTileProc
    nd2ReadImageLevelProc,  //ReadImageLevelProc
    NULL,                   //WriteImageLineProc
    nd2ReadImageRegionProc, //ReadImageRegionProc
    NULL,                   //WriteImageRegionProc
    nd2_append_metadata,    // AppendMetaDataProc
};

extern "C" {

FormatHeader *nd2GetFormatHeader(void) {
    return &nd2Header;
}

} // extern C
