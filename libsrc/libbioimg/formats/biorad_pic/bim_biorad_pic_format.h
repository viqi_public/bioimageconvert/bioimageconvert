/*****************************************************************************
  BIORAD PIC
  Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    04/22/2004 13:06 - First creation
    05/10/2004 14:55 - Big endian support
    08/04/2004 22:25 - Update to FMT_IFS 1.2, support for io protorypes
    2005-01-01 01:01 - supports Big-Endian and Small-Endian arcitectures
    2021-11-09 15:50 - cleaned-up

  ver : 4
*****************************************************************************/

#ifndef BIM_BIORADPIC_FORMAT_H
#define BIM_BIORADPIC_FORMAT_H

#include <cstdio>
#include <string>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>
#include <xstring.h>

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *bioRadPicGetFormatHeader(void);
}

namespace bim {

constexpr unsigned int BIM_BIORAD_HEADER_SIZE = 76;
constexpr unsigned int BIM_BIORAD_NOTE_SIZE = 96;

class BioRadPicParams {
public:
    BioRadPicParams();

    ImageInfo i;

    uint64 num_images = 0;
    uint64 page_size_bytes = 0;
    uint64 data_offset = BIM_BIORAD_HEADER_SIZE;
    uint64 notes_offset = 0;
    //uint64 has_notes = 0;

    // metadata
    bim::xstring note01;
    bim::xstring note20;
    bim::xstring note21;
};

} // namespace bim

#endif // BIM_BIORADPIC_FORMAT_H
