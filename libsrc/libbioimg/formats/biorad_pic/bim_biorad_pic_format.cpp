/*****************************************************************************
  BIORAD PIC
  Copyright (c) 2004 by Dmitry V. Fedorov <www.dimin.net> <dima@dimin.net>
  Copyright (c) 2005 Center for Bio-Image Informatics, UCSB
  Copyright (c) 2021 ViQi Inc

  Author: Dima Fedorov <dima@viqi.org>

  History:
    04/22/2004 13:06 - First creation
    05/10/2004 14:55 - Big endian support
    08/04/2004 22:25 - Update to FMT_IFS 1.2, support for io protorypes
    2005-01-01 01:01 - supports Big-Endian and Small-Endian arcitectures
    2021-11-09 15:50 - cleaned-up

  ver : 4
*****************************************************************************/

#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <string>
#include <vector>

#include <bim_metatags.h>
#include <tag_map.h>
#include <xstring.h>
#include <xconf.h>
#include <xdatetime.h>
#include <xconf.h>

#include "bim_biorad_pic_format.h"

using namespace bim;


//****************************************************************************
// Misc
//****************************************************************************

BioRadPicParams::BioRadPicParams() {
    //i = initImageInfo();
}


//****************************************************************************
// INTERNAL STRUCTURES
//****************************************************************************

void bioradReadNotes(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    if (par->notes_offset <= BIM_BIORAD_HEADER_SIZE) return;

    std::vector<unsigned char> note(BIM_BIORAD_NOTE_SIZE+1, 0);
    xseek(fmtHndl, par->notes_offset, SEEK_SET);
    while (xread(fmtHndl, &note[0], 1, BIM_BIORAD_NOTE_SIZE) == BIM_BIORAD_NOTE_SIZE) {
        short note_type = *(int16 *)(&note[0] + 10);
        if (bim::bigendian) swapShort((uint16 *)&note_type);
        char *text = (char *)(&note[0] + 16);
        if (note_type == 1) {
            if (text[0] == '\0') continue;
            par->note01 += text;
            par->note01 += '\n';
        } else if (note_type == 20) {
            if (text[0] == '\0') continue;
            par->note20 += text;
            par->note20 += '\n';
        } else if (note_type == 21) {
            if (text[0] == '\0') continue;
            par->note21 += text;
            par->note21 += '\n';
        }
    }
}

inline unsigned int biorad_read_uint16(unsigned char *header, unsigned int pos ) {
    unsigned int v = *(header + pos + 1);
    v *= 256;
    v += *(header + pos);
    return v;
}

void bioradGetImageInfo(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    if (fmtHndl->stream == NULL) return;
    if (xseek(fmtHndl, 0, SEEK_SET) != 0) return;

    info->imageMode = ImageModes::IM_GRAYSCALE;
    info->samples = 1;
    info->depth = 8;

    std::vector<bim::uint8> header_buf(BIM_BIORAD_HEADER_SIZE);
    unsigned char *header = &header_buf[0];
    if (xread(fmtHndl, header, 1, BIM_BIORAD_HEADER_SIZE) != BIM_BIORAD_HEADER_SIZE) return;

    par->num_images = biorad_read_uint16(header, 4);
    info->number_pages = par->num_images;
    info->width = biorad_read_uint16(header, 0);
    info->height = biorad_read_uint16(header, 2);

    //par->has_notes = *(int32 *)(header + 10);
    //if (bim::bigendian) swapLong((uint32 *)&par->has_notes);

    unsigned int val = biorad_read_uint16(header, 14);
    if (val == 1) {
        info->depth = 8;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    } else {
        info->depth = 16;
        info->pixelType = bim::DataFormat::FMT_UNSIGNED;
    }

    par->page_size_bytes = info->width * info->height * (info->depth / 8);
    par->notes_offset = par->data_offset + par->page_size_bytes * info->number_pages;

    if ((info->number_pages == 3) || (info->number_pages == 2)) {
        info->samples = 3;
        info->number_pages = 1;
        info->imageMode = ImageModes::IM_MULTI;
    }

    bioradReadNotes(fmtHndl);

    // if more than 1 page it's a z series
    if (info->number_pages > 1) {
        info->number_dims = 3;
        info->number_z = info->number_pages;
    }
}

//----------------------------------------------------------------------------
// READ PROC
//----------------------------------------------------------------------------

bim::uint bioradUpdatePageNumberND(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    XConf *conf = fmtHndl->arguments;
    if (page > 0)
        return page;

    int z = -1;
    if (conf && conf->keyExists("-slice-z"))
        z = conf->getValueInt("-slice-z");

    if (info->number_z > 1 && info->number_t <= 1 && z >= 0) {
        page = z;
    }

    return page;
}

static int read_biorad_image(FormatHandle *fmtHndl) {
    if (!fmtHndl || !fmtHndl->internalParams) return 1;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;
    ImageBitmap *img = fmtHndl->image;
    fmtHndl->pageNumber = bioradUpdatePageNumberND(fmtHndl, (bim::uint)fmtHndl->pageNumber);

    // init the image
    if (allocImg(fmtHndl, info, img) != 0) return 1;
    bim::uint64 offset = par->data_offset + fmtHndl->pageNumber * par->page_size_bytes;

    if (fmtHndl->stream == NULL) return 1;
    if (xseek(fmtHndl, offset, SEEK_SET) != 0) return 1;
    if (xread(fmtHndl, img->bits[0], par->page_size_bytes, 1) != 1) return 1;
    if ((img->i.depth == 16) && (bim::bigendian))
        swapArrayOfShort((uint16 *)img->bits[0], par->page_size_bytes / 2);

    xprogress(fmtHndl, 0, 10, "Reading BIORAD");

    if (info->imageMode == bim::ImageModes::IM_MULTI) { // three pages are considered xRGB channels

        offset += par->page_size_bytes;
        if (xseek(fmtHndl, offset, SEEK_SET) != 0) return 1;
        if (xread(fmtHndl, img->bits[1], par->page_size_bytes, 1) != 1) return 1;

        if ((img->i.depth == 16) && (bim::bigendian))
            swapArrayOfShort((uint16 *)img->bits[1], par->page_size_bytes / 2);

        if (par->num_images > 2) {
            offset += par->page_size_bytes;
            if (xseek(fmtHndl, offset, SEEK_SET) != 0) return 1;
            if (xread(fmtHndl, img->bits[2], par->page_size_bytes, 1) != 1) return 1;

            if ((img->i.depth == 16) && (bim::bigendian))
                swapArrayOfShort((uint16 *)img->bits[2], par->page_size_bytes / 2);
        } else {
            memset(img->bits[2], 0, par->page_size_bytes);
        }
    }

    return 0;
}

//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

void bioradParseNote01(FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    if (par->note01.size() <= 0) return;

    if (!par->note01.startsWith("Live ")) return;

    //Live Thu Aug  7 13:42:13 2003  Zoom 2.0 Kalman 3 Mixer A PMT
    hash->set_value(bim::DOCUMENT_DATETIME, bim::DateTime::from_string(par->note01, "Live %a %b%n%d %H:%M:%S %Y").to_string_iso8601());
}

void bioradParseNote20(FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return;
    if (fmtHndl->internalParams == NULL) return;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    ImageInfo *info = &par->i;

    // now parse it
    if (par->note20.size() <= 0) return;

    std::vector<float> pixel_size(3, 0.0);
    int d = 0;
    float f = 0.0;

    // X
    if (par->note20.contains("AXIS_2 =")) { // AXIS_2 = 257 0.000000e+00 2.689307e-01 microns
        par->note20.right("AXIS_2 = ").sscanf("%d %e %e microns ", &d, &f, &pixel_size[0]);
    } else if (par->note20.contains("AXIS_2 ")) { // AXIS_2 257 0.000000e+00 2.286561e-01 microns
        par->note20.right("AXIS_2 ").sscanf("%d %e %e microns ", &d, &f, &pixel_size[0]);
    }

    // Y
    if (par->note20.contains("AXIS_3 =")) { // AXIS_3 = 257 0.000000e+00 2.689307e-01 microns
        par->note20.right("AXIS_3 = ").sscanf("%d %e %e microns ", &d, &f, &pixel_size[1]);
    } else if (par->note20.contains("AXIS_3 ")) { // AXIS_3 257 0.000000e+00 2.286561e-01 microns
        par->note20.right("AXIS_3 ").sscanf("%d %e %e microns ", &d, &f, &pixel_size[1]);
    }

    // Z
    if (par->note20.contains("AXIS_4 =")) { // AXIS_4 = 257 0.000000e+00 2.689307e-01 microns
        par->note20.right("AXIS_4 = ").sscanf("%d %e %e microns ", &d, &f, &pixel_size[2]);
    } else if (par->note20.contains("AXIS_4 ")) { // AXIS_4 257 0.000000e+00 2.286561e-01 microns
        par->note20.right("AXIS_4 ").sscanf("%d %e %e microns ", &d, &f, &pixel_size[2]);
    }

    hash->set_value(bim::PIXEL_RESOLUTION_X, pixel_size[0]);
    hash->set_value(bim::PIXEL_RESOLUTION_Y, pixel_size[1]);
    hash->set_value(bim::PIXEL_RESOLUTION_Z, pixel_size[2]);

    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_X, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Y, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
    hash->set_value(bim::PIXEL_RESOLUTION_UNIT_Z, bim::PIXEL_RESOLUTION_UNIT_MICRONS);
}

bim::uint biorad_append_metadata(FormatHandle *fmtHndl, bim::TagMap *hash) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams == NULL) return 1;
    if (!hash) return 1;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;

    bioradParseNote01(fmtHndl, hash);
    bioradParseNote20(fmtHndl, hash);

    hash->set_value(bim::IMAGE_NUM_Z, (unsigned int)par->num_images);
    hash->set_value(bim::IMAGE_NUM_T, 1);

    XConf *conf = fmtHndl->arguments;
    if (conf && !conf->hasKeyWith("-meta")) return 0;

    hash->parse_ini(par->note20, "=", "BioRad/");
    hash->set_value_from_old_key("BioRad/LENS_MAGNIFICATION", bim::OBJECTIVE_MAGNIFICATION);

    if (par->note01.size() > 0)
        hash->set_value("BioRad/Note01", par->note01);

    if (par->note21.size() > 0)
        hash->set_value("BioRad/Note21", par->note21);

    return 0;
}


//****************************************************************************
// FORMAT DEMANDED FUNTIONS
//****************************************************************************

//----------------------------------------------------------------------------
// PARAMETERS, INITS
//----------------------------------------------------------------------------

int bioRadPicValidateFormatProc(BIM_MAGIC_STREAM *magic, bim::uint length, const bim::Filename fileName) {
    if (length < 56) return -1;

    if (fileName) {
        xstring filename(fileName);
        filename = filename.toLowerCase();
        if (!filename.endsWith(".pic")) return -1;
    }

    unsigned char *mag_num = (unsigned char *)magic;
    if ((mag_num[54] == 0x39) && (mag_num[55] == 0x30)) return 0;
    return -1;
}

FormatHandle bioRadPicAquireFormatProc(void) {
    FormatHandle fp = initFormatHandle();
    return fp;
}

void bioRadPicCloseImageProc(FormatHandle *fmtHndl);
void bioRadPicReleaseFormatProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    bioRadPicCloseImageProc(fmtHndl);
}

//----------------------------------------------------------------------------
// OPEN/CLOSE
//----------------------------------------------------------------------------

void bioRadPicCloseImageProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return;
    xclose(fmtHndl);
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    fmtHndl->internalParams = 0;
    delete par;
}

bim::uint bioRadPicOpenImageProc(FormatHandle *fmtHndl, ImageIOModes io_mode) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->internalParams != NULL) bioRadPicCloseImageProc(fmtHndl);
    fmtHndl->internalParams = (void *) new BioRadPicParams();

    fmtHndl->io_mode = io_mode;
    if (io_mode == bim::ImageIOModes::IO_READ) {
        xopen(fmtHndl);
        if (fmtHndl->stream == NULL) return 1;
        bioradGetImageInfo(fmtHndl);
        return 0;
    }

    return 1;
}

//----------------------------------------------------------------------------
// INFO for OPEN image
//----------------------------------------------------------------------------

bim::uint bioRadPicGetNumPagesProc(FormatHandle *fmtHndl) {
    if (fmtHndl == NULL) return 0;
    if (fmtHndl->internalParams == NULL) return 0;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    return (bim::uint)par->i.number_pages;
}

ImageInfo bioRadPicGetImageInfoProc(FormatHandle *fmtHndl, bim::uint page_num) {
    if (fmtHndl == NULL) return ImageInfo();
    fmtHndl->pageNumber = page_num;
    BioRadPicParams *par = (BioRadPicParams *)fmtHndl->internalParams;
    return par->i;
}

//----------------------------------------------------------------------------
// READ/WRITE
//----------------------------------------------------------------------------

bim::uint bioRadPicReadImageProc(FormatHandle *fmtHndl, bim::uint page) {
    if (fmtHndl == NULL) return 1;
    if (fmtHndl->stream == NULL) return 1;

    fmtHndl->pageNumber = page;
    return read_biorad_image(fmtHndl);
}

bim::uint bioRadPicWriteImageProc(FormatHandle *) {
    return 1;
}

//****************************************************************************
// EXPORTED
//****************************************************************************

constexpr unsigned int BIM_BIORAD_NUM_FORMATS = 1;

FormatItem bioRadPicItems[BIM_BIORAD_NUM_FORMATS] = { 
{ 
    "BIORAD-PIC", // short name, no spaces
    "ND: BioRad PIC", // Long format name
    "pic",        // pipe "|" separated supported extension list
    1,            //canRead;      // 0 - NO, 1 - YES
    0,            //canWrite;     // 0 - NO, 1 - YES
    1,            //canReadMeta;  // 0 - NO, 1 - YES
    0,            //canWriteMeta; // 0 - NO, 1 - YES
    0,            //canWriteMultiPage;   // 0 - NO, 1 - YES
    { 0, 0, 0, 1, 1, 8, 16, 1 } // constrains ( w, h, pages, minsampl, maxsampl, minbitsampl, maxbitsampl, noLut )
} 
};

FormatHeader bioRadPicHeader = {
    sizeof(FormatHeader),
    "2.0.1",
    "BioRad PIC",
    "BioRad PIC",

    56,                       // 0 or more, specify number of bytes needed to identify the file
    { 1, BIM_BIORAD_NUM_FORMATS, bioRadPicItems }, // FormatList

    bioRadPicValidateFormatProc,
    bioRadPicAquireFormatProc,  // AquireFormatProc
    bioRadPicReleaseFormatProc, // ReleaseFormatProc

    // params
    NULL, // AquireIntParamsProc
    NULL, // LoadFormatParamsProc
    NULL, // StoreFormatParamsProc

    // image begin
    bioRadPicOpenImageProc,  // OpenImageProc
    bioRadPicCloseImageProc, // CloseImageProc

    // info
    bioRadPicGetNumPagesProc,  // GetNumPagesProc
    bioRadPicGetImageInfoProc, // GetImageInfoProc

    // read/write
    bioRadPicReadImageProc, // ReadImageProc
    NULL,                   // WriteImageProc
    NULL,                   // ReadImageTileProc
    NULL,                   // WriteImageTileProc
    NULL,                   // ReadImageLevelProc
    NULL,                   // WriteImageLevelProc
    NULL,                   // ReadImageRegionProc
    NULL,                   // WriteImageRegionProc
    biorad_append_metadata, // AppendMetaDataProc
};

extern "C" {
FormatHeader *bioRadPicGetFormatHeader(void) { 
    return &bioRadPicHeader; 
}
} // extern C
