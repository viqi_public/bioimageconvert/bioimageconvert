/*****************************************************************************
  CZI support
  Copyright (c) 2017, Center for Bio-Image Informatics, UCSB
  Copyright (c) 2017, Dmitry Fedorov <www.dimin.net> <dima@dimin.net>

  Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
  2017-10-25 11:50:40 - First creation

  ver : 1
  *****************************************************************************/

#ifndef BIM_CZI_FORMAT_H
#define BIM_CZI_FORMAT_H

#include <cstdio>
#include <fstream>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

//#ifdef BIM_CZI_FORMAT
//#include <libCZI.h>
//#else
namespace libCZI {
class ICZIReader;
class IStream;
struct SubBlockInfo;
} // namespace libCZI
//#endif

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *cziGetFormatHeader(void);
}

namespace bim {

//----------------------------------------------------------------------------
// CZIBlock
//----------------------------------------------------------------------------

#pragma pack(push, 1)
struct CZIBlock {
    bim::int64 idx = -1;
    bim::int64 mindx = -1;
    bim::float64 scale = -1;

    bim::int64 logical_x = 0;
    bim::int64 logical_y = 0;
    bim::uint64 logical_w = 0;
    bim::uint64 logical_h = 0;
    bim::uint64 physical_w = 0;
    bim::uint64 physical_h = 0;

    bim::uint64 pixel_type = 0;

    bim::float32 Z = std::numeric_limits<float>::quiet_NaN();
    bim::float32 C = std::numeric_limits<float>::quiet_NaN();
    bim::float32 T = std::numeric_limits<float>::quiet_NaN();
    bim::float32 R = std::numeric_limits<float>::quiet_NaN();
    bim::float32 S = std::numeric_limits<float>::quiet_NaN();
    bim::float32 I = std::numeric_limits<float>::quiet_NaN();
    bim::float32 H = std::numeric_limits<float>::quiet_NaN();
    bim::float32 V = std::numeric_limits<float>::quiet_NaN();

public:
    CZIBlock(){};
    CZIBlock(const libCZI::SubBlockInfo &bi, const int &idx);
};
#pragma pack(pop)

//----------------------------------------------------------------------------
// CZIBlocks - find blocks based on their properties, should keep trees here
//----------------------------------------------------------------------------

class CZIBlocks {
public:
    CZIBlocks(){};
    ~CZIBlocks(){};

    void add(const CZIBlock &block);
    size_t size() const { return this->blocks.size(); };

    const CZIBlock &get(const int &idx) const;
    std::vector<CZIBlock *> find(int mindx = 0,
                                 double scale = std::numeric_limits<double>::quiet_NaN(),
                                 float c = std::numeric_limits<float>::quiet_NaN(),
                                 float z = std::numeric_limits<float>::quiet_NaN(),
                                 float t = std::numeric_limits<float>::quiet_NaN(),
                                 float r = std::numeric_limits<float>::quiet_NaN(),
                                 float s = std::numeric_limits<float>::quiet_NaN(),
                                 float i = std::numeric_limits<float>::quiet_NaN(),
                                 float h = std::numeric_limits<float>::quiet_NaN(),
                                 float v = std::numeric_limits<float>::quiet_NaN()) const;

private:
    std::vector<CZIBlock> blocks;

public:
    // I/O
    bool to(std::ostream *s) const;
    bool from(std::istream *s);
};

//----------------------------------------------------------------------------
// CZIOverview - everything about an image
//----------------------------------------------------------------------------

class CZIOverview {
public:
    const char mgk[4] = { 'B', 'I', 'M', '1' };
    const char spc[4] = { 'C', 'Z', 'O', '1' };

public:
    CZIOverview(){};
    ~CZIOverview(){};

    std::vector<double> scales_virtual;
    std::set<double> scales;
    std::map<double, int> tiles_at_scales;

    std::set<int> mindices;

    std::set<int> zs;
    std::set<int> cs;
    std::set<int> ts;
    std::set<int> rotations;
    std::set<int> scenes;
    std::set<int> illuminations;
    std::set<int> phases;
    std::set<int> views;

    int x0 = std::numeric_limits<int>::max();
    int y0 = std::numeric_limits<int>::max();
    int x1 = std::numeric_limits<int>::lowest();
    int y1 = std::numeric_limits<int>::lowest();

    int fov_width = 0;
    int fov_height = 0;
    int image_width = 0;
    int image_height = 0;
    int num_fovs = 0;

    CZIBlocks blocks;

public:
    bool is_loaded() const { return this->image_width > 0 && this->image_height > 0; };
    std::vector<CZIBlock *> boundary() const;

    // I/O
    bool to(std::ostream *s) const;
    bool to(const std::string &fileName) const;
    bool from(std::istream *s);
    bool from(const std::string &fileName);
};

//----------------------------------------------------------------------------
// CZIParams - decoding params
//----------------------------------------------------------------------------

class CZIParams {
public:
    static const int virtual_tile_width = 512;
    static const int virtual_tile_height = 512;
    const std::vector<std::string> channel_names = { "Red", "Green", "Blue", "Alpha" };
    const std::vector<std::vector<int>> channel_colors = { { 255, 0, 0, 255 }, { 0, 255, 0, 255 }, { 0, 0, 255, 255 }, { 0, 0, 0, 0 } };

public:
    CZIParams();
    ~CZIParams();

    ImageInfo i;
    std::shared_ptr<libCZI::IStream> stream;
    std::shared_ptr<libCZI::ICZIReader> cziReader;

    CZIOverview o;
    bim::xstring contrast_method = "";
    int offset_meta_objective = 0;
    int label_idx = -1;
    int preview_idx = -1;
    bool attachments_loaded = false;

    bim::ImageModes imageMode = bim::ImageModes::IM_UNKNOWN;
    int depth = -1;
    bim::DataFormat pixelType = bim::DataFormat::FMT_UNDEFINED;
    int samples = -1;
    bim::xstring path;

    //std::vector<char> buffer_icc;

    void open(const char *filename, bim::ImageIOModes mode);
};

} // namespace bim

#endif // BIM_CZI_FORMAT_H
