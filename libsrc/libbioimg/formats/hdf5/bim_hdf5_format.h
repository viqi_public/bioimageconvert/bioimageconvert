/*****************************************************************************
HDF5 support
Copyright (c) 2018, Center for Bio-Image Informatics, UCSB
Copyright (c) 2019-2021, ViQi Inc

VQI is a proprietary format and requires licese from ViQi Inc for writing

Author: Dmitry Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
2018-04-10 09:12:40 - First creation

ver : 10
*****************************************************************************/

#ifndef BIM_HDF5_FORMAT_H
#define BIM_HDF5_FORMAT_H

#include <cstdio>
#include <fstream>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_img_format_utils.h>

#include <H5Tpublic.h>
#include <H5Cpp.h>

namespace H5 {
    //class H5File;
    //class CompType;
    //class DataSet;
    //class DataType;
    //enum H5T_class_t;
}

// DLL EXPORT FUNCTION
extern "C" {
bim::FormatHeader *hdf5GetFormatHeader(void);
}

namespace bim {

static const int HDF5_GUESS_MAX_CHANNELS = 6;
static const int VIQI_MAX_ITEMS_PER_BLOCK = 1000000;

static const int VIQI_HEATMAP_INTERPOLATION_SPACING = 4;

// sub-formats
enum HDF5SubFormat {
    FMT_HDF5 = 0,
    FMT_IMS = 1,
    FMT_DREAM3D = 2,
    FMT_VIQI = 3
};

//----------------------------------------------------------------------------
// VQITableColumn, VQITableMeasure
//----------------------------------------------------------------------------

class VQITableColumn {
public:
    VQITableColumn(const H5::CompType &cmpt, int i);
    ~VQITableColumn();

    int idx = 0;
    bim::xstring name;
    H5::DataType dt;
    H5T_class_t ct;
    size_t offset = 0;
    size_t size_bytes = 0;
    //H5::DataType dt;
    bool visualizable = false;
    xstring interpolation = "bc";
};

class VQITableMeasure {
public:
    VQITableMeasure(const VQITableColumn &col, const xstring &name, int idx = -1);
    ~VQITableMeasure();

    int idx = 0;
    int idx_column = 0;
    bim::xstring name;
    bim::xstring name_long;
    bim::xstring column;
    int class_id = -1;
    H5::DataType dt;
    H5T_class_t ct;
    size_t offset = 0;
    size_t offset_read = 0;
    size_t size_bytes = 0;
    bim::Image::ResizeMethod interpolation = bim::Image::ResizeMethod::szBiCubic; // bim::Image::szNearestNeighbor //bim::Image::szLanczos
    bool values_are_class_id = false;
    bool randomizer = false;
};

//----------------------------------------------------------------------------
// VQITableMeasureReader - buffered reader for VQI table columns
// the reader will read a specific set of columns in chunks of chunk_size rows and store them in the buffer indexed by the object id
// max_buffer_size allows control of the buffer size in rows, reaching max_buffer_size will offload data of the oldest accessed chunk from the buffer
//----------------------------------------------------------------------------

class VQITableMeasureReader {
public:
    VQITableMeasureReader() = default;
    VQITableMeasureReader(H5::H5File *file, const bim::xstring &path, const std::vector<VQITableMeasure> &measures, bim::uint chunk_size = 0, bim::uint max_buffer_size = 0) {
        this->init(file, path, measures, chunk_size, max_buffer_size);
    }
    ~VQITableMeasureReader() {};

    void init(H5::H5File *file, const bim::xstring &path, const std::vector<VQITableMeasure> &measures, bim::uint chunk_size = 0, bim::uint max_buffer_size = 0);

    template<typename T>
    T get_measure_value(bim::uint object_id, bim::uint measure_id, bim::uint block_id = 0, const T &def = 0);

protected:
    bim::uint chunk_size = 1000;
    bim::uint max_buffer_size = 10000000;
    H5::CompType subtype;

    std::vector<bim::uint> viqi_block_item_id_offsets;
    std::vector<bim::uint> viqi_block_first_row;

    std::vector<VQITableMeasure> measures;
    H5::H5File *file = NULL;
    bim::xstring path;
    bim::xstring path_table;

    hsize_t nchunks = 0;
    hsize_t nrows = 0;
    hsize_t row_sz = 0;

    H5::DataSet dataset;
  
    int measure_id = -1;
    void init_measure(bim::uint measure_id); // init requested measure reading
    void init_measures(); // init object id and class label measures 

    std::map<bim::uint, time_t> chunk_map;
    bim::uint buffer_last_row = 0;
    std::vector<bim::uint8> measure_buffer;
    std::map<bim::uint, bim::uint> measure_index; // mapping of object_id to initial offset in the measure_buffer

    void load_measures(bim::uint measure_id, bim::uint object_id, bim::uint block_id = 0);
    void load_chunk(bim::uint chunk_id);
    bim::uint find_first_chunk_id(bim::uint block_id);

    void parse_measures(bim::uint rows); // update measure index map
   
    VQITableMeasure *m_object_id = NULL;
    VQITableMeasure *m_class_label = NULL;
    VQITableMeasure *m = NULL;
};


//----------------------------------------------------------------------------
// VQITable
//----------------------------------------------------------------------------

class VQITable {
public:
    const int rows_chunk_size = 1000;

public:
    VQITable();
    VQITable(H5::H5File *file, const bim::xstring &path);
    ~VQITable();

    void init(H5::H5File *file, const bim::xstring &path);
    void load_column_info(const H5::DataSet &dataset);
    void init_measures();

    H5::H5File *file = NULL;
    
    std::vector<VQITableColumn> columns;
    std::vector<VQITableMeasure> measures;
    hsize_t nrows = 0;
    bim::xstring path;
    bim::xstring col_object_id;
    bim::xstring col_roi_id;
    bim::xstring col_class_label;
    bim::xstring col_class_confidence;

    std::map<unsigned int, xstring> class_id_to_label_mapping;

    VQITableColumn* getColumn(const xstring &name) const;
    VQITableMeasure* getMeasure(const xstring &name) const;
    VQITableMeasure* getMeasure(size_t measure_id) const;
    bool hasMeasures() const {
        return this->measures.size() > 0;
    }

    bool has_metadata() const;
    void append_metadata(TagMap *hash) const;

    template<typename T>
    T get_measure_value(bim::uint object_id, bim::uint measure_id, bim::uint block_id = 0, const T &def = 0);

protected:
    VQITableMeasureReader measure_reader;
};

//----------------------------------------------------------------------------
// HDF5Params - decoding params
//----------------------------------------------------------------------------

class HDF5Params {
public:
    HDF5Params();
    ~HDF5Params();

    ImageInfo i;
    H5::H5File *file = NULL;
    bim::xstring file_name;
    bim::xstring path;
    bim::xstring path_requested;

    bool image_class = false;
    bool interlaced = false;

    bim::xstring viqi_image_content;
    std::vector<int> viqi_image_size;
    std::vector<bim::xstring> viqi_image_dimensions;
    bool viqi_image_interleaved = false;
    std::vector<int> dimensions_image;
    std::vector<int> dimensions_tile;
    std::vector<int> dimensions_element;
    std::vector<int> dimensions_indices;
    std::vector<int> positions;
    uint num_spatial_dimensions = 2;
    int image_num_samples = 1;

    int resolution_levels = 1;
    std::vector<double> zscales;
    std::vector<double> scales;
    int hdf_pixel_format = -1;

    int block_id = -1;
    int item_id = -1;

    bool sparse_mode = false;
    bool sparse_element_read = false;
    
    bool mask_mode = false;
    bool values_from_measures = false;

    std::vector<int> position_spectrum; // method to define bands of interest to produce a final image with N channels
    std::vector<int> spectral_wavelengths;
    std::string spectral_wavelength_units;
    VQITable table;

    bim::Image::ResizeMethod interpolation = bim::Image::ResizeMethod::szBiCubic; // bim::Image::szNearestNeighbor //bim::Image::szLanczos
    bool user_given_interpolation = false;

    int num_blocks = 0;
    int num_items = 0;
    std::vector<int> block_bboxs;
    int loaded_level = -1;

    //std::vector<char> buffer_icc;

    void open(const char *filename, bim::ImageIOModes mode);
    void close();
};

} // namespace bim

#endif // BIM_HDF5_FORMAT_H
