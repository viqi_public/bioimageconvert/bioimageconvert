/*****************************************************************************
  OME Type definitions
  Copyright (c) 2019, Center for Bio-Image Informatics, UCSB

  Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

  History:
    2019-07-09 23:16 - First creation

  Ver : 1
*****************************************************************************/

#ifndef BIM_OME_TYPES_H
#define BIM_OME_TYPES_H

#include <bim_img_format_interface.h>
#include <xstring.h>
#include <xtypes.h>

#include <limits>
#include <sstream>
#include <string>

// OME Color class from Bioformats Color.h
class OmeColor {
public:
    // The type of an individual color component (R, G, B, A)
    typedef bim::uint8 component_type;
    // The type of all components composed as a single RGBA value (unsigned)
    typedef bim::uint32 composed_type;
    // The type of all components composed as a single RGBA value (signed)
    typedef bim::int32 signed_type;

    inline OmeColor() : value(0x000000FFU) {}

    inline OmeColor(composed_type value) : value(value) {}

    inline OmeColor(signed_type value) : value(static_cast<composed_type>(value)) {}

    inline OmeColor(component_type r, component_type g, component_type b, component_type a = std::numeric_limits<component_type>::max()) : value(static_cast<composed_type>(r) << 24U |
                                                                                                                                                 static_cast<composed_type>(g) << 16U |
                                                                                                                                                 static_cast<composed_type>(b) << 8U |
                                                                                                                                                 static_cast<composed_type>(a) << 0U) {}

    inline ~OmeColor() {}

    // Get the red component of this color.
    inline component_type getRed() const {
        return static_cast<component_type>((this->value >> 24U) & 0xffU);
    }

    // Set the red component of this color.
    inline void setRed(component_type red) {
        this->value &= ~(0xFFU << 24U);
        this->value |= static_cast<composed_type>(red) << 24U;
    }

    // Get the green component of this color.
    inline component_type getGreen() const {
        return static_cast<component_type>((this->value >> 16U) & 0xffU);
    }

    // Set the green component of this color.
    inline void setGreen(component_type green) {
        this->value &= ~(0xffU << 16U);
        this->value |= static_cast<composed_type>(green) << 16U;
    }

    // Get the blue component of this color.
    inline component_type getBlue() const {
        return static_cast<component_type>((this->value >> 8U) & 0xffU);
    }

    // Set the blue component of this color.
    inline void setBlue(component_type blue) {
        this->value &= ~(0xffU << 8U);
        this->value |= static_cast<composed_type>(blue) << 8U;
    }

    // Get the alpha component of this color.
    inline component_type getAlpha() const {
        return static_cast<component_type>((this->value >> 0) & 0xffU);
    }

    // Set the alpha component of this color.
    inline void setAlpha(component_type alpha) {
        this->value &= ~(0xffU << 0);
        this->value |= static_cast<composed_type>(alpha) << 0;
    }

    // Get the *signed* integer value of this color.
    inline signed_type getValue() const {
        return static_cast<signed_type>(this->value);
    }

    // Set the integer value of this color from an unsigned integer
    inline void setValue(composed_type value) {
        this->value = value;
    }

    // Set the integer value of this color from a *signed* integer.
    inline void setValue(signed_type value) {
        this->value = static_cast<composed_type>(value);
    }

    // Cast the color to its value as an unsigned integer.
    inline operator composed_type() const {
        return this->value;
    }

    // Cast the color to its value as a signed integer.
    inline operator signed_type() const {
        return static_cast<signed_type>(this->value);
    }

    inline std::string toString() const {
        std::stringstream s;
        s << this->getValue();
        return s.str();
    }

private:
    composed_type value;
};

namespace bim {

std::string colorRGBStringToOMEString(const xstring &c);
std::string colorOMEStringToRGBString(const xstring &c);

std::string omeTiffPixelType(const bim::uint32 depth, const bim::DataFormat pixel_type);
std::string omeTiffPixelType(const bim::ImageBitmap *img);

std::string constructOMEXML(const std::string &filename, const bim::ImageBitmap *img, const bim::TagMap *hash,
                            const bool short_ome_format = false);
std::string constructOMEXML(const std::string &filename, const bim::uint64 width, const bim::uint64 height, const bim::uint32 samples,
                            const bim::uint64 number_z, const bim::uint64 number_t, const bim::uint32 depth,
                            const bim::DataFormat pixel_type, const bim::TagMap *hash,
                            const bool short_ome_format = false);

bim::TagMap parseOMEXML(const bim::xstring &tag_270, const bim::uint64 number_z, const bim::uint samples);

void ome_xml_append_metadata(const bim::xstring &tag_270, const bim::uint64 number_z, const bim::uint samples, bim::TagMap *hash);
xstring ometiff_normalize_xml_spaces(const bim::xstring &s);

} // namespace bim

#endif