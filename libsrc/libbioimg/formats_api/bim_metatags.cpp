/*****************************************************************************
 Tag names for metadata

 Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>
 Copyright (c) 2010 Vision Research Lab, UCSB <http://vision.ece.ucsb.edu>

 History:
   2010-07-29 17:18:22 - First creation

 Ver : 1
*****************************************************************************/

#include "bim_metatags.h"

// redefine the declaration macro in order to get proper initialization
#undef DECLARE_STR
#if defined(WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64) || defined(_MSVC)
// On MSVC, a const global variable has internal linkage by default. If you want
// the variable to have external linkage, apply the extern keyword to definition
// as well as to all other declarations in other files:
#define DECLARE_STR(S, V) extern const std::string bim::S = V;
#else
#define DECLARE_STR(S, V) const std::string bim::S = V;
#endif

#include "bim_metatags.def.h" // include actual string data
