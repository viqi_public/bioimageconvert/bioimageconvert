/*******************************************************************************

  Image Proxy Class - opens format session and allows reading tiles and levels

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>
  Copyright (c) 2018, ViQi Inc

  History:
    03/23/2014 18:03 - First creation

  ver: 1

*******************************************************************************/

#include <cmath>
#include <cstring>

#include <map>
#include <sstream>
#include <string>
#include <vector>

#include <bim_img_format_interface.h>
#include <bim_metatags.h>
#include <bim_format_manager.h>
#include <xstring.h>
#include <xtypes.h>

#include "bim_image_proxy.h"

using namespace bim;

//------------------------------------------------------------------------------
// ImageProxy
//------------------------------------------------------------------------------

ImageProxy::ImageProxy() {
    init();
}

ImageProxy::~ImageProxy() {
    closeFile();
    delete fm;
}

ImageProxy::ImageProxy(const std::string &fileName, XConf *c) {
    this->conf = c;
    init();
    openFile(fileName);
}

void ImageProxy::init() {
    fm = new FormatManager(this->conf);
    progress_proc = NULL;
    error_proc = NULL;
    test_abort_proc = NULL;
    external_manager = false;
}

bool ImageProxy::openFile(const std::string &fileName, XConf *c) {
    fm->setConfiguration(c);
    return fm->sessionStartRead((bim::Filename)fileName.c_str()) == 0;
}

void ImageProxy::closeFile() {
    if (external_manager) {
        external_manager = false;
        fm = new FormatManager();
    } else {
        fm->sessionEnd();
    }
}

//------------------------------------------------------------------------------

bool ImageProxy::read(Image &img, bim::uint page) {
    return fm->sessionReadImage(img.imageBitmap(), page) == 0;
}

void ImageProxy::parseScales() {
    if (this->scales.size() > 0) return;

    fm->sessionParseMetaData(fm->sessionGetCurrentPage());
    //int levels = fm->get_metadata_tag_int(bim::IMAGE_NUM_RES_L, 0);
    xstring s = fm->get_metadata_tag(bim::IMAGE_RES_L_SCALES, "");
    this->scales = s.splitDouble(",");
    if (this->scales.size() == 0)
        this->scales.push_back(1.0);
    this->full_width = fm->get_metadata_tag_int(bim::IMAGE_NUM_X, 0);
    this->full_height = fm->get_metadata_tag_int(bim::IMAGE_NUM_Y, 0);

    s = fm->get_metadata_tag(bim::TILE_SIZE_X, "");
    this->tile_sizes_w = s.splitInt(",");
    s = fm->get_metadata_tag(bim::TILE_SIZE_Y, "");
    this->tile_sizes_h = s.splitInt(",");
}

int ImageProxy::getImageLevel(const uint64 level, bool power_two_level) {
    this->parseScales();
    if (this->scales.size() == 0 && level == 0) return 0;
    if (!power_two_level) {
        return static_cast<int>(level < this->scales.size() ? level : -1);
    }

    double requested_scale = 1.0 / pow(2.0, (double)level);
    return this->getImageLevel(requested_scale);
}

int ImageProxy::getImageLevel(const double requested_scale) {
    this->parseScales();
    std::vector<double> difs = this->scales;
    for (size_t i = 0; i < this->scales.size(); ++i) {
        difs[i] = fabs(this->scales[i] - requested_scale);
    }
    int requested_level = (int)bim::minix<double>(&difs[0], difs.size());
    if (difs[requested_level] > requested_scale * 0.01) { // allow for minor fluctuations in scale
        return -1;
    }
    return requested_level;
}

int ImageProxy::getClosestLevel(const double requested_scale) {
    this->parseScales();
    std::vector<double> difs = this->scales;
    for (size_t i = 0; i < this->scales.size(); ++i) {
        double d = this->scales[i] - requested_scale;
        difs[i] = d >= 0 ? d : 100000;
    }
    return (int) bim::minix<double>(&difs[0], difs.size());
}

bool ImageProxy::readLevel(Image &img, const uint page, const uint64 level, bool power_two_level) {
    int requested_level = getImageLevel(level, power_two_level);
    if (requested_level < 0) {
        // requested scale does not exist in the image, use scaling interface
        double requested_scale = 1.0 / pow(2.0, (double)level);
        return this->readLevel(img, page, requested_scale);
    }
    if (static_cast<size_t>(requested_level) >= this->scales.size()) return false;
    return fm->sessionReadLevel(img.imageBitmap(), page, requested_level) == 0;
}

bool ImageProxy::readLevel(Image &img, bim::uint page, double scale) {
    int requested_level = getImageLevel(scale);
    if (requested_level >= 0) return this->readLevel(img, page, requested_level, false);

    // exact level is not present in the image, need to read and rescale
    requested_level = this->getClosestLevel(scale);
    if (requested_level < 0) return false;
    if (static_cast<size_t>(requested_level) >= this->scales.size()) return false;
    if (fm->sessionReadLevel(img.imageBitmap(), page, requested_level) != 0) return false;

    bim::uint w = bim::round<bim::uint>((double)this->full_width * scale);
    bim::uint h = bim::round<bim::uint>((double)this->full_height * scale);
    img = img.resample(w, h, bim::Image::ResizeMethod::szBiCubic);
    return true;
}

bool ImageProxy::readTile(Image &img, uint page, uint64 xid, uint64 yid, uint64 level, uint64 tile_size, bool power_two_level) {
    bim::uint64 x1 = xid * tile_size;
    bim::uint64 x2 = xid * tile_size + tile_size - 1;
    bim::uint64 y1 = yid * tile_size;
    bim::uint64 y2 = yid * tile_size + tile_size - 1;
    return this->readRegion(img, page, x1, y1, x2, y2, level, power_two_level);
}

bool ImageProxy::readTile(Image &img, uint page, uint64 xid, uint64 yid, double scale, uint64 tile_size) {
    bim::uint64 x1 = xid * tile_size;
    bim::uint64 x2 = xid * tile_size + tile_size - 1;
    bim::uint64 y1 = yid * tile_size;
    bim::uint64 y2 = yid * tile_size + tile_size - 1;
    return this->readRegion(img, page, x1, y1, x2, y2, scale);
}

bool ImageProxy::readRegion(Image &img,
                            uint page, 
                            uint64 x1, uint64 y1, 
                            uint64 x2, uint64 y2, 
                            uint64 level, bool power_two_level) {

    int requested_level = getImageLevel(level, power_two_level);
    if (requested_level < 0) {
        // requested scale does not exist in the image, use scaling interface
        double requested_scale = 1.0 / pow(2.0, (double)level);
        return this->readRegion(img, page, x1, y1, x2, y2, requested_scale);
    }

    // if decoder supports arbitrary tile access
    bool arbitrary_tiles = fm->get_metadata_tag(bim::IMAGE_RES_STRUCTURE, "") == bim::IMAGE_RES_STRUCTURE_ARBITRARY;
    if (arbitrary_tiles) {
        return fm->sessionReadRegion(img.imageBitmap(), page, x1, y1, x2, y2, requested_level) == 0;
    }

    // fetching from rigid structure decoder
    if (requested_level >= this->scales.size()) return false;

    const uint64 tile_size_x = x2 - x1 + 1;
    const uint64 tile_size_y = y2 - y1 + 1;

    // in case image levels contain tiles of different sizes
    int im_tile_w = this->tile_sizes_w.size() > requested_level ? this->tile_sizes_w[requested_level] : fm->get_metadata_tag_int(bim::TILE_NUM_X, 0);
    int im_tile_h = this->tile_sizes_h.size() > requested_level ? this->tile_sizes_h[requested_level] : fm->get_metadata_tag_int(bim::TILE_NUM_Y, 0);

    bool flat_structure = fm->get_metadata_tag(bim::IMAGE_RES_STRUCTURE, "") == bim::IMAGE_RES_STRUCTURE_FLAT;
    if (flat_structure && level > 0) {
        // tiles get progressively smaller with the flat structure, adjust tile size and positions accordingly
        im_tile_w = fm->get_metadata_tag_int(bim::TILE_NUM_X, 0);
        im_tile_w = (int)bim::round<bim::uint64>((double)im_tile_w / pow(2.0, level));
        im_tile_h = fm->get_metadata_tag_int(bim::TILE_NUM_Y, 0);
        im_tile_h = (int)bim::round<bim::uint64>((double)im_tile_h / pow(2.0, level));
        ImageInfo info = fm->sessionGetInfo();
        bim::uint64 w = bim::round<bim::uint64>((double)info.width / pow(2.0, level));
        bim::uint64 h = bim::round<bim::uint64>((double)info.height / pow(2.0, level));
        if (w < tile_size_x && h < tile_size_y)
            return fm->sessionReadLevel(img.imageBitmap(), page, (bim::uint)level);
    }

    // if the requested block is exactly the size stored in the image, read directly
    if (tile_size_x == im_tile_w && tile_size_y == im_tile_h) {
        bim::uint64 xid = x1 / im_tile_w;
        bim::uint64 yid = y1 / im_tile_h;
        return fm->sessionReadTile(img.imageBitmap(), page, xid, yid, requested_level) == 0;
    }

    // if file tile size is different from the requested size, compose output tile from stored tiles
    double xt1 = x1 / (double)im_tile_w;
    double xt2 = x2 / (double)im_tile_w;
    double yt1 = y1 / (double)im_tile_h;
    double yt2 = y2 / (double)im_tile_h;

    int xid1 = (int)floor(xt1);
    int nx = bim::max<int>((int)ceil(xt2) - xid1, 1);
    int yid1 = (int)floor(yt1);
    int ny = bim::max<int>((int)ceil(yt2) - yid1, 1);

    // read all required tiles into the temp image
    Image temp;
    Image tile;
    for (uint64 x = 0; x < nx; ++x) {
        for (uint64 y = 0; y < ny; ++y) {
            if (fm->sessionReadTile(tile.imageBitmap(), page, x + xid1, y + yid1, requested_level) == 0) {
                if (temp.isEmpty())
                    temp.alloc(nx * im_tile_w, ny * im_tile_h, tile.samples(), tile.depth(), tile.pixelType());
                temp.setROI(x * im_tile_w, y * im_tile_h, tile);
            }
        } // y
    }     // x

    // compute level image size
    double scale = this->scales[requested_level];
    bim::uint level_w = bim::round<bim::uint>((double)this->full_width * scale);
    bim::uint level_h = bim::round<bim::uint>((double)this->full_height * scale);

    // extract requested ROI
    int dx = static_cast<int>(x1 - (xid1 * im_tile_w));
    int dy = static_cast<int>(y1 - (yid1 * im_tile_h));
    int szx = std::min<int>((int)tile_size_x, static_cast<int>(level_w - x1));
    int szy = std::min<int>((int)tile_size_y, static_cast<int>(level_h - y1));
    if (szx > 0 && szy > 0)
        img = temp.ROI(dx, dy, szx, szy);
    else
        img = Image();
    return true;
}

bool ImageProxy::readRegion(Image &img, uint page,
                            const uint64 x1, const uint64 y1,
                            const uint64 x2, const uint64 y2,
                            const double scale)
{
    if (x1 > x2 || y1 > y2) return false;

    int requested_level = getImageLevel(scale);
    if (requested_level >= 0) return this->readRegion(img, page, x1, y1, x2, y2, requested_level, false);

    // exact level is not present in the image, need to read and rescale
    requested_level = this->getClosestLevel(scale);
    if (requested_level < 0) return false;
    double requested_scale = this->scales[requested_level];
    const uint64 x1s = bim::round<uint64>((x1 / scale) * requested_scale);
    const uint64 y1s = bim::round<uint64>((y1 / scale) * requested_scale);
    const uint64 x2s = bim::round<uint64>((x2 / scale) * requested_scale);
    const uint64 y2s = bim::round<uint64>((y2 / scale) * requested_scale);
    if (this->readRegion(img, page, x1s, y1s, x2s, y2s, requested_level, false) != true) return false;

    // add image size restriction
    const uint64 w = bim::round<uint64>(this->full_width * scale);
    const uint64 h = bim::round<uint64>(this->full_height * scale);
    const uint64 tw = std::min(x2 - x1 + 1, w - x1);
    const uint64 th = std::min(y2 - y1 + 1, h - y1);
    img = img.resample(tw, th, bim::Image::ResizeMethod::szBiCubic);
    return true;
}
