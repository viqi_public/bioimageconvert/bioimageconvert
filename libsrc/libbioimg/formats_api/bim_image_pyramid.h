/*******************************************************************************

  Defines Image Pyramid Class

  Programmer: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
    03/23/2004 18:03 - First creation

  ver: 1

*******************************************************************************/

#ifndef BIM_IMAGE_PYRAMID_H
#define BIM_IMAGE_PYRAMID_H

#include <vector>

#include "bim_image.h"
#include "bim_image_stack.h"

namespace bim {

const uint64 d_min_image_size = 50;

class ImagePyramid : public ImageStack {
public:
    ImagePyramid();
    ImagePyramid(const Image &img, uint64 v = 0);
    ~ImagePyramid();

    void init();

    // return true if the level exists
    bool levelDown() { return positionNext(); }
    bool levelUp() { return positionPrev(); }
    bool level0() { return position0(); }
    bool levelLowest() { return positionLast(); }
    bool levelSet(uint64 l) { return positionSet(l); }
    uint64 levelCurrent() const { return positionCurrent(); }
    uint64 levelClosestTop(uint64 w, uint64 h) const;
    uint64 levelClosestBottom(uint64 w, uint64 h) const;
    uint64 numberLevels() const { return numberPlanes(); }

    void setMinImageSize(uint64 v) {
        min_width = v;
        min_height = v;
    }

    void createFrom(const Image &img);
    bool fromFile(const char *fileName, int page, XConf *c = NULL);
    bool fromFile(const std::string &fileName, int page, XConf *c = NULL) { return fromFile(fileName.c_str(), page, c); }

protected:
    uint64 min_width;
    uint64 min_height;

protected:
    // prohibit copy-constructor
    ImagePyramid(const ImagePyramid &);
    bool fromFile(const char * /*fileName*/) { return false; }
    bool fromFile(const std::string & /*fileName*/) { return false; }
};

} // namespace bim

#endif //BIM_IMAGE_PYRAMID_H
