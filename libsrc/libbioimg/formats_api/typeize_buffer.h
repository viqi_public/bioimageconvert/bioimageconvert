/*******************************************************************************

  Buffer convertions unit: coverts pixels into typed pixels

  Author: Dima Fedorov Levit <dimin@dimin.net> <http://www.dimin.net/>

  History:
    2007-07-06 17:02 - First creation

  ver: 1

*******************************************************************************/

#ifndef TYPEIZE_BUFFER_H
#define TYPEIZE_BUFFER_H

namespace bim {

// convert a byte-field with only binary values to a bit-field:
void cnv_buffer_8to1bit(unsigned char *b1, const unsigned char *b8, const size_t width_pixels);

void cnv_buffer_8to4bit(unsigned char *b4, const unsigned char *b8, const size_t width_pixels);

void cnv_buffer_16to12bit(unsigned char *b12, const unsigned char *b16, const size_t width_pixels);

// convert a bit-field to a bit-field with only binary values:
void cnv_buffer_1to8bit(unsigned char *b8, const unsigned char *b1, const size_t width_pixels);

void cnv_buffer_4to8bit(unsigned char *b8, const unsigned char *b4, const size_t width_pixels);

void cnv_buffer_12to16bit(unsigned char *b16, const unsigned char *b12, const size_t width_pixels);

} // namespace bim

#endif
