/*******************************************************************************

Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

History:
10/13/2006 16:00 - First creation

Ver : 4
*******************************************************************************/

#include <cmath>
#include <cstdio>
#include <cstring>

#include <vector>
#include <algorithm>
#include <limits>
#include <iostream>
#include <fstream>

#if defined(max)
#undef max
#endif

#if defined(min)
#undef min
#endif

using namespace bim;

template <typename T>
void kmeans(const std::vector<T> &v, std::vector<double> &centers, std::vector<int> &classes) {
    int N = v.size();
    int K = centers.size();
    if (N < K) return;

    // init all samples to same class
    classes.resize(0);
    classes.resize(N, 0);
    
    // init centroids
    for (int i = 0; i < K; ++i) {
        centers[i] = v[i];
    }
    
    bool changed = true;
    while (changed) {
        changed = false;

        // compute distances
        //#pragma omp parallel for default(shared)
        for (int x = 0; x < N; ++x) {
            double d = std::numeric_limits<double>::max();
            int c = 0;
            for (int i = 0; i < K; ++i) {
                double dd = fabs(centers[i] - v[x]);
                if (dd < d) {
                    d = dd;
                    c = i;
                }
            }
            if (c != classes[x]) {
                classes[x] = c;
                changed = true;
            }
        }

        // recompute centroids
        if (changed)
        for (int i = 0; i < K; ++i) {
            double sum = 0, n=0;
            for (int x = 0; x < N; ++x) {
                if (classes[x] == i) {
                    sum += v[x];
                    ++n;
                }
            }
            centers[i] = sum / n;
        }
    }
}
