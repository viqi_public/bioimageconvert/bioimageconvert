/*******************************************************************************

  Tests for bim::TagMap

  This file is part of BioImageConvert.

  *******************************************************************************/

#include <typeize_buffer.h>

#include <gtest/gtest.h>

#include <cmath>
#include <ctime>

TEST(TypeizeBufferTest, cnv_buffer_1to8bit_forward_backward) {
    srand((unsigned)time(0));
    const size_t width = 1 + rand() % 255;

    const size_t input_depth = 8;
    const size_t output_depth = 1;
    const double size_ratio = static_cast<double>(output_depth) / static_cast<double>(input_depth);


    const size_t bytesize_b8 = width;
    uint8_t *b8_in = new uint8_t[bytesize_b8];
    uint8_t *b8_out = new uint8_t[bytesize_b8];

    const size_t bytesize_b1 = static_cast<size_t>(std::ceil(static_cast<double>(width) * size_ratio));
    uint8_t *b1 = new uint8_t[bytesize_b1];

    // Fill the byte field with random bool values:
    for (size_t idx = 0; idx < bytesize_b8; ++idx) {
        b8_in[idx] = static_cast<uint8_t>(rand() % (size_t)std::pow(2, output_depth - 1));
    }

    // Forward conversion:
    bim::cnv_buffer_8to1bit(b1, b8_in, bytesize_b8);

    // Backward conversion:
    bim::cnv_buffer_1to8bit(b8_out, b1, bytesize_b8);

    // compare the result:
    for (size_t idx = 0; idx < bytesize_b8; ++idx) {
        ASSERT_EQ(b8_in[idx], b8_out[idx]);
    }

    delete[] b8_in;
    delete[] b8_out;
    delete[] b1;
}

TEST(TypeizeBufferTest, cnv_buffer_4to8bit_forward_backward) {
    srand((unsigned)time(0));
    const size_t width = 1 + rand() % 255;

    const size_t input_depth = 8;
    const size_t output_depth = 4;
    const double size_ratio = static_cast<double>(output_depth) / static_cast<double>(input_depth);


    const size_t bytesize_b8 = width;
    uint8_t *b8_in = new uint8_t[bytesize_b8];
    uint8_t *b8_out = new uint8_t[bytesize_b8];

    const size_t bytesize_b4 = static_cast<size_t>(std::ceil(static_cast<double>(width) * size_ratio));
    uint8_t *b4 = new uint8_t[bytesize_b4];

    // Fill the byte field with random bool values:
    for (size_t idx = 0; idx < bytesize_b8; ++idx) {
        b8_in[idx] = static_cast<uint8_t>(rand() % (size_t)std::pow(2, output_depth - 1));
    }

    // Forward conversion:
    bim::cnv_buffer_8to4bit(b4, b8_in, bytesize_b8);

    // Backward conversion:
    bim::cnv_buffer_4to8bit(b8_out, b4, bytesize_b8);

    // compare the result:
    for (size_t idx = 0; idx < bytesize_b8; ++idx) {
        ASSERT_EQ(b8_in[idx], b8_out[idx]);
    }

    delete[] b8_in;
    delete[] b8_out;
    delete[] b4;
}

#if false
// NOTE: Currently this conversion does not return the same result.
// The implementations for cnv_buffer_16to12bit() and cnv_buffer_12to16bit()
// assume a different memory layout.
TEST(TypeizeBufferTest, cnv_buffer_12to16bit_forward_backward) {
    srand((unsigned)time(0));
    const size_t width = 1 + rand() % 255;

    const size_t input_depth = 16;
    const size_t output_depth = 12;
    const double size_ratio = static_cast<double>(output_depth) / static_cast<double>(input_depth);


    const size_t bytesize_b16 = width;
    uint16_t *b16_in = new uint16_t[bytesize_b16];
    uint16_t *b16_out = new uint16_t[bytesize_b16];

    const size_t bytesize_b12 = static_cast<size_t>(std::ceil(static_cast<double>(width) * size_ratio));
    uint16_t *b12 = new uint16_t[bytesize_b12];

    // Fill the byte field with random bool values:
    for (size_t idx = 0; idx < bytesize_b16; ++idx) {
        b16_in[idx] = static_cast<uint16_t>(rand() % (size_t)std::pow(2, output_depth - 1));
    }

    // Forward conversion:
    bim::cnv_buffer_16to12bit(reinterpret_cast<uint8_t *>(b12), reinterpret_cast<uint8_t *>(b16_in), bytesize_b16);

    // Backward conversion:
    bim::cnv_buffer_12to16bit(reinterpret_cast<uint8_t *>(b16_out), reinterpret_cast<uint8_t *>(b12), bytesize_b16);

    // compare the result:
    for (size_t idx = 0; idx < bytesize_b16; ++idx) {
        ASSERT_EQ(b16_in[idx], b16_out[idx]);
    }

    delete[] b16_in;
    delete[] b16_out;
    delete[] b12;
}
#endif