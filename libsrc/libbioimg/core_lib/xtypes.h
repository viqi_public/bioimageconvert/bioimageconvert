/*****************************************************************************
 Base typing and type conversion definitions

 DEFINITION

 Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>
 Copyright (c) 2006 Vision Research Lab, UCSB <http://vision.ece.ucsb.edu>

 History:
   04/19/2006 16:20 - First creation

 Ver : 1
*****************************************************************************/

#ifndef BIM_XTYPES
#define BIM_XTYPES
//#pragma message(">>>>>  xtypes: included types and type conversion utils")

#include <cmath>
#include <cstdint>
#include <limits>
#include <stdexcept>
#include <sys/types.h>
#include <tuple>
#include <vector>
#include <algorithm>

namespace bim {

//------------------------------------------------------------------------------
// first define type macros, you can define them as needed, OS specific
//------------------------------------------------------------------------------
/*
Datatype  LP64  ILP64 LLP64 ILP32 LP32
char       8     8     8      8      8
short     16    16    16     16     16
_int32          32
int       32    64    32     32     16
long      64    64    32     32     32
long long       64
pointer   64    64    64     32     32
*/

#ifndef _BIM_TYPEDEFS_
#define _BIM_TYPEDEFS_

// system types
typedef unsigned char uchar;
typedef unsigned int uint;

// sized types
typedef signed char int8;
typedef unsigned char uint8;
typedef short int16;
typedef unsigned short uint16;

#if defined(WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64) || defined(_MSVC)
typedef __int32 int32;
typedef __int64 int64;
typedef unsigned __int32 uint32;
typedef unsigned __int64 uint64;
#else
typedef int32_t int32;
typedef int64_t int64;
typedef uint32_t uint32;
typedef uint64_t uint64;
#endif

typedef float float32;
typedef double float64;
typedef long double float80;

#endif // _BIM_TYPEDEFS_


#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#if ((defined(WIN32) || defined(WIN64) || defined(_WIN32) || defined(_WIN64) || defined(_MSVC)) && !defined(__MINGW32__))
#ifndef BIM_WIN
#define BIM_WIN
#endif
#endif

#if defined(_MSC_VER)
#define BIM_DEF(a) = a
#else
#define BIM_DEF(a)
#endif

template<typename T>
constexpr T PI = T(3.1415926535897932385);

//------------------------------------------------------------------------------
// BIG_ENDIAN is for SPARC, Motorola, IBM and LITTLE_ENDIAN for intel type
//------------------------------------------------------------------------------
const static int bimOne = 1;
const static int bigendian = (*(const char *)&bimOne == 0);
const static double Pi = 3.14159265358979323846264338327950288419716939937510;

#if defined(_MSC_VER)
#define BIM_RESTRICT __restrict
#define BIM_ALIGN(a) __declspec(align(a))
#elif defined(__GNUG__)
#define BIM_RESTRICT __restrict__
#define BIM_ALIGN(a) __attribute__((__aligned__(a)))
#else
#define BIM_RESTRICT
#define BIM_ALIGN(a)
#endif
#define BIM_ALIGN_INT8 BIM_ALIGN(8)
#define BIM_ALIGN_INT16 BIM_ALIGN(16)
#define BIM_ALIGN_INT32 BIM_ALIGN(32)
#define BIM_ALIGN_INT64 BIM_ALIGN(64)
#define BIM_ALIGN_FLOAT BIM_ALIGN(32)
#define BIM_ALIGN_DOUBLE BIM_ALIGN(64)

#define BIM_OMP_FOR1 3000
#define BIM_OMP_FOR2 200
#define BIM_OMP_SCHEDULE schedule(runtime)

#if __FAST_MATH__

template<typename T>
inline bool isnan(T value) {
    throw std::runtime_error("isnan not implemented for this type under g++ fast-math");
    //is_ieee754_nan( value );
}

template<>
inline bool isnan(float value) {
    const bim::uint32 u = *(bim::uint32 *)&value;
    return (u & 0x7F800000) == 0x7F800000 && (u & 0x7FFFFF);
}

template<>
inline bool isnan(double value) {
    const bim::uint64 u = *(bim::uint64 *)&value;
    return (u & 0x7FF0000000000000ULL) == 0x7FF0000000000000ULL && (u & 0xFFFFFFFFFFFFFULL);
}

#else // not -fast-math

template<typename T>
inline bool isnan(T value) {
#if __cplusplus <= 199711L
    return value != value;
#else
    return std::isnan(value);
#endif
}

#endif


//------------------------------------------------------------------------------
// SWAP types for big/small endian conversions
//------------------------------------------------------------------------------

inline void swapShort(uint16 *wp) {
    uchar *cp = (uchar *)wp;
    uchar t;
    t = cp[1];
    cp[1] = cp[0];
    cp[0] = t;
}

inline void swapLong(uint32 *lp) {
    uchar *cp = (uchar *)lp;
    uchar t;
    t = cp[3];
    cp[3] = cp[0];
    cp[0] = t;
    t = cp[2];
    cp[2] = cp[1];
    cp[1] = t;
}

void swapArrayOfShort(uint16 *wp, uint64 n);
void swapArrayOfLong(uint32 *lp, uint64 n);

inline void swapFloat(float32 *lp) {
    uchar *cp = (uchar *)lp;
    uchar t;
    t = cp[3];
    cp[3] = cp[0];
    cp[0] = t;
    t = cp[2];
    cp[2] = cp[1];
    cp[1] = t;
}

void swapDouble(float64 *dp);
void swapArrayOfFloat(float32 *dp, uint64 n);
void swapArrayOfDouble(float64 *dp, uint64 n);

//------------------------------------------------------------------------------
// min/max
//------------------------------------------------------------------------------

template<typename T>
inline const T &min(const T &a, const T &b) {
    return (a < b) ? a : b;
}

template<typename T>
inline const T &max(const T &a, const T &b) {
    return (a > b) ? a : b;
}

//------------------------------------------------------------------------------
// min/max for arrays
//------------------------------------------------------------------------------

template<typename T>
const T max(const T *a, size_t size) {
    T val = a[0];
    for (unsigned int i = 0; i < size; ++i)
        val = (val < a[i]) ? a[i] : val;
    return val;
}

template<typename T>
unsigned int maxix(const T *a, size_t size) {
    unsigned int i, ix;
    T val = a[0];
    ix = 0;
    for (i = 0; i < size; ++i)
        if (val < a[i]) {
            val = a[i];
            ix = i;
        }
    return ix;
}

template<typename T>
const T min(const T *a, size_t size) {
    T val = a[0];
    for (unsigned int i = 0; i < size; ++i)
        val = (val > a[i]) ? a[i] : val;
    return val;
}

template<typename T>
unsigned int minix(const T *a, size_t size) {
    unsigned int i, ix;
    T val = a[0];
    ix = 0;
    for (i = 0; i < size; ++i)
        if (val > a[i]) {
            val = a[i];
            ix = i;
        }
    return ix;
}

//------------------------------------------------------------------------------
// utils
//------------------------------------------------------------------------------

template<typename T>
inline T highest() {
    return std::numeric_limits<T>::max();
}

#if __cplusplus>=201103L // c++11 version
template<typename T>
inline T lowest() {
    return std::numeric_limits<T>::lowest();
}
#else
template<typename T>
inline T lowest() {
    //return std::numeric_limits<T>::is_integer ? std::numeric_limits<T>::min() : -std::numeric_limits<T>::max();
    return std::numeric_limits<T>::min();
}

template<>
inline float lowest() {
    return -std::numeric_limits<float>::max();
}

template<>
inline double lowest() {
    return -std::numeric_limits<double>::max();
}

#endif

#if __cplusplus>=201103L // c++11 version
template<typename To, typename Ti>
inline To trim(Ti val,
    To min = std::numeric_limits<To>::lowest(),
    To max = std::numeric_limits<To>::max()) {
    if (val < min) return min;
    if (val > max) return max;
    return (To)val;
}
#else
template<typename To, typename Ti>
inline To trim(Ti val, To min = bim::lowest<To>(), To max = std::numeric_limits<To>::max()) {
    if (val < (Ti)min) return min;
    if (val > (Ti)max) return max;
    return (To)val;
}
#endif

//------------------------------------------------------------------------------
// little math
//------------------------------------------------------------------------------

template<typename T>
inline T round(double x) {
    return (T)floor(x + 0.5);
}

template<typename T>
inline T round(float x) {
    return (T)floor(x + 0.5f);
}

// round number n to d decimal points
template<typename T>
inline T round(double x, int d) {
    return (T)floor(x * pow(10., d) + .5) / pow(10., d);
}

template<typename T>
T power(T base, int index) {

    if (index < 0) {
        return 1.0 / pow(base, -index);
    } else
        return pow(base, index);
}

template<typename T>
inline T log2(T n) {
    return (T)(log((double)n) / log(2.0));
}

/*
template <typename T>
inline T ln(T n) {
  return (T) (log((double)n)/log(E));
}
*/

/*
float invSqrt (float x) {
  float xhalf = 0.5f*x;
  int i = *(int*)&x;
  i = 0x5f3759df - (i >> 1);
  x = *(float*)&i;
  x = x*(1.5f - xhalf*x*x);
  return x;
}
*/

//------------------------------------------------------------------------------
// ops for vectors
//------------------------------------------------------------------------------

template<typename T>
const T max(const std::vector<T> &a) {
    T val = a[0];
    const T *pa = &a[0];
    for (unsigned int i = 0; i < a.size(); ++i)
        val = (val < pa[i]) ? pa[i] : val;
    return val;
}

template<typename T>
const T min(const std::vector<T> &a) {
    T val = a[0];
    const T *pa = &a[0];
    for (unsigned int i = 0; i < a.size(); ++i)
        val = (val > pa[i]) ? pa[i] : val;
    return val;
}

template<typename T>
unsigned int argmax(const std::vector<T> &a) {
    T val = a[0];
    const T *pa = &a[0];
    unsigned int ix = 0;
    for (unsigned int i = 0; i < a.size(); ++i)
        if (val < pa[i]) {
            val = pa[i];
            ix = i;
        }
    return ix;
}

template<typename T>
unsigned int argmin(const std::vector<T> &a) {
    T val = a[0];
    const T *pa = &a[0];
    unsigned int ix = 0;
    for (unsigned int i = 0; i < a.size(); ++i)
        if (val > pa[i]) {
            val = pa[i];
            ix = i;
        }
    return ix;
}

template<typename T>
const double sum(const std::vector<T> &a) {
    double val = 0;
    const T *pa = &a[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        val += pa[i];
    }
    return val;
}

template<typename T>
const double mean(const std::vector<T> &a) {
    return (bim::sum<T>(a) * 1.0) / a.size();
};

template<typename T>
const T median(const std::vector<T> &a) {
    size_t pos = a.size() / 2;
    std::vector<T> b = a;
    std::sort(b.begin(), b.end());
    return b[pos];
};

template<typename T>
const double std(const std::vector<T> &a) {
    double avg = bim::mean<T>(a);
    double v = 0;
    const T *pa = &a[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        v += (pa[i] - avg) * (pa[i] - avg);
    }
    return sqrt(v * 1.0 / a.size());
};

template<typename T>
std::vector<T> div(const std::vector<T> &a, const T &v) {
    std::vector<T> b(a.size(), 0);
    const T *pa = &a[0];
    T *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = pa[i] / v;
    }
    return b;
};

template<typename T>
std::vector<T> mul(const std::vector<T> &a, const T &v) {
    std::vector<T> b(a.size(), 0);
    const T *pa = &a[0];
    T *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = pa[i] * v;
    }
    return b;
};

template<typename T>
std::vector<T> add(const std::vector<T> &a, const T &v) {
    std::vector<T> b(a.size(), 0);
    const T *pa = &a[0];
    T *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = pa[i] + v;
    }
    return b;
};

template<typename T>
std::vector<T> sub(const std::vector<T> &a, const T &v) {
    std::vector<T> b(a.size(), 0);
    const T *pa = &a[0];
    T *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = pa[i] - v;
    }
    return b;
};

template<typename T>
std::vector<T> clip(const std::vector<T> &a, const T &vmin, const T &vmax) {
    std::vector<T> b(a.size(), 0);
    const T *pa = &a[0];
    T *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = bim::trim<T>(pa[i], vmin, vmax);
    }
    return b;
};

template<typename T>
std::vector<T> threshold_to(const std::vector<T> &a, const T &vmin, const T &v) {
    std::vector<T> b(a.size(), 0);
    const T *pa = &a[0];
    T *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = pa[i] < vmin ? v : pa[i];
    }
    return b;
};

template<typename T, typename To>
std::vector<To> log10p1(const std::vector<T> &a) {
    std::vector<To> b(a.size(), 0);
    const T *pa = &a[0];
    To *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = log10(static_cast<To>(pa[i]) + 1.0);
    }
    return b;
}

template<typename T, typename To>
std::vector<To> convert_type(const std::vector<T> &a) {
    std::vector<To> b(a.size(), 0);
    const T *pa = &a[0];
    To *pb = &b[0];
    for (unsigned int i = 0; i < a.size(); ++i) {
        pb[i] = static_cast<To>(pa[i]);
    }
    return b;
}

// ----------------------------------------------------------------------------------
// 1D convolution for small vectors
// ----------------------------------------------------------------------------------

template<typename T, typename To>
std::vector<To> convolution_func(const std::vector<T> &a, const std::vector<double> &k) {
    unsigned int a_sz = (unsigned int)a.size();
    unsigned int k_sz = (unsigned int)k.size();
    int k_sz_h = bim::round<int>(static_cast<double>(k_sz) / 2.0) - 1;
    double w = sum<double>(k);
    std::vector<To> b(a.size(), 0);
    std::vector<To> t(a.size() + k_sz * 2, 0);

    // copy array content
    unsigned int j = k_sz;
    for (unsigned int i = 0; i < a_sz; ++i) {
        t[j] = a[i];
        ++j;
    }

    // mirror front
    j = k_sz - 1;
    for (unsigned int i = 0; i < k_sz; ++i) {
        t[j] = a[i];
        --j;
    }

    // mirror back
    j = a_sz;
    for (int i = a_sz - 1; i > static_cast<int>(a_sz - k_sz); --i) {
        t[j] = a[i];
        ++j;
    }

    // convolve
    j = k_sz;
    for (unsigned int i = 0; i < a_sz; ++i) {
        double v = 0;
        unsigned int m = j - k_sz_h;
        for (unsigned int n = 0; n < k_sz; ++n) {
            v += t[m] * k[n];
            ++m;
        }

        b[i] = static_cast<To>(v / w);
        ++j;
    }

    return b;
}

// ----------------------------------------------------------------------------------
// 1D Gaussian blur for small vectors
// ----------------------------------------------------------------------------------

// Generates a kernel used for the gaussian blur effect.
// @param sz is an odd integer
// @param sigma is the standard deviation used for our gaussian function.
//
// @returns an array with sz^2 number of numbers, all less than or equal
//   to 1. Represents our gaussian blur kernel.

template<typename T>
std::vector<T> generateGaussianKernel(unsigned int sz = 3, float sigma = 1.0) {
    if (!(sz % 2) || floor(sz) != sz || sz < 3) {
        throw std::invalid_argument("The sz must be an odd integer greater than or equal to 3");
    }
    std::vector<T> kernel(sz, 0);

    double twoSigmaSquare = 2.0 * sigma * sigma;
    double centre = (sz - 1.0) / 2.0;
    for (unsigned int j = 0; j < sz; j++) {
        double distance = std::abs(centre - j);
        kernel[j] = (1.0 / sqrt(bim::PI<double> * twoSigmaSquare)) * exp((-1) * (pow(distance, 2) / twoSigmaSquare));
    }

    // Returns the unit vector of the kernel array
    double w = bim::sum<T>(kernel);
    for (unsigned int i = 0; i < kernel.size(); ++i) {
        kernel[i] /= w;
    }
    return kernel;
}

template<typename T, typename To>
std::vector<To> convolution_gaussian(const std::vector<T> &a, unsigned int k_sz, float sigma) {
    if (!(k_sz % 2)) k_sz += 1;
    sigma = sigma || 2.0;
    std::vector<double> k = generateGaussianKernel<double>(k_sz, sigma);
    return bim::convolution_func<T, To>(a, k);
}

template<typename T, typename To>
std::vector<To> medfilt(const std::vector<T> &a, unsigned int k_sz = 3) {
    unsigned int c = (k_sz - 1) / 2;
    size_t a_sz = a.size();
    std::vector<To> b(a.size(), 0);
    std::vector<To> t(a_sz + k_sz, 0);
    std::vector<To> k(k_sz, 0);

    // mirror front
    int j = k_sz - 1;
    for (unsigned int i = 0; i < k_sz; ++i) {
        t[j] = a[i];
        --j;
    }

    // mirror back
    j = (int)a_sz;
    for (int i = (int)a_sz - 1; i > static_cast<int>(a_sz - k_sz); --i) {
        t[j] = a[i];
        ++j;
    }

    // copy array content
    for (unsigned int j = c, i = 0; i < a_sz; ++i, ++j) {
        t[j] = a[i];
    }

    // compute median
    for (unsigned int i = 0; i < a_sz; ++i) {
        for (unsigned int j = 0; j < k_sz; ++j) {
            k[j] = t[i + j];
        }
        std::sort(k.begin(), k.end());
        b[i] = k[c];
    }

    return b;
};

//------------------------------------------------------------------------------
// coordinates
//------------------------------------------------------------------------------

typedef std::tuple<int, int> Coord2i;
typedef std::tuple<int, int, int> Coord3i;
typedef std::tuple<int, int, int, int> Coord4i;
typedef std::tuple<int, int, int, int, int> Coord5i;
typedef std::tuple<int, int, int, int, int, int> Coord6i;

typedef std::tuple<double, double> Coord2f;
typedef std::tuple<double, double, double> Coord3f;
typedef std::tuple<double, double, double, double> Coord4f;
typedef std::tuple<double, double, double, double, double> Coord5f;
typedef std::tuple<double, double, double, double, double, double> Coord6f;

//------------------------------------------------------------------------------
// bounding boxes
//------------------------------------------------------------------------------

template<typename T>
inline bool axis_intersect(T x1, T w1, T x2, T w2) {
    return x1 < x2 + w2 && x2 < x1 + w1;
}

template<typename T>
class Bbox {
public:
    std::vector<T> pos;
    std::vector<T> size;
    int ndims = 0;

public:
    Bbox(int ndims = 0) {
        this->set(ndims);
    }

    Bbox(T x, T y, T w, T h) {
        this->set(x, y, w, h);
    }

    Bbox(T x, T y, T z, T w, T h, T d) {
        this->set(x, y, z, w, h, d);
    }

    Bbox(const std::vector<T> &pos, const std::vector<T> &size) {
        this->set(pos, size);
    }

    Bbox(const std::vector<T> &v, bool from_vertices = false) {
        this->set(v, from_vertices);
    }

    Bbox(const T *pos, const T *size, int ndims) {
        this->set(pos, size, ndims);
    }

    Bbox(const T *v, int ndims, bool from_vertices = false) {
        this->set(v, ndims, from_vertices);
    }

    // setters

    void set(int ndims = 0) {
        this->ndims = ndims;
        //int szbytes = this->ndims * sizeof(T);
        this->pos.resize(ndims);
        this->size.resize(ndims);
    }

    void set(T x, T y, T w, T h) {
        this->set(2);
        this->pos[0] = x;
        this->pos[1] = y;
        this->size[0] = w;
        this->size[1] = h;
    }

    void set(T x, T y, T z, T w, T h, T d) {
        this->set(3);
        this->pos[0] = x;
        this->pos[1] = y;
        this->pos[2] = z;
        this->size[0] = w;
        this->size[1] = h;
        this->size[2] = d;
    }

    void set(const std::vector<T> &pos, const std::vector<T> &size) {
        this->ndims = pos.size();
        this->pos = pos;
        this->size = size;
    }

    void set(const std::vector<T> &v, bool from_vertices = false) {
        this->set(static_cast<T>(v.size() / 2));
        int szbytes = this->ndims * sizeof(T);
        memcpy(&this->pos[0], &v[0], szbytes);
        memcpy(&this->size[0], &v[this->ndims], szbytes);
        if (from_vertices == true)
            this->vertex_to_size();
    }

    void set(const T *pos, const T *size, int ndims) {
        this->set(ndims);
        int szbytes = this->ndims * sizeof(T);
        memcpy(&this->pos[0], pos, szbytes);
        memcpy(&this->size[0], size, szbytes);
    }

    void set(const T *v, int ndims, bool from_vertices = false) {
        this->set(ndims);
        int szbytes = this->ndims * sizeof(T);
        memcpy(&this->pos[0], v, szbytes);
        memcpy(&this->size[0], v + this->ndims, szbytes);
        if (from_vertices == true)
            this->vertex_to_size();
    }

    // getters

    int num_dims() const {
        return (int)this->ndims;
    }

    bool is_intersecting(const Bbox<T> &bbox) const {
        int ndims = std::min(this->ndims, bbox.ndims);
        bool intersect = true;
        for (int i = 0; i < ndims; ++i) {
            intersect = intersect && axis_intersect(this->pos[i], this->size[i], bbox.pos[i], bbox.size[i]);
        }
        return intersect;
    }

    std::vector<T> getBBox() const {
        int szbytes = this->ndims * sizeof(T);
        std::vector<T> v(this->ndims * 2, 0);
        memcpy(&v[0], &this->pos[0], szbytes);
        memcpy(&v[this->ndims], &this->size[0], szbytes);
        return v;
    }

    std::vector<T> getPos() const {
        return this->pos;
    }

    std::vector<T> getSize() const {
        return this->size;
    }

    T getX() const {
        if (this->ndims < 2) return 0;
        return this->pos[1];
    }

    T getY() const {
        if (this->ndims < 1) return 0;
        return this->pos[0];
    }

    T getZ() const {
        if (this->ndims < 3) return 0;
        return this->pos[2];
    }

    T getWidth() const {
        if (this->ndims < 2) return 0;
        return this->size[1];
    }

    T getHeight() const {
        if (this->ndims < 1) return 0;
        return this->size[0];
    }

    T getDepth() const {
        if (this->ndims < 3) return 0;
        return this->size[2];
    }

    T getX2() const {
        if (this->ndims < 2) return 0;
        return this->pos[1] + this->size[1] - 1;
    }

    T getY2() const {
        if (this->ndims < 1) return 0;
        return this->pos[0] + this->size[0] - 1;
    }

    T getZ2() const {
        if (this->ndims < 3) return 0;
        return this->pos[2] + this->size[2] - 1;
    }

    // modifiers
    void offset_by(const T &d) {
        for (int i = 0; i < this->ndims; ++i) {
            this->pos[i] += d;
        }
    }

    void resize_by(const T &d) {
        for (int i = 0; i < this->ndims; ++i) {
            this->size[i] += d;
        }
    }


private:

    void vertex_to_size() {
        for (size_t i = 0; i < this->size.size(); ++i) {
            this->size[i] = static_cast<T>(std::fabs(this->size[i] - this->pos[i]));
        }

    }
};

} // namespace bim

#endif // BIM_XTYPES
