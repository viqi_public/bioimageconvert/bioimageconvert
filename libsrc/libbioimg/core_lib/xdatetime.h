/*****************************************************************************
 Date/Time definition and conversions

 Copyright (c) 2019-2021, ViQI Inc

 Author: Dmitry Fedorov <mailto:dima@viqi.org> <http://www.viqi.org/>

 History:
 2019-05-23 11:50:40 - First creation

 Ver : 1
*****************************************************************************/

/*
Parsing format:

The format string consists of zero or more conversion specifiers, whitespace characters, and ordinary characters (except %). 
Each ordinary character is expected to match one character in the input stream in case-insensitive comparison. 
Each whitespace character matches arbitrary whitespace in the input string. Each conversion specification begins with % character, 
optionally followed by E or O modifier (ignored if unsupported by the locale), followed by the character that determines the behavior of the specifier. 
The format specifiers match the POSIX function strptime()

Conversion
specifier	Explanation	Writes to fields
%	matches a literal %. The full conversion specification must be %%.	(none)
t	matches any whitespace.	(none)
n	matches any whitespace.	(none)

Year
Y	parses full year as a 4 digit decimal number, leading zeroes permitted but not required	tm_year
EY	parses year in the alternative representation, e.g. (year Heisei 23) which writes 2011 to tm_year in ja_JP locale	tm_year
y	parses last 2 digits of year as a decimal number. Range [69,99] results in values 1969 to 1999, range [00,68] results in 2000-2068	tm_year
Oy	parses last 2 digits of year using the alternative numeric system, e.g. is parsed as 11 in ja_JP locale	tm_year
Ey	parses year as offset from locale's alternative calendar period %EC	tm_year
C	parses the first 2 digits of year as a decimal number (range [00,99])	tm_year
EC	parses the name of the base year (period) in the locale's alternative representation, e.g. (Heisei era) in ja_JP	tm_year

Month
b	parses the month name, either full or abbreviated, e.g. Oct	tm_mon
h	synonym of b	tm_mon
B	synonym of b	tm_mon
m	parses the month as a decimal number (range [01,12]), leading zeroes permitted but not required	tm_mon
Om	parses the month using the alternative numeric system, e.g. parses as 12 in ja_JP locale	tm_mon

Week
U	parses the week of the year as a decimal number (Sunday is the first day of the week) (range [00,53]), leading zeroes permitted but not required	tm_year, tm_wday, tm_yday
OU	parses the week of the year, as by %U, using the alternative numeric system, e.g. parses as 52 in ja_JP locale	tm_year, tm_wday, tm_yday
W	parses the week of the year as a decimal number (Monday is the first day of the week) (range [00,53]), leading zeroes permitted but not required	tm_year, tm_wday, tm_yday
OW	parses the week of the year, as by %W, using the alternative numeric system, e.g. parses as 52 in ja_JP locale	tm_year, tm_wday, tm_yday

Day of the year/month
j	parses day of the year as a decimal number (range [001,366]), leading zeroes permitted but not required	tm_yday
d	parses the day of the month as a decimal number (range [01,31]), leading zeroes permitted but not required	tm_mday
Od	parses the day of the month using the alternative numeric system, e.g parses as 27 in ja_JP locale, leading zeroes permitted but not required	tm_mday
e	synonym of d	tm_mday
Oe	synonym of Od	tm_mday

Day of the week
a	parses the name of the day of the week, either full or abbreviated, e.g. Fri	tm_wday
A	synonym of a	tm_wday
w	parses weekday as a decimal number, where Sunday is 0 (range [0-6])	tm_wday
Ow	parses weekday as a decimal number, where Sunday is 0, using the alternative numeric system, e.g. parses as 2 in ja_JP locale	tm_wday

Hour, minute, second
H	parses the hour as a decimal number, 24 hour clock (range [00-23]), leading zeroes permitted but not required	tm_hour
OH	parses hour from 24-hour clock using the alternative numeric system, e.g. parses as 18 in ja_JP locale	tm_hour
I	parses hour as a decimal number, 12 hour clock (range [01,12]), leading zeroes permitted but not required	tm_hour
OI	parses hour from 12-hour clock using the alternative numeric system, e.g. reads as 06 in ja_JP locale	tm_hour
M	parses minute as a decimal number (range [00,59]), leading zeroes permitted but not required	tm_min
OM	parses minute using the alternative numeric system, e.g. parses as 25 in ja_JP locale	tm_min
S	parses second as a decimal number (range [00,60]), leading zeroes permitted but not required	tm_sec
OS	parses second using the alternative numeric system, e.g. parses as 24 in ja_JP locale	tm_sec

Other
c	parses the locale's standard date and time string format, e.g. Sun Oct 17 04:41:13 2010 (locale dependent)	all
Ec	parses the locale's alternative date and time string format, e.g. expecting (year Heisei 23) instead of 2011 (year 2011) in ja_JP locale	all
x	parses the locale's standard date representation	all
Ex	parses the locale's alternative date representation, e.g. expecting (year Heisei 23) instead of 2011 (year 2011) in ja_JP locale	all
X	parses the locale's standard time representation	all
EX	parses the locale's alternative time representation	all
D	equivalent to "%m / %d / %y "	tm_mon, tm_mday, tm_year
r	parses locale's standard 12-hour clock time (in POSIX, "%I : %M : %S %p")	tm_hour, tm_min, tm_sec
R	equivalent to "%H : %M"	tm_hour, tm_min
T	equivalent to "%H : %M : %S"	tm_hour, tm_min, tm_sec
p	parses the locale's equivalent of a.m. or p.m.	tm_hour

BIM extensions:
q   finds and skips the milliseconds block starting with a dot and containing a set of numbers: .040
*/

#ifndef BIM_DATETIME
#define BIM_DATETIME

#include <cmath>
#include <cstdint>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "xstring.h"

namespace bim {

//------------------------------------------------------------------------------
// color
//------------------------------------------------------------------------------


class DateTime {
public:
    std::tm t = { 0 };

public:
    bool operator==(DateTime &t2) const {
        return std::difftime(std::mktime((std::tm *)&this->t), std::mktime((std::tm *)&t2.t)) == 0;
    }
    bool operator>=(DateTime &t2) const { 
        return std::difftime(std::mktime((std::tm *)&this->t), std::mktime((std::tm *)&t2.t)) > 0; 
    }
    bool operator<=(DateTime &t2) const { 
        return std::difftime(std::mktime((std::tm *)&this->t), std::mktime((std::tm *)&t2.t)) < 0; 
    }

public:
    const std::vector<xstring> months_short = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    const std::vector<xstring> months_long = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

    const std::vector<xstring> weekdays_short = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
    const std::vector<xstring> weekdays_long = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

    const std::vector<xstring> daytime = { "AM", "PM" };

public:
    DateTime() {
        /*this->t.tm_year = 0;
        this->t.tm_mon = 0;
        this->t.tm_mday = 0;

        this->t.tm_hour = 0;
        this->t.tm_min = 0;
        this->t.tm_sec = 0;*/
    }

    DateTime(const std::tm &timeinfo) {
        this->t = timeinfo;
    }

    DateTime(time_t rawtime) {
        this->t = DateTime::from_time(rawtime).t;
    }

    inline int getYear() const {
        return this->t.tm_year;
    }

    inline int getMonth() const {
        return this->t.tm_mon;
    }

    inline int getDay() const {
        return this->t.tm_mday;
    }

    inline int getHour() const {
        return this->t.tm_hour;
    }

    inline int getMinute() const {
        return this->t.tm_min;
    }

    inline int getSecond() const {
        return this->t.tm_sec;
    }

    inline bool isValid() const {
        if (this->t.tm_mon < 0 || this->t.tm_mon > 11) return false;
        if (this->t.tm_mday < 1 || this->t.tm_mday > 31) return false;
        return true;
    }

    bim::xstring to_string(const xstring &format) const {
        if (!this->isValid())
            return "";
        std::ostringstream ss;
        ss << std::put_time(&this->t, format.c_str());
        return ss.str();
    }

    bim::xstring to_string_iso8601() const {
        return this->to_string("%Y-%m-%dT%H:%M:%S");
    }

    bim::xstring to_string_us() const { 
        // 08/28/2006 04:34:47 PM
        return this->to_string("%Y/%m/%d %I:%M:%S %p");
    }

    bim::xstring to_string_us_long() const {
        // 02:27:29 PM Fri Dec 17 2004
        return this->to_string("%I:%M:%S %p %a %b %d %Y");
    }

    time_t asTime() const {
        return mktime((std::tm *) & this->t);
    }

    std::tm asTM() const {
        return this->t;
    }

    void set_date(const xstring &s, const xstring &format = "") {
        // set only the date portion form the string, use the ISO8601 as a default formatting
        bim::xstring fmt = format.size()>0 ? format : "%Y-%m-%d";
        DateTime dt = DateTime::from_string(s, fmt);
        this->t.tm_year = dt.t.tm_year;
        this->t.tm_mon = dt.t.tm_mon;
        this->t.tm_mday = dt.t.tm_mday;
        this->t.tm_wday = dt.t.tm_wday;
        this->t.tm_yday = dt.t.tm_yday;
    }

    void set_time(const xstring &s, const xstring &format = "") {
        // set only the time portion form the string, use the ISO8601 as a default formatting
        bim::xstring fmt = format.size() > 0 ? format : "%H:%M:%S";
        DateTime dt = DateTime::from_string(s, fmt);
        this->t.tm_hour = dt.t.tm_hour;
        this->t.tm_min = dt.t.tm_min;
        this->t.tm_sec = dt.t.tm_sec;
        this->t.tm_isdst = dt.t.tm_isdst;
    }

public:
    static DateTime from_now() {
        DateTime dt;
        time_t rawtime;
        time(&rawtime);
#ifdef _MSC_VER
        localtime_s(&dt.t, &rawtime);
#else
        localtime_r(&rawtime, &dt.t);
#endif
        return dt;
    }

    static DateTime from_time(time_t rawtime) {
        DateTime dt;
#ifdef _MSC_VER
        gmtime_s(&dt.t, &rawtime);
#else
        dt.t = *gmtime(&rawtime);
#endif
        return dt;
    }

    static DateTime from_string(const xstring &_s, const xstring &format = "") {
        // set only the time portion form the string, use the ISO8601 as a default formatting
        bim::xstring fmt = format.size() > 0 ? format : "%Y-%m-%dT%H:%M:%S";
        bim::xstring s = _s;
        DateTime dt;

        DateTime::pre_fix(dt, s, fmt);

        std::istringstream ss(s);
        ss >> std::get_time(&dt.t, fmt.c_str());

        DateTime::post_fix(dt, s, fmt);
        return dt;
    }

    static DateTime from_strings(const xstring &sd, const xstring &st, const xstring &fd = "", const xstring &ft = "") {
        // set form separate date/time strings, use the ISO8601 as a default formatting
        bim::xstring fmtd = fd.size() > 0 ? fd : "%Y-%m-%d";
        bim::xstring fmtt = ft.size() > 0 ? ft : "%H:%M:%S";

        DateTime dt = bim::DateTime::from_string(sd, fmtd);
        dt.set_time(st, fmtt);
        return dt;
    }

    static DateTime from_string_iso8601(const xstring &s) {
        return DateTime::from_string(s, "%Y-%m-%dT%H:%M:%S");
    }

    static DateTime from_string_us(const xstring &s) { 
        // 08/28/2006 04:34:47.000 PM
        // this will support PM/AM string anywhere in the text by "from_string" due to lacking milliseconds parsing scheme
        return DateTime::from_string(s, "%Y/%m/%d %I:%M:%S");
    }

    static DateTime from_string_us_long(const xstring &s) {
        // 02:27:29 PM Fri Dec 17 2004
        return DateTime::from_string(s, "%I:%M:%S %p %a %b %d %Y");
    }

 protected:

    static int fix_replace(DateTime &dt, xstring &s, xstring &fmt, const xstring &var, const std::vector<xstring> &vec) {
        std::vector<xstring> varv = { " %s", "%s ", "%s" };
        for (int i = 0; i < varv.size(); ++i) {
            xstring v = xstring::xprintf(varv[i].c_str(), var.c_str());
            if (fmt.contains(v)) {
                for (int j = 0; j < vec.size(); ++j) {
                    xstring r = xstring::xprintf(varv[i].c_str(), vec[j].c_str());
                    if (s.contains(r)) {
                        fmt = fmt.replace(v, "");
                        s = s.replace(r, "");
                        return j;
                    }
                }
            }
        }
        return -1;
    }

    static void fix_milliseconds(DateTime &dt, xstring &s, xstring &fmt) {
        if (fmt.endsWith("%q")) {
            fmt = fmt.replace("%q", "");
            s = s.trim_after(".");
        } else if (fmt.contains("%q ")) {
            fmt = fmt.replace("%q", "");
            std::string::size_type p = s.rfind(".");
            if (p != std::string::npos) {
                xstring sub = "." + s.section(".", " ", p);
                s = s.replace(sub, "");
            }
        }
    }

    static void pre_fix(DateTime &dt, xstring &s, xstring &fmt) {
        // crazy hacks follow

        // remove milliseconds block ".XXX" as it may interfere with the rest of processing
        DateTime::fix_milliseconds(dt, s, fmt);

#ifndef _MSC_VER
        // fixes libstdc++ bug in %b processing
        int p = DateTime::fix_replace(dt, s, fmt, "%b", dt.months_long);
        if (p >= 0)
            dt.t.tm_mon = p;
        p = DateTime::fix_replace(dt, s, fmt, "%b", dt.months_short);
        if (p >= 0)
            dt.t.tm_mon = p;

        // days of the week
        DateTime::fix_replace(dt, s, fmt, "%a", dt.weekdays_long);
        DateTime::fix_replace(dt, s, fmt, "%a", dt.weekdays_short);

        // AM/PM
        if (DateTime::fix_replace(dt, s, fmt, "%p", dt.daytime) == 1) {
            dt.hour_inc = 12;
        }
#endif
     }

    static void post_fix(DateTime &dt, const xstring &s, const xstring &fmt) {
        // crazy hacks follow

        //account for PM in presense of milliseconds in the string
        if (s.contains("PM") && !fmt.contains("%p") && fmt.contains("%I")) {
            dt.t.tm_hour += 12;
        }

        if (dt.hour_inc > 0) {
            dt.t.tm_hour += dt.hour_inc;
            dt.hour_inc = 0;
        }

#ifndef _MSC_VER
        // fixes for libstdc++ bug in year processing
        if (fmt.contains("%Y") || fmt.contains("%y")) {
            if (dt.t.tm_year < 69)
                dt.t.tm_year += 100;
        }
#endif
    }

private:
    int hour_inc = 0;
};

} // namespace bim

#endif // BIM_DATETIME
