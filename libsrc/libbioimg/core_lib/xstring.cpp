/*****************************************************************************
 Extended String Class

 IMPLEMENTATION

 Author: Dima V. Fedorov <mailto:dima@dimin.net> <http://www.dimin.net/>

 History:
   12/05/2005 23:38 - First creation

 Ver : 7
*****************************************************************************/

#include <cstdarg>
#include <cstdio>
#include <cstring>

#include <algorithm>
#include <ctype.h>
#include <fstream>
#include <iostream>
#include <locale>
#include <sstream>
#include <utility>
#include <string>

#include "xstring.h"

#ifdef BIM_USE_CODECVT
#include <codecvt>
#endif

const int MAX_STR_SIZE = 1024 * 100;

#if (defined(_MSC_VER) && (_MSC_VER >= 1400))
#define HAVE_SECURE_C
#pragma message(">>>>>  xstring: using secure C libraries")
#endif

using namespace bim;

//******************************************************************************

xstring &xstring::sprintf(const char *fmt, ...) {
    //const char *fmt = format.c_str();
    va_list ap;
    va_start(ap, fmt);
    this->_sprintf(fmt, ap);
    va_end(ap);
    return *this;
}

xstring &xstring::saddf(const char *fmt, ...) {
    //const char *fmt = format.c_str();
    xstring result;
    va_list ap;
    va_start(ap, fmt);
    result._sprintf(fmt, ap);
    va_end(ap);
    *this += result;
    return (*this);
}

xstring xstring::xprintf(const char *fmt, ...) {
    //const char *fmt = format.c_str();
    xstring result;
    va_list ap;
    va_start(ap, fmt);
    result._sprintf(fmt, ap);
    va_end(ap);
    return result;
}

xstring &xstring::_sprintf(const char *format, va_list ap) {
    std::vector<char> cbuf(MAX_STR_SIZE);
#ifdef HAVE_SECURE_C
    int w = vsprintf_s((char *)&cbuf[0], MAX_STR_SIZE, format, ap);
#else
    int w = vsnprintf((char *)&cbuf[0], MAX_STR_SIZE, format, ap);
#endif
    if (w > MAX_STR_SIZE) {
        cbuf.resize(w);
#ifdef HAVE_SECURE_C
        vsprintf_s((char *)&cbuf[0], w, format, ap);
#else
        vsnprintf((char *)&cbuf[0], w, format, ap);
#endif
    }

    *this = &cbuf[0];
    return *this;
}

xstring xstring::fromInt(const bim::int64& v) {
    return std::to_string(v);
}

xstring xstring::fromUInt(const bim::uint64& v) {
    return std::to_string(v);
}

xstring xstring::fromFloat(const float& v) {
    return std::to_string(v);
}
xstring xstring::fromDouble(const double& v) {
    return std::to_string(v);
}

//******************************************************************************

int xstring::sscanf(const char *fmt, ...) const {
    //const char *fmt = format.c_str();
    va_list ap;
    va_start(ap, fmt);
    int result = this->_sscanf(fmt, ap);
    va_end(ap);
    return result;
}

int xstring::_sscanf(const char *format, va_list ap) const {
#ifdef HAVE_SECURE_C
    return vsscanf_s((const char *)this->c_str(), format, ap);
#else
    return vsscanf((const char *)this->c_str(), format, ap);
#endif
}

//******************************************************************************

xstring &xstring::insertAfterLast(const char *p, const xstring &s) {
    const size_t sp = (unsigned int)this->rfind(p);
    if (sp != std::string::npos)
        this->insert(sp, s);
    return *this;
}

//******************************************************************************

// strips trailing chars
xstring xstring::rstrip(const xstring &chars) const {
    xstring s(*this);
    size_t p = s.find_last_not_of(chars);
    if (p != std::string::npos) {
        s.resize(p + 1, ' ');
    } else { // if the string is only composed of those chars
        for (unsigned int i = 0; i < chars.size(); ++i)
            if (s[0] == chars[i]) {
                s.resize(0);
                break;
            }
    }
    return s;
}

// strips leading chars
xstring xstring::lstrip(const xstring &chars) const {
    xstring s(*this);
    size_t p = s.find_first_not_of(chars);
    if (p != std::string::npos) {
        s = s.substr(p, std::string::npos);
    } else { // if the string is only composed of those chars
        for (unsigned int i = 0; i < chars.size(); ++i)
            if (s[0] == chars[i]) {
                s.resize(0);
                break;
            }
    }
    return s;
}

// strips leading and trailing chars
xstring xstring::strip(const xstring &chars) const {
    return this->rstrip(chars).lstrip(chars);
}

bool is_zero(int i) { return i == 0; }

xstring xstring::erase_zeros() const {
    xstring s(*this);
    s.erase(std::remove_if(s.begin(), s.end(), is_zero), s.end());
    return s;
}

//******************************************************************************

template<typename RT, typename T, typename Trait, typename Alloc>
RT str_to_num(const std::basic_string<T, Trait, Alloc> &the_string) {
    std::basic_istringstream<T, Trait, Alloc> temp_ss(the_string);
    RT num;
    temp_ss.exceptions(std::ios::badbit | std::ios::failbit);
    temp_ss >> num;
    return num;
}

template<typename T, typename Trait, typename Alloc>
bim::uint64 str_to_num(const std::basic_string<T, Trait, Alloc> &the_string) {
    bim::uint64 v = std::stoull(the_string);
    return v;
}

template<typename T, typename Trait, typename Alloc>
bim::int64 str_to_num(const std::basic_string<T, Trait, Alloc> &the_string) {
    bim::int64 v = std::stoll(the_string);
    return v;
}

template<typename T, typename Trait, typename Alloc>
double str_to_num(const std::basic_string<T, Trait, Alloc> &the_string) {
    double v = std::stold(the_string);
    return v;
}

template<typename RT, typename T, typename Trait, typename Alloc>
RT string_to_num(const std::basic_string<T, Trait, Alloc> &the_string, RT def) {
    if (the_string.size() < 1) return def;
    try {
        return str_to_num<RT, T, Trait, Alloc>(the_string);
    } catch (std::ios_base::failure e) {
        return def;
    }
}

int xstring::toInt(int def) const {
    bim::int64 v = string_to_num<bim::int64>(*this, def);
    return (int) v;
}

unsigned int xstring::toUInt(unsigned int def) const {
    bim::uint64 v = string_to_num<bim::uint64>(*this, def);
    return (unsigned int) v;
}

bim::int64 xstring::toInt64(bim::int64 def) const {
    return string_to_num<bim::int64>(*this, def);
}

bim::uint64 xstring::toUInt64(bim::uint64 def) const {
    return string_to_num<bim::uint64>(*this, def);
}

int xstring::toIntFromHex(int def) const {
    int n = def;
    int r = this->sscanf("%x", &n);
    return n;
}

double xstring::toDouble(double def) const {
    return string_to_num<double>(*this, def);
}

// converts from either numeric 0 or 1 or from textual 'true' or 'false'
bool xstring::toBool(bool def) const {
    if (this->toLowerCase() == "true") return true;
    if (this->toLowerCase() == "false") return false;
    if (this->toInt(0) == 0) return false;
    return true;
}

bool xstring::convertableToBool() const {
    if (this->toLowerCase() == "true") return true;
    if (this->toLowerCase() == "false") return true;
    return false;
}

bool xstring::convertableToInt() const {
    try {
        str_to_num<int>(*this);
    } catch (std::ios_base::failure e) {
        return false;
    }
    return true;
}

bool xstring::convertableToDouble() const {
    try {
        str_to_num<double>(*this);
    } catch (std::ios_base::failure e) {
        return false;
    }
    return true;
}

//******************************************************************************

bool xstring::operator==(const xstring &other) const {
    return (size() == other.size()) && (memcmp(this->c_str(), other.c_str(), size()) == 0);
}

bool xstring::operator==(const char *s) const {
    xstring other = s;
    return (size() == other.size()) && (memcmp(this->c_str(), other.c_str(), size()) == 0);
}

int xstring::compare(const xstring &s) const {
    return strncmp(this->c_str(), s.c_str(), std::min(this->size(), s.size()));
}

//******************************************************************************

bool xstring::startsWith(const xstring &s, bool strict) const {
    if (strict == true && s.size() > this->size()) return false;
    //return (this->substr( 0, s.size() ) == s);
    return (strncmp(this->c_str(), s.c_str(), std::min(this->size(), s.size())) == 0);
}

bool xstring::endsWith(const xstring &s) const {
    if (s.size() > this->size()) return false;
    return (this->substr(this->size() - s.size(), s.size()) == s);
}


//******************************************************************************
std::vector<xstring> xstring::split(const xstring &s, const bool &ignore_empty) const {

    std::vector<xstring> list;
    xstring part;

    std::string::size_type start = 0;
    std::string::size_type end;

    while (1) {
        end = this->find(s, start);
        if (end == std::string::npos) {
            part = this->substr(start);
            if (!ignore_empty || (ignore_empty && part.size() > 0))
                list.push_back(part);
            break;
        } else {
            part = this->substr(start, end - start);
            start = end + s.size();
            if (!ignore_empty || (ignore_empty && part.size() > 0))
                list.push_back(part);
        }
    }

    return list;
}

std::vector<int> xstring::splitInt(const xstring &separator, const int &def) const {
    std::vector<int> v2;
    for (xstring val : this->split(separator))
        v2.push_back(val.toInt(def));
    return v2;
}

std::vector<double> xstring::splitDouble(const xstring &separator, const double &def) const {
    std::vector<double> v2;
    for (xstring val : this->split(separator))
        v2.push_back(val.toDouble(def));
    return v2;
}

//******************************************************************************

xstring xstring::join(const std::vector<xstring> &v, const xstring &separator) {
    if (v.size() == 0) return "";
    xstring encs = separator.toPercentEncoding();
    xstring s(v[0].replace(separator, encs));
    for (size_t i = 1; i < v.size(); ++i) {
        s += separator;
        s += v[i].replace(separator, encs);
    }
    return s;
}

xstring xstring::join(const std::vector<int> &v, const xstring &separator) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%d", v[i]);
    }
    return xstring::join(vv, separator);
}

xstring xstring::join(const std::vector<double> &v, const xstring &separator) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%f", v[i]);
    }
    return xstring::join(vv, separator);
}

xstring xstring::join(const std::vector<unsigned char> &v, const xstring &separator) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%X", v[i]);
    }
    return xstring::join(vv, separator);
}

xstring xstring::join(const std::vector<bim::Coord2i> &v, const xstring &separator1, const xstring &s2) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%d%s%d", std::get<0>(v[i]), s2.c_str(), std::get<1>(v[i]));
    }
    return xstring::join(vv, separator1);
}

xstring xstring::join(const std::vector<bim::Coord3i> &v, const xstring &separator1, const xstring &s2) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%d%s%d%s%d", std::get<0>(v[i]), s2.c_str(), std::get<1>(v[i]), s2.c_str(), std::get<2>(v[i]));
    }
    return xstring::join(vv, separator1);
}

xstring xstring::join(const std::vector<bim::Coord4i> &v, const xstring &separator1, const xstring &s2) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%d%s%d%s%d%s%d", std::get<0>(v[i]), s2.c_str(), std::get<1>(v[i]), s2.c_str(), std::get<2>(v[i]), s2.c_str(), std::get<3>(v[i]));
    }
    return xstring::join(vv, separator1);
}

xstring xstring::join(const std::vector<bim::Coord2f> &v, const xstring &separator1, const xstring &s2) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%f%s%f", std::get<0>(v[i]), s2.c_str(), std::get<1>(v[i]));
    }
    return xstring::join(vv, separator1);
}

xstring xstring::join(const std::vector<bim::Coord3f> &v, const xstring &separator1, const xstring &s2) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%f%s%f%s%f", std::get<0>(v[i]), s2.c_str(), std::get<1>(v[i]), s2.c_str(), std::get<2>(v[i]));
    }
    return xstring::join(vv, separator1);
}

xstring xstring::join(const std::vector<bim::Coord4f> &v, const xstring &separator1, const xstring &s2) {
    std::vector<xstring> vv(v.size());
    for (size_t i = 0; i < v.size(); ++i) {
        vv[i] = xstring::xprintf("%f%s%f%s%f%s%f", std::get<0>(v[i]), s2.c_str(), std::get<1>(v[i]), s2.c_str(), std::get<2>(v[i]), s2.c_str(), std::get<3>(v[i]));
    }
    return xstring::join(vv, separator1);
}




//******************************************************************************
xstring xstring::toLowerCase() const {
    xstring s = *this;
    std::transform(s.begin(), s.end(), s.begin(), tolower);
    return s;
}

xstring xstring::toUpperCase() const {
    xstring s = *this;
    std::transform(s.begin(), s.end(), s.begin(), toupper);
    return s;
}

//******************************************************************************
xstring xstring::left(std::string::size_type pos) const {
    return this->substr(0, pos);
}

xstring xstring::left(const xstring &sub) const {
    std::string::size_type p = this->find(sub);
    if (p == std::string::npos) return xstring();
    return this->left(p);
}

xstring xstring::right(std::string::size_type pos) const {
    return this->substr(pos, std::string::npos);
}

xstring xstring::right(const xstring &sub) const {
    std::string::size_type p = this->rfind(sub);
    if (p == std::string::npos) return xstring();
    return this->right(p + sub.size());
}

xstring xstring::trim_after(const xstring& sub) const {
    std::string::size_type p = this->rfind(sub);
    if (p == std::string::npos || p <= 1) return *this;
    return this->left(p);
}

xstring xstring::section(std::string::size_type start, std::string::size_type end) const {
    return this->substr(start, end - start);
}

xstring xstring::section(const xstring &start, const xstring &end, std::string::size_type pos) const {
    std::string::size_type s = this->find(start, pos);
    if (s == std::string::npos)
        return xstring();
    else
        s += start.size();
    std::string::size_type e = this->find(end, s);
    if (e == std::string::npos) return xstring();
    return this->section(s, e);
}

xstring bim::xstring::consume_token(const xstring &separator, std::string::size_type pos) {
    std::string::size_type e = this->find(separator, pos);
    if (e == std::string::npos)
        return xstring();

    xstring token = this->section(pos, e); 
    *this = this->right(e + separator.size());
    return token;
}

//******************************************************************************
bool xstring::contains(const xstring &str) const {
    std::string::size_type loc = this->find(str, 0);
    return loc != std::string::npos;
}

bool xstring::contains(const char &ch) const {
    std::string::size_type loc = this->find(ch, 0);
    return loc != std::string::npos;
}

//******************************************************************************
xstring xstring::replace(const xstring &what, const xstring &with) const {
    std::string::size_type p = this->find(what, 0);
    if (p == std::string::npos) return *this;
    xstring r = *this;
    while (p != std::string::npos) {
        r = r.replace(p, what.size(), with.c_str());
        p = r.find(what, p + with.size());
    }
    return r;
}

// replaces variables encased in {} with the values from the map
xstring xstring::processTemplate(const std::map<std::string, std::string> &vars) const {
    xstring o = *this;
    std::map<std::string, std::string>::const_iterator it = vars.begin();
    while (it != vars.end()) {
        xstring k = xstring::xprintf("{%s}", it->first.c_str());
        xstring v = it->second;
        o = o.replace(k, v);
        ++it;
    }
    return o;
}


//******************************************************************************
// I/O
//******************************************************************************

bool xstring::toFile(const xstring &file_name) const {
    std::ofstream f;
    f.open(file_name.c_str());
    if (!f.is_open()) return false;
    f << *this;
    f.close();
    return true;
}

bool xstring::fromFile(const xstring &file_name) {
    std::ifstream f(file_name.c_str());
    if (!f.is_open()) return false;
    while (!f.eof()) {
        std::string line;
        std::getline(f, line);
        this->append(line);
        this->append("\n");
    }
    f.close();
    return true;
}

//******************************************************************************
// character encoding
//******************************************************************************

#ifdef BIM_USE_CODECVT

std::wstring xstring::toUTF16() const {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
    return converter.from_bytes(*this);
}

void xstring::fromUTF16(const std::wstring &utf16) {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
    *this = converter.to_bytes(utf16);
}

/*
std::string xstring::toLatin1() const {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
    return converter.from_bytes(*this);
}

void xstring::fromLatin1(const std::string &str) {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
    *this = converter.to_bytes(utf16);
}*/


xstring xstring::FromUTF16(const std::wstring &utf16) {
    xstring result;
    result.fromUTF16(utf16);
    return result;
}

#endif

xstring xstring::FromFixed16(const char *f16, const size_t size) {
    xstring result;
    for (size_t i = 0; i < size; i += 2) {
        if (f16[i] == 0) break;
        result += f16[i];
    }
    return result;
}

xstring xstring::FromFixed16(const std::vector<unsigned char> &f16) {
    return xstring::FromFixed16((const char *)&f16[0], f16.size());
}

// percent and url encodings

char from_hex(char ch) {
    return isdigit(ch) ? ch - '0' : tolower(ch) - 'a' + 10;
}

char to_hex(char code) {
    static char hex[] = "0123456789abcdef";
    return hex[code & 15];
}

xstring xstring::toPercentEncoding(const bim::xstring &skip) const {
    xstring result;
    for (size_t i = 0; i < this->size(); ++i) {
        char ch = this->at(i);
        if (skip.size() > 0 && skip.contains(ch)) {
            result += this->at(i);
        } else {
            result += "%";
            result += to_hex(ch);
        }
    }
    return result;
}

void xstring::fromPercentEncoding(const bim::xstring &str) {
    xstring result;
    for (size_t i = 0; i < str.size(); ++i) {
        if (str[i] == '%' && str.size() > i + 2) {
            char d = (from_hex(str[i + 1]) << 4) | from_hex(str[i + 2]);
            result += d;
            i += 2;
        }
    }
    *this = result;
}

xstring xstring::toUrlEncoding(const bim::xstring &skip) const {
    return this->toPercentEncoding(skip);
}

void xstring::fromUrlEncoding(const bim::xstring &str) {
    this->fromPercentEncoding(str);
}

xstring xstring::toMetaEncoding() const {
    return this->replace(":", "%3A").replace("<", "%3C").replace(">", "%3E").replace("\"", "%22").replace("\n", "%0A");
}

xstring xstring::fromMetaEncoding() const {
    return this->replace("%3A", ":").replace("%3C", "<").replace("%3E", ">").replace("%22", "\"").replace("%0A", "\n");
}
